package com.google.code.mircle.core.transfers.queue;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class QueueView extends JPanel implements Controller {

    public QueueView(QueueModel s) {
        super(new BorderLayout());

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("[automatic queue, handle only 3 torrent at once. other staying in queue]");
        listModel.addElement("Seeding Torrent 1");
        listModel.addElement("Seeding Torrent 2");
        listModel.addElement("Seeding Torrent 3");
        listModel.addElement("Sending DC++ file to USer1");

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
