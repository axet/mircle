package com.google.code.mircle.core;

import javax.swing.JPopupMenu;

/**
 * Root elements in the left pane
 * 
 * @author axet
 * 
 */
public interface PluginRoot extends TreeItemModel {

    public JPopupMenu contextMenu(Model m);

    public Controller createController(Model m);

    /**
     * return TransferHandler.MOVE or TransferHandler.NONE
     * 
     * @param m
     * @return
     */
    public int dndAction(Model m);
    
    public boolean dndCheck(Model source, Model target);
    
    public boolean dndDrop(Model source, Model target);

    public View createView(Model m);

}
