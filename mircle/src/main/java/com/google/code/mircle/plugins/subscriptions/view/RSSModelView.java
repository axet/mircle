package com.google.code.mircle.plugins.subscriptions.view;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.Worker.Status;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;

public class RSSModelView implements View {

    RSSModel model;

    public RSSModelView(RSSModel m) {
        model = m;
    }

    public String toString() {
        Status s = model.status();
        String str = "";
        if (s.delay != null) {
            return s.title + " retyring in " + s.delay;
        } else {
            if (model.getState().getDownloadState() == DownloadState.DOWNLOAD_QUEUED)
                str += "Q ";

            if (model.getState().getDownloadState() == DownloadState.DOWNLOAD_START)
                str += "R ";

            if (model.getState().getDownloadState() == DownloadState.DOWNLOAD_FAILED)
                str += "E ";

            RSSModel r = model;

            if (model.getDownloadedCount() > 0)
                str += "(" + model.getDownloadedCount() + ") / ";
            else if (r.getState().feed.getNewCount() > 0)
                str += "(new: " + r.getState().feed.getNewCount() + ") / ";

            if (s.total != null) {
                float progress = s.current / (float) s.total;
                str += (int) (Math.floor(progress * 100)) + "% /";
            }

            if (model.getState().getDownloadState() == DownloadState.DOWNLOAD_START) {
                if (s.downloadRate != null) {
                    float speed = s.downloadRate;
                    if (speed < 1000000) {
                        speed /= 1024;
                        str += " / " + String.format("%.02f", speed) + " KB/s";
                    } else {
                        speed /= 1024 * 1024;
                        str += " / " + String.format("%.02f", speed) + " MB/s";
                    }
                }

                if (s.total != null && s.downloadRateAverage > 0) {
                    long diff = ((s.total - s.current) / s.downloadRateAverage);
                    long days = diff / (60 * 60 * 24);
                    diff -= days * (60 * 60 * 24);
                    long hours = diff / (60 * 60);
                    diff -= hours * (60 * 60);
                    long min = diff / 60;
                    diff -= min * 60;
                    long sec = diff;
                    if (!str.isEmpty())
                        str += " / ";
                    if (days != 0)
                        str += "days:" + days + " ";
                    if (hours != 0)
                        str += String.format("%02d:", hours);
                    if (hours != 0 || min != 0)
                        str += String.format("%02d:", min);

                    // 00:00:12
                    if (hours == 0 && min == 0 && sec > 9)
                        str += String.format("%02d", sec);
                    // 00:00:09
                    else if (hours == 0 && min == 0 && sec < 10)
                        str += String.format("%d", sec);
                    // 02:04:09
                    else
                        str += String.format("%02d", sec);

                    if (days == 0 && hours == 0 && min == 0)
                        str += " secs";

                    str += " / ";
                }
            }
            str += s.title;

            return str;
        }
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public void close() {
    }

}
