package com.google.code.mircle.plugins.community;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class CommunityView extends JPanel implements Controller {
    private static final long serialVersionUID = -400956807334604470L;

    public CommunityView(CommunityModel s) {
        super(new BorderLayout());

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("Channel element 1");
        listModel.addElement("Channel element 2");
        listModel.addElement(s.toString());

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
