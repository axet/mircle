package com.google.code.mircle.core.transfers.direct;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.Worker.Status;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class TransferView implements View {

    Transfer t;

    public TransferView(Transfer t) {
        this.t = t;
    }

    public String toString() {
        Status s = t.status();
        String str = "";
        if (s.delay != 0) {
            return s.title + " retyring in " + s.delay;
        } else {
            switch (t.getState().getDownloadState()) {
            case DOWNLOAD_QUEUED:
                str += "Q ";
                break;
            case DOWNLOAD_FAILED:
                str += "E ";
                break;
            case DOWNLOAD_COMPLETE:
            case DOWNLOAD_STOP:
                if (s.total != null) {
                    float progress = s.current / (float) s.total;
                    str += (int) (Math.floor(progress * 100)) + "%";
                }
                break;
            case DOWNLOAD_START:
                if (s.total != null) {
                    float progress = s.current / (float) s.total;
                    str += (int) (Math.floor(progress * 100)) + "%";
                }

                float speed = s.downloadRate;
                if (speed < 1000000) {
                    speed /= 1024;
                    str += " / " + String.format("%.02f", speed) + " KB/s";
                } else {
                    speed /= 1024 * 1024;
                    str += " / " + String.format("%.02f", speed) + " MB/s";
                }

                if (s.total != null && s.downloadRateAverage > 0) {
                    long diff = ((s.total - s.current) / s.downloadRateAverage);
                    long days = diff / (60 * 60 * 24);
                    diff -= days * (60 * 60 * 24);
                    long hours = diff / (60 * 60);
                    diff -= hours * (60 * 60);
                    long min = diff / 60;
                    diff -= min * 60;
                    long sec = diff;
                    str += " / ";
                    if (days != 0)
                        str += "days:" + days + " ";
                    if (hours != 0)
                        str += String.format("%02d:", hours);
                    if (hours != 0 || min != 0)
                        str += String.format("%02d:", min);

                    // 00:00:12
                    if (hours == 0 && min == 0 && sec > 9)
                        str += String.format("%02d", sec);
                    // 00:00:09
                    else if (hours == 0 && min == 0 && sec < 10)
                        str += String.format("%d", sec);
                    // 02:04:09
                    else
                        str += String.format("%02d", sec);

                    if (days == 0 && hours == 0 && min == 0)
                        str += " secs";
                }
                str += " ";
                break;
            default:
                break;
            }

            str += "/ " + s.title;

            Transfer.State ss = (State) t.getState();
            if (ss.getDeleted())
                str += " / DELETED";

            return str;
        }
    }

    @Override
    public Model getModel() {
        return t;
    }

    @Override
    public void close() {
    }

}
