package com.google.code.mircle.core;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.DefaultListModel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

public abstract class QuickSearch {

    public interface ListModel {
        void add(Object o);

        int getCount();

        Object get(int i);

        String toString(Object o);
    }

    public interface ListContainer {
        public void setModel(ListModel l);

        public ListModel getModel();

        public void requestFocus();

        public void addKeyListener(KeyListener l);

        public Object getSelected();

        public void scrollTo(Object m);

        public void select(Object m);
    }

    private ListContainer container;
    JTextField quickSearchField;
    ListModel modelMaster;
    ListModel modelMasterSearch;

    public static final int MAX_RESULTS = 25;

    public QuickSearch() {
    }

    public void create(JTextField quickSearchField, ListContainer container) {
        this.container = container;
        this.quickSearchField = quickSearchField;

        quickSearchField.setVisible(false);
        quickSearchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                case KeyEvent.VK_ESCAPE:
                    cancel();
                    QuickSearch.this.container.requestFocus();
                    break;
                }
            }
        });

        container.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                QuickSearch.this.quickSearchField.setVisible(true);
                QuickSearch.this.doParentLayout();

                char c = e.getKeyChar();

                if (Character.isLetterOrDigit(c)) {
                    try {
                        QuickSearch.this.quickSearchField.getDocument().insertString(
                                QuickSearch.this.quickSearchField.getText().length(), "" + c, null);
                    } catch (BadLocationException e1) {
                        throw new RuntimeException(e1);
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                case KeyEvent.VK_ESCAPE:
                    cancel();
                    break;
                case KeyEvent.VK_BACK_SPACE:
                    String s = QuickSearch.this.quickSearchField.getText();
                    if (s.length() > 0)
                        s = s.substring(0, s.length() - 1);
                    QuickSearch.this.quickSearchField.setText(s);
                    break;
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_RIGHT:
                    QuickSearch.this.quickSearchField.requestFocus();
                    break;
                }
            }
        });

        quickSearchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent arg0) {
                quickSearchGenres(QuickSearch.this.quickSearchField.getText());
            }

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                quickSearchGenres(QuickSearch.this.quickSearchField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                quickSearchGenres(QuickSearch.this.quickSearchField.getText());
            }
        });
    }

    public void quickSearchGenres(String str) {
        if (str.isEmpty()) {
            // after quicksearch box closed. we need to select the same genre.
            Object m = container.getSelected();

            container.setModel(modelMaster);
            notifyQuickExit();
            modelMaster = null;
            modelMasterSearch = null;

            if (m != null) {
                container.scrollTo(m);

                container.select(m);
            }
        } else {
            if (modelMaster == null) {
                modelMaster = container.getModel();

                notifyQuickEnter();
            }
            if (modelMasterSearch == null) {
                modelMasterSearch = getMasterListModel();
            }

            ListModel genresQs = createListModel();

            container.setModel(genresQs);

            str = str.toLowerCase();

            for (int i = 0; i < modelMasterSearch.getCount(); i++) {
                Object m = modelMasterSearch.get(i);
                if (modelMasterSearch.toString(m).toLowerCase().contains(str)) {
                    genresQs.add(m);
                    if (genresQs.getCount() > MAX_RESULTS) {
                        maxResultsReached();
                        break;
                    }
                }
            }

            if (genresQs.getCount() > 0) {
                Object m = genresQs.get(0);
                container.select(m);
            }

            notifySearch();
        }
    }

    public void cancel() {
        QuickSearch.this.quickSearchField.setText("");
        QuickSearch.this.quickSearchField.setVisible(false);
        QuickSearch.this.doParentLayout();
    }

    public boolean active() {
        return modelMaster != null;
    }

    public void notifyQuickEnter() {
    }

    public void notifyQuickExit() {
    }

    public void maxResultsReached() {
    }

    public abstract ListModel getMasterListModel();

    public abstract void notifySearch();

    public abstract void doParentLayout();

    public abstract ListModel createListModel();
}
