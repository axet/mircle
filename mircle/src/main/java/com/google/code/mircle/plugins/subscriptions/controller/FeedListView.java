package com.google.code.mircle.plugins.subscriptions.controller;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.google.code.mircle.plugins.subscriptions.model.FeedListener;
import com.google.code.mircle.plugins.subscriptions.model.Range;

public abstract class FeedListView extends JList {
    private static final long serialVersionUID = -5843882598074012891L;

    FeedListener modelMaster;
    DefaultListModel modelSlave = new DefaultListModel();

    final static int PAGE_SIZE = 25;

    public FeedListView(FeedListener listModel) {
        this.modelMaster = listModel;

        setModel(modelSlave);

        // for java designer
        if (modelMaster == null)
            return;

        modelMaster.addListener(new ListDataListener() {
            @Override
            public void intervalRemoved(ListDataEvent arg0) {
                if (modelSlave.getSize() == 0)
                    return;

                Range r1 = new Range(0, modelSlave.getSize() - 1);
                Range r2 = new Range(arg0.getIndex0(), arg0.getIndex1());

                Range r3 = r1.overlaps(r2);
                if (r3 != null) {
                    modelSlave.removeRange(r3.i1, r3.i2);
                    loadMore(r3.size());
                }
            }

            @Override
            public void intervalAdded(ListDataEvent arg0) {
                Range r2 = new Range(arg0.getIndex0(), arg0.getIndex1());

                if (modelSlave.getSize() == 0) {
                    loadMore(r2.size());
                    return;
                }

                Range r1 = new Range(0, modelSlave.getSize() - 1);

                Range r3 = r1.overlaps(r2);
                if (r3 != null) {
                    loadMore(r3.i1, r3.i2);
                }
            }

            @Override
            public void contentsChanged(ListDataEvent arg0) {
                for (int i = arg0.getIndex0(); i <= arg0.getIndex1(); i++) {
                    // check if entry loaded to the list (we are showing
                    // first 25)
                    if (i < modelSlave.getSize())
                        modelSlave.set(i, createView(modelMaster.get(i)));
                }
            }
        });

        loadMore();
    }

    public void loadMore() {
        loadMore(PAGE_SIZE);
    }

    void loadMore(int more) {
        if (modelMaster.getSize() == 0)
            return;

        int i1 = modelSlave.getSize();
        int i2 = i1 + more - 1;
        i2 = Math.min(modelMaster.getSize() - 1, i2);
        loadMore(i1, i2);
    }

    void loadMore(int i1, int i2) {
        for (int i = i1; i <= i2; i++) {
            Object m = modelMaster.get(i);
            modelSlave.add(i, createView(m));
        }
    }

    abstract Object createView(Object model);
    
    public DefaultListModel getModelSlave() {
        return modelSlave;
    }
}
