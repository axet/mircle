package com.google.code.mircle.core;

import com.github.axet.wget.info.DownloadInfo;

/**
 * Plugin which handles htmls to produce rss. (html with rss inside)
 * 
 */
public interface PluginContent {

    public void close();

    /**
     * check if we can handle the following url, we shall check
     * info.getContentType()
     * 
     * @param info
     * @return we can't handle it == null, if we can - return Model
     */
    public Model extract(DownloadInfo info);

    /**
     * check html for infomration we need. for example rss+atom links.
     * 
     * @param html
     * @return if we can handle content return Model, or null
     */
    public Model extract(DownloadInfo info, String html);
}
