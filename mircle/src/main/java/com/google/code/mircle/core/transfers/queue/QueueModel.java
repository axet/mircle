package com.google.code.mircle.core.transfers.queue;

import com.google.code.mircle.core.Model;

public class QueueModel implements Model {

    String t;

    public QueueModel(String t) {
        this.t = t;
    }

    public String toString() {
        return t;
    }

    @Override
    public void close() {
    }
}
