package com.google.code.mircle.plugins.community;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.google.code.mircle.core.Controller;

public class CommunitySettingsView extends JPanel implements Controller {

    public CommunitySettingsView(CommunityPlugin s) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 103, 59, 0 };
        gridBagLayout.rowHeights = new int[] { 28, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 0, 5, 5);
        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel label_1 = new JLabel("Settings1");
        add(label_1, gbc);

        GridBagConstraints gbc_1 = new GridBagConstraints();
        gbc_1.fill = GridBagConstraints.BOTH;
        gbc_1.anchor = GridBagConstraints.NORTHWEST;
        gbc_1.insets = new Insets(0, 0, 5, 0);
        gbc_1.gridx = 1;
        gbc_1.gridy = 0;

        JTextField textField_1 = new JTextField("value1");
        add(textField_1, gbc_1);
        GridBagConstraints gbc_2 = new GridBagConstraints();
        gbc_2.anchor = GridBagConstraints.WEST;
        gbc_2.insets = new Insets(0, 0, 0, 5);
        gbc_2.gridx = 0;
        gbc_2.gridy = 1;

        JLabel label = new JLabel("Settings2");
        add(label, gbc_2);
        GridBagConstraints gbc_3 = new GridBagConstraints();
        gbc_3.fill = GridBagConstraints.BOTH;
        gbc_3.anchor = GridBagConstraints.NORTHWEST;
        gbc_3.gridx = 1;
        gbc_3.gridy = 1;
        JTextField textField = new JTextField("value2");
        add(textField, gbc_3);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
