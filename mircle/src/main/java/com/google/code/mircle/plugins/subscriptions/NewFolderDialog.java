package com.google.code.mircle.plugins.subscriptions;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class NewFolderDialog extends JDialog {

    private final JButton btnNewButton_1 = new JButton("Cancel");
    final JTextArea textArea;

    public String folderName;

    public NewFolderDialog() {
        final NewFolderDialog that = this;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);

        setBounds(100, 100, 467, 315);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0, 1.0 };
        getContentPane().setLayout(gridBagLayout);

        JLabel lblDirectDownload = new JLabel();
        lblDirectDownload.setText("Folder Name");
        GridBagConstraints gbc_lblDirectDownload = new GridBagConstraints();
        gbc_lblDirectDownload.gridwidth = 2;
        gbc_lblDirectDownload.insets = new Insets(0, 0, 5, 0);
        gbc_lblDirectDownload.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDirectDownload.gridx = 0;
        gbc_lblDirectDownload.gridy = 0;
        getContentPane().add(lblDirectDownload, gbc_lblDirectDownload);

        textArea = new JTextArea();
        textArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    dispose();
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    ok();
            }
        });
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 2;
        gbc_textArea.insets = new Insets(0, 0, 5, 0);
        gbc_textArea.fill = GridBagConstraints.BOTH;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        getContentPane().add(textArea, gbc_textArea);

        JButton btnNewButton = new JButton("OK");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ok();
            }
        });

        GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
        gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
        gbc_btnNewButton.gridx = 0;
        gbc_btnNewButton.gridy = 2;
        getContentPane().add(btnNewButton, gbc_btnNewButton);
        GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
        gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
        gbc_btnNewButton_1.gridx = 1;
        gbc_btnNewButton_1.gridy = 2;
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                that.dispose();
            }
        });
        getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
    }

    void ok() {
        download(textArea.getText());
        dispose();
    }

    public boolean modal(Component root) {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(SwingUtilities.getRoot(root));
        setVisible(true);

        return folderName != null;
    }

    void download(String url) {
        this.folderName = url;
    }

}
