package com.google.code.mircle.core.shares;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class ShareView extends JPanel implements Controller {

    ShareModel s;

    public ShareView(ShareModel s) {
        super(new BorderLayout());

        this.s = s;

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("File 1");
        listModel.addElement("Dir 2");
        listModel.addElement(s.toString());

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
