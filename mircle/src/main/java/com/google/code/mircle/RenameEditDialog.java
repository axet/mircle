package com.google.code.mircle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.ModelRename;

public class RenameEditDialog extends JFrame {

    static class HTextArea extends JTextArea {
        public int row() {
            return getRowHeight();
        }
    }

    private static final long serialVersionUID = -1692984112374933323L;

    public ModelRename m;

    final HTextArea textArea;

    Core core;

    public RenameEditDialog(Core core) {
        this.core = core;

        setUndecorated(true);

        final RenameEditDialog that = this;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        getContentPane().setLayout(new BorderLayout(0, 0));

        textArea = new HTextArea();
        textArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                switch (arg0.getKeyCode()) {
                case KeyEvent.VK_ENTER:
                    ok();
                    that.dispose();
                case KeyEvent.VK_ESCAPE:
                    cancel();
                }
            }
        });

        textArea.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent arg0) {
                cancel();
            }

            @Override
            public void focusGained(FocusEvent arg0) {
            }
        });

        textArea.setBorder(BorderFactory.createEmptyBorder());

        textArea.setLineWrap(true);

        JScrollPane scroll = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder());
        panel.add(scroll);

        setContentPane(panel);
    }

    void ok() {
        m.setName(textArea.getText());
        
        DefaultMutableTreeNode n = core.app.appSwitch.getNode((Model) m);
        DefaultTreeModel model = (DefaultTreeModel) core.app.appGUI.left.getModel();
        model.nodeStructureChanged(n);
        dispose();
    }

    void cancel() {
        dispose();
    }

    public void modal() {
        textArea.setText(m.getName());
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        DefaultMutableTreeNode n = core.app.appSwitch.getNode((Model) m);

        pack();

        Rectangle r = core.app.appGUI.left.getPathBounds(new TreePath(n.getPath()));
        Insets insets = this.getInsets();

        Dimension d = new Dimension(r.width + insets.left + insets.right, r.height + insets.top + insets.bottom);
        d.height = textArea.row() + insets.top + insets.bottom;

        Point tl = new Point(r.x - insets.left, r.y - insets.top);
        SwingUtilities.convertPointToScreen(tl, core.app.appGUI.left);
        setLocation(tl);
        textArea.setSize(d);
        textArea.setSelectionStart(0);
        textArea.setSelectionEnd(textArea.getText().length());

        setVisible(true);
        setResizable(true);

        textArea.requestFocus();
    }
}
