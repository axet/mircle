package com.google.code.mircle;

import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URI;

import com.github.axet.desktop.os.mac.AppleHandlers;
import com.google.code.mircle.core.Core;

public class OSX {

    AppleHandlers a = new AppleHandlers();

    final App app;

    AppleHandlers.AboutHandler ab = new AppleHandlers.AboutHandler() {
        @Override
        public void showAboutMenu() {
            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    app.appGUI.about();
                }
            });
        }
    };

    AppleHandlers.OpenFileHandler o = new AppleHandlers.OpenFileHandler() {
        @Override
        public void openFile(final File e) {
            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    app.mircle(e.toString());
                }
            });
        }
    };

    // global key hooks
    KeyEventDispatcher k = new KeyEventDispatcher() {
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_W && e.isMetaDown()) {
                Component c = (Component) e.getSource();
                Component parent = null;
                while (c != null) {
                    parent = c;
                    c = c.getParent();
                }
                parent.setVisible(false);
            }
            return false;
        }
    };

    AppleHandlers.AppReOpenedListener r = new AppleHandlers.AppReOpenedListener() {
        @Override
        public void appReOpened() {
            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    app.appGUI.show();
                }
            });
        }
    };

    AppleHandlers.OpenURIHandler u = new AppleHandlers.OpenURIHandler() {
        @Override
        public void openURI(final URI uri) {
            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    app.mircle(uri.toString());
                }
            });
        }
    };

    public OSX(final App app) {
        this.app = app;

        a.addOpenFileListener(o);

        a.addAboutListener(ab);

        a.addAppReOpenedListener(r);

        a.addOpenURIListener(u);

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(k);
    }

    public void close() {
        a.removeOpenFileListener(o);

        a.removeAboutListener(ab);

        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(k);

        a.removeAppReOpenedListener(r);

        a.removeOpenURIListener(u);
    }

    public static void registerProtocols(String bundleId) {
        String[] ss;

        // ss = new String[] { "mircle", "magnet" };
        ss = new String[] { "mircle", };
        for (String s : ss)
            AppleHandlers.setUrlType(s, bundleId);

        // ss = new String[] { "org.bittorrent.torrent", "torrent" };
        ss = new String[] {};
        for (String s : ss)
            AppleHandlers.setFileType(s, bundleId);
    }

    public static boolean isAvailable() {
        return AppleHandlers.isAvailable();
    }

    static OSX osx;

    public static void register(App app) {
        osx = new OSX(app);

        registerProtocols();
    }

    public static boolean isMac() {
        return com.sun.jna.Platform.isMac();
    }

    public static void unregister() {
        osx.close();
    }

    public static void registerProtocols() {
        OSX.registerProtocols("com.google.code.mircle");
    }
}
