package com.google.code.mircle.plugins.subscriptions.model;

import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class RSSModelTransferLink {
    public Transfer transfer;
    // has been added to the DownloadedCount
    public boolean addedDownloadedCount;
    // has been added to the DownloadingCount
    public boolean addedDownloadingCount;
    public RSSModel model;
    FeedEntry entry;

    LongRun.Listener transferListener = new LongRun.Listener() {
        @Override
        public void stop() {
            Transfer.State s = (Transfer.State) transfer.getState();
            if (!addedDownloadedCount) {
                if (s.getDownloadState().equals(DownloadState.DOWNLOAD_COMPLETE) && s.isDownloadExist()) {
                    model.setDownloadedCount(model.getDownloadedCount() + 1);
                    addedDownloadedCount = true;
                }
            }

            model.autoDownloadIf();

            model.notifyChanged(entry);
        }

        @Override
        public void start() {
            if (addedDownloadedCount) {
                model.setDownloadedCount(model.getDownloadedCount() - 1);
                addedDownloadedCount = false;
            }
            model.notifyChanged(entry);
        }

        @Override
        public void changed() {
            update();

            Transfer.State state = (State) transfer.getState();
            if (state.getDeleted()) {
                model.autoDownloadIf();
            }

            model.notifyChanged(entry);
        }

        @Override
        public void close() {
            setTransfer(null);

            model.autoDownloadIf();

            model.notifyChanged(entry);
        }
    };

    public RSSModelTransferLink(Core core, RSSModel m, FeedEntry e) {
        addedDownloadedCount = false;
        model = m;
        entry = e;

        // find transfer
        Transfer transfer = core.transfers.getDirect().check(entry.link);
        if (transfer == null)
            transfer = core.transfers.getDirect().check(entry.media);

        if (transfer != null && !entry.direct)
            entry.direct = true;

        setTransfer(transfer);
    }

    public void close() {
        if (transfer != null) {
            transfer.removeListener(transferListener);
            transfer = null;
        }
    }

    public void setTransfer(Transfer t) {
        if (transfer != null)
            transfer.removeListener(transferListener);

        transfer = t;

        update();
    }

    void update() {
        if (addedDownloadingCount) {
            model.setDownloadingCount(model.getDownloadingCount() - 1);
            addedDownloadingCount = false;
        }
        if (addedDownloadedCount) {
            model.setDownloadedCount(model.getDownloadedCount() - 1);
            addedDownloadedCount = false;
        }

        // have we found it? subscribe
        if (transfer != null) {
            // probaby file still downloading. so
            // subscribe to events.
            transfer.addListener(transferListener);
        }

        // only perform checks if it is unread and had a download.
        // if entry has no download no checks.
        // if entry already been read. we do not need inform user about
        // anything.
        if (entry.direct) {
            // have we found it?
            if (transfer == null) {
                // no. trasfer was deleted. mark feedentry as read.
                entry.unread = false;
            } else {
                // yes

                Transfer.State s = (Transfer.State) transfer.getState();

                if (transfer.getState().getDownloadState().equals(DownloadState.DOWNLOAD_COMPLETE)) {
                    // check if file exist
                    if (!s.isDownloadExist()) {
                        // no ? then mark entry as read
                        entry.unread = false;
                    }
                }
            }
        }

        if (transfer != null) {
            // add to the downloading count
            if (!addedDownloadingCount) {
                Transfer.State s = (Transfer.State) transfer.getState();
                if (s.getDownloadState().equals(DownloadState.DOWNLOAD_QUEUED)
                        || s.getDownloadState().equals(DownloadState.DOWNLOAD_START)) {
                    model.setDownloadingCount(model.getDownloadingCount() + 1);
                    addedDownloadingCount = true;
                }
            }

            // add to the downloaded count
            if (!addedDownloadedCount) {
                Transfer.State s = (Transfer.State) transfer.getState();
                if (s.getDownloadState().equals(DownloadState.DOWNLOAD_COMPLETE) && !s.getDeleted()) {
                    model.setDownloadedCount(model.getDownloadedCount() + 1);
                    addedDownloadedCount = true;
                }
            }
        }

    }
}