package com.google.code.mircle.core;

/**
 * Data class displayble as entry in the left pane thougth the View
 * 
 * @author axet
 * 
 */
public interface Model {

    public void close();

}
