package com.google.code.mircle.plugins.radio;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.google.code.mircle.GlobalPlayer;
import com.google.code.mircle.GlobalPlayer.PlayObject;
import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.QuickSearchTable;

public class SHOUTcastModelController extends JPanel implements Controller {

    public static class SHOUTgenreViewRenderer extends JLabel implements TableCellRenderer {
        private static final long serialVersionUID = 8062464613434327456L;

        SHOUTgenreView g;

        public SHOUTgenreViewRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object v, boolean isSelected, boolean hasFocus,
                int row, int column) {
            SHOUTgenreView newColor = (SHOUTgenreView) v;
            g = newColor;
            SHOUTgenreModel m = (SHOUTgenreModel) newColor.getModel();

            if (m.empty())
                setForeground(Color.GRAY);
            else
                setForeground(table.getForeground());

            if (isSelected) {
                setBackground(table.getSelectionBackground());
            } else {
                setBackground(table.getBackground());
            }

            return this;
        }

        public void paint(Graphics g) {
            setText(this.g.toString());

            super.paint(g);
        }
    }

    public static class SHOUTstationsViewRenderer implements TableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object v, boolean isSelected, boolean hasFocus,
                int row, int column) {

            JButton m = (JButton) v;

            if (isSelected) {
                m.setBackground(table.getSelectionBackground());
            } else {
                m.setBackground(table.getBackground());
            }

            return m;
        }
    }

    public static class SHOUTstationsViewEditor extends DefaultCellEditor {

        JButton m;

        public SHOUTstationsViewEditor() {
            super(new JCheckBox());
        }

        private static final long serialVersionUID = -1683641110389482995L;

        @Override
        public Component getTableCellEditorComponent(JTable table, Object v, boolean isSelected, int row, int column) {
            m = (JButton) v;

            if (isSelected) {
                m.setBackground(table.getSelectionBackground());
            } else {
                m.setBackground(table.getBackground());
            }

            return m;
        }

        @Override
        public Object getCellEditorValue() {
            return m;
        }
    }

    private static final long serialVersionUID = 2021402596448431925L;

    SHOUTcastModel t;

    private JTable tableGenres;
    DefaultTableModel genresMaster;
    JScrollPane genresScroll;

    private JTable tableStations;
    DefaultTableModel stationsMaster;

    DefaultTableModel genres;

    QuickSearchTable qsGenres;
    QuickSearchTable qsStations;

    GlobalPlayer.PlayControl control = new GlobalPlayer.PlayControl() {
        @Override
        public PlayObject getPlayObject() {
            return SHOUTcastModelController.this.getPlayObject();
        }
    };

    TableModelListener list = new TableModelListener() {
        @Override
        public void tableChanged(TableModelEvent e) {
            if (e.getType() == TableModelEvent.INSERT) {
                if (qsStations.active())
                    return;
                for (int i = e.getFirstRow(); i <= e.getLastRow(); i++) {
                    updateStation(tableStations, i);
                }
            }
        }
    };

    Core core;

    public SHOUTcastModelController(Core cc, final SHOUTcastModel t) {
        this.t = t;
        this.core = cc;

        genres = t.getGenres();

        setLayout(new BorderLayout(0, 0));

        JSplitPane splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.0);
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        add(splitPane, BorderLayout.CENTER);

        genresMaster = genres;
        tableGenres = new JTable(genres);

        tableGenres.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (qsGenres.active()) {
                    int row = tableGenres.getSelectedRow();

                    if (row != -1) {
                        SHOUTgenreView vv = (SHOUTgenreView) tableGenres.getValueAt(row, 0);
                        SHOUTgenreModel mmm = (SHOUTgenreModel) vv.getModel();

                        for (int i = 0; i < genresMaster.getRowCount(); i++) {
                            SHOUTgenreView v = (SHOUTgenreView) genresMaster.getValueAt(i, 0);
                            SHOUTgenreModel mm = (SHOUTgenreModel) v.getModel();
                            if (mm.equals(mmm))
                                t.getState().selectedGenreIndex = i;
                        }
                    } else {
                        t.getState().selectedGenreIndex = null;
                    }
                } else {
                    t.getState().selectedGenreIndex = tableGenres.getSelectedRow();
                    if (t.getState().selectedGenreIndex == -1)
                        t.getState().selectedGenreIndex = null;
                }
            }
        });
        tableGenres.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableGenres.setDefaultRenderer(Object.class, new SHOUTgenreViewRenderer());
        tableGenres.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int r = tableGenres.getSelectedRow();
                if (r == -1)
                    return;

                SHOUTgenreView v = (SHOUTgenreView) tableGenres.getValueAt(r, 0);
                genre = (SHOUTgenreModel) v.getModel();
                t.click(genre);

                if (tableStations.getModel() != null) {
                    tableStations.getModel().removeTableModelListener(list);
                }

                DefaultTableModel model = genre.getModel();
                model.addTableModelListener(list);
                stationsMaster = model;
                tableStations.setModel(model);
                qsStations.cancel();
                updateStationsWidth(tableStations);
            }
        });

        genresScroll = new JScrollPane(tableGenres);
        final JPanel pgen = new JPanel(new BorderLayout());
        pgen.add(genresScroll);

        final JTextField genreQs = new JTextField();

        qsGenres = new QuickSearchTable(tableGenres, genreQs) {
            @Override
            public int indexOf(DefaultTableModel m, Object o) {
                Object[] oo = (Object[]) o;
                SHOUTgenreView vv = (SHOUTgenreView) oo[0];
                SHOUTgenreModel mmm = (SHOUTgenreModel) vv.getModel();

                for (int i = 0; i < m.getRowCount(); i++) {
                    SHOUTgenreView v = (SHOUTgenreView) m.getValueAt(i, 0);
                    SHOUTgenreModel mm = (SHOUTgenreModel) v.getModel();
                    if (mm.equals(mmm))
                        return i;
                }

                return -1;
            }

            @Override
            public Object getRow(DefaultTableModel m, int i) {
                SHOUTgenreView v = (SHOUTgenreView) m.getValueAt(i, 0);
                SHOUTgenreModel mm = (SHOUTgenreModel) v.getModel();
                return SHOUTgenreView.toTreeModel(mm);
            }

            @Override
            public void doParentLayout() {
                pgen.doLayout();
            }

            @Override
            public void addRow(DefaultTableModel m, Object o) {
                m.addRow((Object[]) o);
            }

            @Override
            public DefaultTableModel createModel() {
                DefaultTableModel m = new DefaultTableModel() {
                    private static final long serialVersionUID = -2971599819634214639L;

                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                SHOUTgenreView.initTree(m);
                return m;
            }

            @Override
            public String toString(Object o) {
                Object[] oo = (Object[]) o;
                return SHOUTgenreView.fromTreeModel(oo).getGenre().getName();
            }

            @Override
            public void notifySearch() {
            }
        };

        pgen.add(genreQs, BorderLayout.SOUTH);
        genresScroll.setPreferredSize(new Dimension(300, 300));
        splitPane.setLeftComponent(pgen);

        tableStations = new JTable();
        tableStations.setDefaultRenderer(JButton.class, new SHOUTstationsViewRenderer());
        tableStations.setDefaultEditor(JButton.class, new SHOUTstationsViewEditor());
        tableStations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tableStations.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (qsStations.active()) {
                    int row = tableStations.getSelectedRow();

                    if (row != -1) {
                        SHOUTstationView vv = (SHOUTstationView) tableStations.getValueAt(row, 0);
                        SHOUTstationModel mmm = (SHOUTstationModel) vv.getModel();

                        core.app.player.control(control);

                        for (int i = 0; i < stationsMaster.getRowCount(); i++) {
                            SHOUTstationView v = (SHOUTstationView) stationsMaster.getValueAt(i, 0);
                            SHOUTstationModel mm = (SHOUTstationModel) v.getModel();
                            if (mm.equals(mmm))
                                t.getState().selectedStationIndex = i;
                        }
                    } else {
                        t.getState().selectedStationIndex = null;
                        core.app.player.control(null);
                    }
                } else {
                    int row = tableStations.getSelectedRow();
                    if (row != -1) {
                        t.getState().selectedStationIndex = row;
                        core.app.player.control(control);
                    } else {
                        t.getState().selectedStationIndex = null;
                        core.app.player.control(null);
                    }
                }
            }
        });

        tableStations.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() != 2)
                    return;

                controlPlay();
            }
        });
        final JPanel dow = new JPanel();
        final JPanel dow2 = new JPanel(new BorderLayout());
        dow.setLayout(new BorderLayout(0, 0));
        JScrollPane scroll2 = new JScrollPane(tableStations);
        scroll2.setPreferredSize(new Dimension(300, 300));
        dow2.add(scroll2);
        JTextField qsStationText = new JTextField();
        dow2.add(qsStationText, BorderLayout.SOUTH);
        dow.add(dow2);

        qsStations = new QuickSearchTable(tableStations, qsStationText) {
            @Override
            public int indexOf(DefaultTableModel m, Object o) {
                Object[] oo = (Object[]) o;
                SHOUTstationView vv = (SHOUTstationView) oo[0];
                SHOUTstationModel mmm = (SHOUTstationModel) vv.getModel();

                for (int i = 0; i < m.getRowCount(); i++) {
                    SHOUTstationView v = (SHOUTstationView) m.getValueAt(i, 0);
                    SHOUTstationModel mm = (SHOUTstationModel) v.getModel();
                    if (mm.equals(mmm))
                        return i;
                }

                return -1;
            }

            @Override
            public void notifyQuickEnter() {
            }

            @Override
            public Object getRow(DefaultTableModel m, int i) {
                SHOUTstationView v = (SHOUTstationView) m.getValueAt(i, 0);
                SHOUTstationModel mm = (SHOUTstationModel) v.getModel();
                return SHOUTstationView.toTreeModel(mm);
            }

            @Override
            public void doParentLayout() {
                // dow2.invalidate();
                dow2.doLayout();
            }

            @Override
            public void addRow(DefaultTableModel m, Object o) {
                m.addRow((Object[]) o);
            }

            @Override
            public DefaultTableModel createModel() {
                return SHOUTstationView.createTreeModel();
            }

            @Override
            public String toString(Object o) {
                Object[] oo = (Object[]) o;
                return SHOUTstationView.fromTreeModel(oo).getStation().getName();
            }

            @Override
            public void notifySearch() {
                updateStationsWidth(tableStations);
            }

            @Override
            public void notifyQuickExit() {
                updateStationsWidth(tableStations);
            }

        };
        splitPane.setRightComponent(dow);
    }

    @Override
    public void close() {
    }

    @Override
    public void activated() {
        t.refresh();

        if (t.getState().selectedGenreIndex != null) {
            tableGenres.setRowSelectionInterval(t.getState().selectedGenreIndex, t.getState().selectedGenreIndex);
            Rectangle r = tableGenres.getCellRect(t.getState().selectedGenreIndex, 0, true);
            tableGenres.scrollRectToVisible(r);
        }
        if (t.getState().selectedStationIndex != null) {
            tableStations.setRowSelectionInterval(t.getState().selectedStationIndex, t.getState().selectedStationIndex);
            Rectangle r = tableStations.getCellRect(t.getState().selectedStationIndex, 0, true);
            tableStations.scrollRectToVisible(r);
        }

        if (getPlayObject() != null)
            core.app.player.control(control);
    }

    @Override
    public void deactivated() {
        core.app.player.control(null);
    }

    void controlStop() {
        core.app.player.controlStop();
    }

    SHOUTgenreModel genre;

    public void notifyStationChanged(SHOUTstationModel g) {
        genre.notifyStationChanged(g);

        DefaultTableModel model = (DefaultTableModel) tableStations.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            SHOUTstationView t = (SHOUTstationView) model.getValueAt(i, 0);
            SHOUTstationModel m = (SHOUTstationModel) t.getModel();

            if (m.equals(g)) {
                Object[] oo = SHOUTstationView.toTreeModel(g);
                for (int c = 0; c < model.getColumnCount(); c++) {
                    model.setValueAt(oo[c], i, c);
                }
                return;
            }
        }
    }

    PlayObject getPlayObject() {
        int row = tableStations.getSelectedRow();
        if (row == -1)
            return null;

        final SHOUTstationView v = (SHOUTstationView) tableStations.getValueAt(row, 0);

        return control((SHOUTstationModel) v.getModel());
    }

    void controlPlay() {

        core.app.player.play(getPlayObject());
    }

    PlayObject control(final SHOUTstationModel station) {
        return new PlayObject() {
            @Override
            public URL getURL() {
                return station.getStation().getURL();
            }

            @Override
            public String getName() {
                return station.getStation().getName();
            }

            @Override
            public void notifyStart() {
                station.play = true;
                notifyStationChanged(station);
            }

            @Override
            public void notifyStop() {
                station.play = false;
                notifyStationChanged(station);
            }

        };
    }

    void updateStationsWidth(JTable table) {
        for (int column = 0; column < table.getColumnCount(); column++) {
            int columnWidth = 0;
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                columnWidth = Math.max(comp.getPreferredSize().width, columnWidth);
            }

            table.getColumnModel().getColumn(column).setPreferredWidth(columnWidth);
        }
    }

    void updateStation(JTable table, int row) {
        int columnWidth = 0;

        for (int column = 0; column < table.getColumnCount(); column++) {
            columnWidth = table.getColumnModel().getColumn(column).getPreferredWidth();

            TableCellRenderer renderer = table.getCellRenderer(row, column);
            Component comp = table.prepareRenderer(renderer, row, column);
            columnWidth = Math.max(comp.getPreferredSize().width, columnWidth);

            table.getColumnModel().getColumn(column).setPreferredWidth(columnWidth);
        }
    }

    public int getSelectedGenreIndex() {
        int row = tableGenres.getSelectedRow();
        return row;
    }
}
