package com.google.code.mircle;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.google.code.mircle.core.transfers.direct.LinkLabel;

public class DownloadProgressDialog extends JDialog {

    private final JButton btnNewButton_1 = new JButton("Stop");

    public String url;

    public JTextArea lblValue;
    public LinkLabel lblUrl;

    public DownloadProgressDialog() {
        final DownloadProgressDialog that = this;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);

        setBounds(100, 100, 467, 315);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 1.0 };
        getContentPane().setLayout(gridBagLayout);
        GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
        gbc_btnNewButton_1.gridx = 2;
        gbc_btnNewButton_1.gridy = 4;
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                that.dispose();
            }
        });

        JLabel lblProgress = new JLabel("Progress");
        GridBagConstraints gbc_lblProgress = new GridBagConstraints();
        gbc_lblProgress.insets = new Insets(0, 0, 5, 5);
        gbc_lblProgress.gridx = 0;
        gbc_lblProgress.gridy = 1;
        getContentPane().add(lblProgress, gbc_lblProgress);
        
        lblUrl = new LinkLabel("url");
        GridBagConstraints gbc_lblUrl = new GridBagConstraints();
        gbc_lblUrl.insets = new Insets(0, 0, 5, 5);
        gbc_lblUrl.gridx = 1;
        gbc_lblUrl.gridy = 2;
        getContentPane().add(lblUrl, gbc_lblUrl);

        lblValue = new JTextArea("Value");
        GridBagConstraints gbc_lblValue = new GridBagConstraints();
        gbc_lblValue.insets = new Insets(0, 0, 5, 5);
        gbc_lblValue.gridx = 1;
        gbc_lblValue.gridy = 3;
        getContentPane().add(lblValue, gbc_lblValue);
        getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
    }

    public boolean modal(Component root) {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(SwingUtilities.getRoot(root));
        setVisible(true);

        return url != null;
    }

    void download(String url) {
        this.url = url;
    }

}
