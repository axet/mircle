package com.google.code.mircle.plugins.subscriptions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.TransferHandler;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;

import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.google.code.mircle.AppError;
import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.ModelRename;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginContent;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.EchoRuDownload;
import com.google.code.mircle.plugins.subscriptions.controller.RSSFolderController;
import com.google.code.mircle.plugins.subscriptions.controller.RSSView;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolder;
import com.google.code.mircle.plugins.subscriptions.model.RSSItem;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;
import com.google.code.mircle.plugins.subscriptions.view.RSSFolderView;
import com.google.code.mircle.plugins.subscriptions.view.RSSModelView;
import com.sun.syndication.io.FeedException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class SubscriptionsPlugin extends RSSFolder implements Plugin, Model, PluginRoot, PluginContent {

    // all RSSFolders and RSSModels here. no hierarhy.
    ArrayList<Worker> models = new ArrayList<Worker>();

    Core core;

    // count current downloads
    int count = 0;

    // runtime folders map
    HashMap<Long, RSSFolder> foldersMap = new HashMap<Long, RSSFolder>();
    long foldersNextIndex = 0;

    // max amount of concurent downloads
    static final int MAX_CONCURRENT_DOWNLOADS = 3;

    // try to refresh every minute
    public final static int REFRESH_EVERY = 1 * 60 * 1000;

    // refresh feed every XX minutes
    public final static int REFRESH_FEED_EVERY = 10 * 60 * 1000;

    // max auto downloaded items for a one feed
    public final static int AUTO_DOWNLOAD_MAX = 3;

    @XStreamAlias("State")
    public static class State {
        Long version = new Long(1);
        ArrayList<Object[]> states;
    }

    public SubscriptionsPlugin(Core core) {
        this.core = core;
    }

    void timer() {
        Core.invoke(REFRESH_EVERY, new Runnable() {
            @Override
            public void run() {
                refresh();

                timer();
            }
        }, "RSS Refresh");
    }

    public void close() {
        for (Worker t : models) {
            t.close();
        }
    }

    void newFolder(RSSFolder parent, String name) {
        RSSFolder f = new RSSFolder();
        f.getState().name = name;
        f.getState().id = foldersNextIndex;
        foldersNextIndex++;
        models.add(0, f);

        foldersMap.put(f.getFolderId(), f);

        parent.addModel(0, f);
    }

    void downloadDialog(RSSFolder selected, RSSFolder m) {
        try {
            NewRSSDialog dialog = new NewRSSDialog();
            if (dialog.modal(core.app.appGUI.main)) {
                DownloadProgressThread progress = new DownloadProgressThread(core);
                progress.modal(dialog.url, selected, core.app.appGUI.main);
            }
        } catch (DownloadError ee) {
            AppError.modalError(ee);
        }
    }

    public JPopupMenu contextMenu(final Model m) {
        JPopupMenu popup = new JPopupMenu();

        if (m instanceof RSSFolder) {
            JMenuItem download = new JMenuItem("New Subscription");
            download.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    downloadDialog((RSSFolder) core.app.appSwitch.getSelectedModel(), (RSSFolder) m);
                }
            });
            popup.add(download);
        }

        if (m instanceof RSSFolder) {
            JMenuItem item = new JMenuItem("New Folder");
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    NewFolderDialog d = new NewFolderDialog();
                    if (d.modal(core.app.appGUI.main)) {
                        newFolder((RSSFolder) core.app.appSwitch.getSelectedModel(), d.folderName);
                    }
                }
            });
            popup.add(item);
        }

        if (!(m instanceof SubscriptionsPlugin)) {
            JMenuItem item = new JMenuItem("Rename");
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Model m = core.app.appSwitch.getSelectedModel();
                    if (m instanceof ModelRename)
                        core.app.appGUI.rename((ModelRename) m);
                }
            });
            popup.add(item);
        }

        if (!(m instanceof SubscriptionsPlugin)) {
            JMenuItem delete = new JMenuItem("Delete");
            delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    Model m = core.app.appSwitch.getSelectedModel();
                    if (m instanceof RSSItem) {
                        remove((RSSItem) m);
                    }
                }
            });
            popup.add(delete);
        }

        return popup;
    }

    void remove(RSSItem m) {
        models.remove(m);
        RSSFolder f = getFolder(m);
        f.removeModel(m);
    }

    public Controller createController(Model m) {
        Worker w = (Worker) m;

        if (w instanceof RSSFolder)
            return new RSSFolderController(core, (RSSFolder) w);

        if (w instanceof RSSModel)
            return new RSSView(core, (RSSModel) w);

        return null;
    }

    public RSSModel rss(URL url) {
        RSSModel w = new RSSModel(core, url);

        return w;
    }

    public RSSModel rss(URL www, URL url) {
        RSSModel w = new RSSModel(core, www, url);

        return w;
    }

    public RSSModel exist(final RSSModel w) {
        for (Worker m : models) {
            if (m instanceof RSSModel) {
                RSSModel t = (RSSModel) m;

                URL t1 = t.getState().rssSource;
                URL t2 = t.getState().source;
                URL w1 = w.getState().rssSource;
                URL w2 = w.getState().source;

                if (t1.equals(w1) || t1.equals(w2) || t2.equals(w1) || t2.equals(w2))
                    return t;
            }
        }

        return null;
    }

    /**
     * run each rss, and update if required
     */
    public void refresh() {
        for (Worker w : models) {
            if (w instanceof RSSModel) {
                RSSModel m = (RSSModel) w;

                // do not run twice
                if (m.isAlive())
                    continue;

                // not yet extracted. do so.
                if (!m.extracted()) {
                    start(m);
                    continue;
                }

                // do not refresh it too often
                Date date = DateUtils.addMilliseconds(m.getState().refresh, REFRESH_FEED_EVERY);
                if (date.before(new Date())) {
                    start(m);
                    continue;
                }
            }
        }
    }

    RSSFolder getFolder(RSSItem m) {
        Long id = m.getParentFolderId();
        RSSFolder f;
        if (id == null)
            f = this;
        else
            f = foldersMap.get(id);
        return f;
    }

    public void folderChanged(RSSModel m) {
        RSSFolder parent = getFolder(m);
        parent.modelChanged(m);
    }

    public void start(final Worker t) {
        if (count >= MAX_CONCURRENT_DOWNLOADS) {
            t.putToQueue();
            folderChanged((RSSModel) t);
            return;
        }

        t.addListener(new LongRun.Listener() {
            @Override
            public void stop() {
                final RSSModel.Listener that = this;

                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        t.removeListener(that);
                        count--;
                        startNext();
                    }
                });
            }

            @Override
            public void start() {
            }

            @Override
            public void changed() {
            }

            @Override
            public void close() {
            }
        });

        count++;
        t.start();
        folderChanged((RSSModel) t);
    }

    public void startNext() {
        for (Worker t : new ArrayList<Worker>(models)) {
            if (t.getState().getDownloadState() == DownloadState.DOWNLOAD_QUEUED) {
                start(t);

                // stop tagging tasks only if here is a limit of downloads
                if (count >= MAX_CONCURRENT_DOWNLOADS)
                    return;
            }
        }
    }

    @Override
    public void save(File path) {
        try {
            State state = new State();
            state.states = new ArrayList<Object[]>();
            for (Worker t : models) {
                state.states.add(new Object[] { t.getClass().getName(), t.save() });
            }
            core.save(state, xstream(), FileUtils.getFile(path, "subscriptions.xml"));
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    XStream xstream() {
        XStream stream = new XStream();
        stream.processAnnotations(new Class[] { State.class, RSSModel.State.class, EchoRuDownload.class,
                DownloadInfo.class, RSSFolder.State.class });
        return stream;
    }

    @Override
    public void load(File path) {
        try {
            XStream s = xstream();
            State o = (State) s.fromXML(FileUtils.getFile(path, "subscriptions.xml"));

            ArrayList<Object[]> list = (ArrayList<Object[]>) o.states;
            for (Object[] oo : list) {
                String name = (String) oo[0];
                Worker.State state = (Worker.State) oo[1];

                Class<?> klass = Class.forName((String) name);
                final Worker w;

                if (klass.isAssignableFrom(RSSFolder.class))
                    w = (Worker) klass.newInstance();
                else
                    w = (Worker) klass.getDeclaredConstructor(Core.class).newInstance(core);

                w.load(state);

                this.models.add(w);

                if (w instanceof RSSFolder) {
                    RSSFolder f = (RSSFolder) w;
                    long id = f.getState().id;

                    {
                        RSSFolder mf = foldersMap.get(id);
                        if (mf != null) {
                            f.merge(mf);
                            mf.close();
                        }
                    }
                    foldersMap.put(id, f);

                    if (foldersNextIndex <= id)
                        foldersNextIndex = id + 1;

                    Long parent = f.getState().folderId;
                    if (parent != null) {
                        RSSFolder mf = foldersMap.get(parent);
                        if (mf == null) {
                            mf = new RSSFolder();
                            mf.getState().id = parent;
                            foldersMap.put(parent, mf);
                        }
                        mf.addModel(f);
                    } else {
                        addModel(f);
                    }
                }

                if (w instanceof RSSModel) {
                    RSSModel m = (RSSModel) w;
                    Long id = m.getParentFolderId();
                    if (id != null) {
                        RSSFolder mf = foldersMap.get(id);
                        if (mf == null) {
                            mf = new RSSFolder();
                            mf.getState().id = id;
                            foldersMap.put(id, mf);
                        }
                        mf.addModel(m);
                    } else {
                        addModel(m);
                    }
                }

                DownloadState ds = w.getState().getDownloadState();

                if (ds != null) {
                    switch (ds) {
                    case DOWNLOAD_START:
                    case DOWNLOAD_QUEUED:
                        start(w);
                    default:
                        // ignore
                    }
                }
            }
        } catch (com.thoughtworks.xstream.io.StreamException stream) {
            try {
                throw stream.getCause();
            } catch (FileNotFoundException ignore) {
                // skip file not found
            } catch (EOFException ignore) {
                // empty file
            } catch (Throwable e) {
                throw new com.thoughtworks.xstream.io.StreamException(e);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        timer();
    }

    @Override
    public Model extract(DownloadInfo info) {
        if (RSSModel.handle(info))
            return rss(info.getSource());

        return null;
    }

    public void add(RSSModel w) {
        add(w, this);
    }

    public void add(RSSModel w, RSSFolder parent) {
        models.add(0, w);

        parent.addModel(0, w);

        refresh();
    }

    @Override
    public Model extract(DownloadInfo info, String html) {
        // 1) feed burner (and alike) support
        String ct = info.getContentType().toLowerCase();
        if (ct.contains("text/xml") || ct.contains("application/xml")) {
            try {
                RSSModel m = new RSSModel(core, info.getSource(), html);
                return m;
            } catch (FeedException e) {
                // ignore, it is not an rss feed
            }
        }

        // 2) echo msk support, handle "http://echo.msk.ru/programs/..."
        if (info.getSource().getHost().equals("echo.msk.ru") && info.getSource().getPath().startsWith("/programs/")) {
            List<URL> rss = RSSModel.extractRSS(info.getSource(), html);
            for (URL u : rss) {
                if (u.getPath().contains("rss-audio"))
                    return core.rss.rss(info.getSource(), u);
            }
            return null;
        }

        // 3) rss link
        List<URL> rss = RSSModel.extractRSS(info.getSource(), html);
        if (rss.size() != 0)
            return core.rss.rss(info.getSource(), rss.get(0));

        return null;
    }

    @Override
    public View createView(Model m) {
        if (m instanceof SubscriptionsPlugin)
            return new SubscriptionsPluginView((SubscriptionsPlugin) m);
        if (m instanceof RSSModel)
            return new RSSModelView((RSSModel) m);
        if (m instanceof RSSFolder)
            return new RSSFolderView((RSSFolder) m);
        return null;
    }

    @Override
    public int dndAction(Model m) {
        if (m instanceof RSSItem)
            return TransferHandler.MOVE;
        return TransferHandler.NONE;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        if (source instanceof RSSItem) {
            if (target instanceof RSSFolder)
                return true;
            if (target instanceof SubscriptionsPlugin)
                return true;
        }
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        if (source instanceof RSSItem && target instanceof RSSFolder) {
            RSSItem m = (RSSItem) source;
            Long parentFolder = m.getParentFolderId();
            if (parentFolder == null) {
                removeModel(m);
            } else {
                RSSFolder f = foldersMap.get(parentFolder);
                f.removeModel(m);
            }

            RSSFolder t = (RSSFolder) target;
            t.addModel(0, m);
        }
        if (source instanceof RSSItem && target instanceof SubscriptionsPlugin) {
            RSSItem m = (RSSItem) source;
            Long parentFolder = m.getParentFolderId();
            if (parentFolder == null) {
                removeModel(m);
            } else {
                RSSFolder f = foldersMap.get(parentFolder);
                f.removeModel(m);
            }

            addModel(0, m);
        }
        return false;
    }
}
