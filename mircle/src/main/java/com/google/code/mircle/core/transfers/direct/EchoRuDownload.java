package com.google.code.mircle.core.transfers.direct;

import java.io.File;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.github.axet.wget.info.ex.DownloadRetry;
import com.google.code.mircle.core.Worker;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author axet
 * 
 */
public class EchoRuDownload extends DirectDownload {

    @XStreamAlias("EchoRuDownloadState")
    static class State extends DirectDownload.State {
        boolean parsed;
    }

    State state = new State();

    public EchoRuDownload(URL source, File target) {
        try {
            super.state = state;

            this.state.setInfo(new DownloadInfo(source));
            this.state.setTargetUser(target);
            this.state.setWeb(source);
        } catch (DownloadRetry e) {
            state.setException(e);
        } catch (DownloadError e) {
            state.setException(e);
        }
    }

    public boolean checkSource(Worker w) {
        if (w instanceof EchoRuDownload) {
            EchoRuDownload ww = (EchoRuDownload) w;

            return ww.state.getWeb().equals(this.state.getWeb());
        }

        return super.checkSource(w);
    }

    protected EchoRuDownload() {
    }

    @Override
    public Transfer.State getState() {
        return super.getState();
    }

    @Override
    public void load(Worker.State s) {
        super.load(s);

        state = (State) s;
    }

    public static boolean handle(String url) {
        Pattern p = Pattern.compile("http[s]*://echo.msk.ru/programs/(.+)/(.+)");
        Matcher m = p.matcher(url);
        if (m.find()) {
            return true;
        }

        return false;
    }

    @Override
    public void run() {
        this.state.getInfo().extract(stop, new Sync());

        while (!state.parsed) {
            try {
                String str = WGet.getHtml(state.getInfo().getSource(), new WGet.HtmlLoader() {
                    @Override
                    public void notifyRetry(int arg0, Throwable arg1) {
                        changed();
                    }

                    @Override
                    public void notifyDownloading() {
                        changed();
                    }

                    @Override
                    public void notifyMoved() {
                        changed();
                    }
                }, stop);

                // then stop been caled
                if (str == null)
                    return;

                Pattern p = Pattern.compile("(http[s]*://cdn.echo.msk.ru/snd/[^\"]*)");
                Matcher m = p.matcher(str);
                if (!m.find())
                    throw new RuntimeException("download not found");

                state.setInfo(new DownloadInfo(new URL(m.group(1))));
                state.getInfo().extract(stop, new Sync());

                state.parsed = true;
            } catch (DownloadRetry e) {
                state.setException(e);
                retry();
            } catch (DownloadError e) {
                state.setException(e);
                return;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        changed();

        super.run();
    }

}
