package com.google.code.mircle.plugins.subscriptions.model;

import com.google.code.mircle.core.ModelRename;
import com.google.code.mircle.core.TreeItemModel;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.Transfer;

public abstract class RSSItem extends Worker implements TreeItemModel, FeedListener, ModelRename {

    public abstract Long getParentFolderId();

    public abstract void setParentFolderId(Long l);

    public abstract Transfer getTransfer(FeedEntry e);

    public abstract void notifyChanged(FeedEntry e);

    public abstract void download(FeedEntry e);

    public DownloadState getDownloadState() {
        return getState().getDownloadState();
    }
}
