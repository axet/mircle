package com.google.code.mircle.plugins.subscriptions.controller;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.plugins.subscriptions.SubscriptionsPlugin;

public class SubsctiptionsView extends JPanel implements Controller {

    private static final long serialVersionUID = 1782714071455399785L;

    Core core;

    public SubsctiptionsView(final Core core, final SubscriptionsPlugin s) {
        this.core = core;

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 59, 55, 0 };
        gridBagLayout.rowHeights = new int[] { 28, 0, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }
}
