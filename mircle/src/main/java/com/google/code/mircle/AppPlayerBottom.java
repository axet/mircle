package com.google.code.mircle;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class AppPlayerBottom extends JPanel {
    private static final long serialVersionUID = 1L;

    JLabel lblNewLabel;
    JButton btnNewDownload;

    public AppPlayerBottom(final AppGUI app) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gridBagLayout.rowHeights = new int[] { 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        btnNewDownload = new JButton("Play");
        btnNewDownload.setEnabled(false);

        GridBagConstraints gbc_btnNewDownload = new GridBagConstraints();
        gbc_btnNewDownload.insets = new Insets(0, 0, 0, 5);
        gbc_btnNewDownload.gridx = 0;
        gbc_btnNewDownload.gridy = 0;
        add(btnNewDownload, gbc_btnNewDownload);

        lblNewLabel = new JLabel("New label");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
        gbc_lblNewLabel.gridx = 1;
        gbc_lblNewLabel.gridy = 0;
        add(lblNewLabel, gbc_lblNewLabel);

        JProgressBar progressBar = new JProgressBar();
        GridBagConstraints gbc_progressBar = new GridBagConstraints();
        gbc_progressBar.insets = new Insets(0, 0, 0, 5);
        gbc_progressBar.gridx = 2;
        gbc_progressBar.gridy = 0;
        add(progressBar, gbc_progressBar);

    }
}
