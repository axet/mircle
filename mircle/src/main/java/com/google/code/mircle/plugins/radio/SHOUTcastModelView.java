package com.google.code.mircle.plugins.radio;

import com.github.axet.shoutcast.SHOUTcast;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class SHOUTcastModelView implements View {

    SHOUTcastModel t;

    public SHOUTcastModelView(SHOUTcastModel t) {
        this.t = t;
    }

    public String toString() {
        String str = "";

        switch (t.getSt()) {
        case DONE:
            break;
        case ERROR:
            str += "E ";
            break;
        case LOAD_GENRES:
            str += "R";
            SHOUTcast c = t.c;
            switch (c.getState()) {
            case RETRY:
                str += "[RETRY:" + t.c.getDelay() + "]";
                break;
            case EXTRACTING_MAIN:
                str += "[MAIN:" + t.c.getGenres().size() + "]";
                break;
            case EXTRACTING_SUB:
                str += "[SUB:" + t.c.getGenres().size() + "]";
                break;
            case DONE:
                break;
            }
            break;
        case LOAD_STATIONS:
            str += "R ";
            break;
        }

        str += "SHOUTcast";

        return str;
    }

    @Override
    public Model getModel() {
        return t;
    }

    @Override
    public void close() {
    }

}
