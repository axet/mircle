package com.google.code.mircle.plugins.radio;

import java.net.URL;

import com.github.axet.shoutcast.SHOUTstation;
import com.google.code.mircle.core.Model;

public class SHOUTstationModel implements Model {

    SHOUTstation station;

    private boolean favorite;

    transient SHOUTgenreModel genre;

    transient boolean play;

    public SHOUTstationModel(SHOUTgenreModel genre, SHOUTstation s) {
        station = s;
        this.genre = genre;
    }

    @Override
    public void close() {
    }

    public SHOUTstation getStation() {
        return station;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean equals(Object o) {
        URL u1 = getStation().getURL();
        URL u2 = ((SHOUTstationModel) o).getStation().getURL();
        return u1.equals(u2);
    }

}
