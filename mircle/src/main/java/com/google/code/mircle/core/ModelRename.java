package com.google.code.mircle.core;

/**
 * Model's who support renames (RSSFolder, RSSItem) should implement this
 * interface
 * 
 * @author axet
 * 
 */
public interface ModelRename {

    public String getName();

    public void setName(String name);

}
