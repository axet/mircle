package com.google.code.mircle;

import java.awt.BorderLayout;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class AppError {

    static class EditableListPanel extends JPanel {
        private static final long serialVersionUID = 785927898208696335L;

        EditableListPanel(Throwable e) {
            super(new BorderLayout());

            JTextArea text = new JTextArea();
            text.setEditable(false);
            text.setText(ExceptionUtils.getStackTrace(e));
            text.setBackground(getBackground());
            add(text, BorderLayout.CENTER);
        }
    }

    public static void fatalError(Thread t, Throwable e) {
        save(e);

        e.printStackTrace();
        JOptionPane.showMessageDialog(null, new EditableListPanel(e), "Fatal eror", JOptionPane.ERROR_MESSAGE);
    }

    public static void modalError(Throwable e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(null, new EditableListPanel(e), "Eror", JOptionPane.INFORMATION_MESSAGE);
    }

    public static File savePath = null;

    public static void save(Throwable e) {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(b);
            e.printStackTrace(ps);
            ps.close();
            b.flush();
            String str = b.toString("UTF-8");
            if (savePath != null) {
                File report = FileUtils.getFile(savePath, "report." + new Date().getTime() + ".txt");
                FileUtils.writeStringToFile(report, str);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
