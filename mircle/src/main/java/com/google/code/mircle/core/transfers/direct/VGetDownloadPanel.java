package com.google.code.mircle.core.transfers.direct;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.io.FileUtils;

import com.github.axet.wget.info.DownloadInfo;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class VGetDownloadPanel extends JPanel {

    Core core;
    Plugin plugin;

    Transfer t;

    JLabel lblNewLabel_1 = new JLabel("false");
    JLabel lblNewLabel_3 = new JLabel("New label");
    JLabel lblNewLabel_4 = new JLabel("New label");
    JButton btnDeleteFile = new JButton("Delete File");
    private final JLabel lblQuality = new JLabel("Quality");
    private final JLabel lblNewLabel_5 = new JLabel("New label");
    GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
    private final JLabel lblWeb = new JLabel("Web");
    private final LinkLabel lblWeb_1 = new LinkLabel("Web URL");
    private final JLabel lblCurrent = new JLabel("Current");
    private final JLabel lblTotal = new JLabel("Total");
    private final JLabel lblCurrentValue = new JLabel("New label");
    private final JLabel lblTotalValue = new JLabel("New label");
    private final ProgressPanel progress = new ProgressPanel();

    public VGetDownloadPanel(Core c, Plugin p) {
        this.core = c;
        this.plugin = p;

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0 };
        setLayout(gridBagLayout);

        JLabel lblDirectDownload = new JLabel();
        lblDirectDownload.setText("YouTube Download");
        GridBagConstraints gbc_lblDirectDownload = new GridBagConstraints();
        gbc_lblDirectDownload.insets = new Insets(0, 0, 5, 5);
        gbc_lblDirectDownload.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDirectDownload.gridx = 0;
        gbc_lblDirectDownload.gridy = 0;
        add(lblDirectDownload, gbc_lblDirectDownload);

        btnDeleteFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Transfer.State s = (State) t.getState();
                FileUtils.deleteQuietly(s.getTargetApp());
            }
        });
        GridBagConstraints gbc_btnDeleteFile = new GridBagConstraints();
        gbc_btnDeleteFile.insets = new Insets(0, 0, 5, 5);
        gbc_btnDeleteFile.gridx = 2;
        gbc_btnDeleteFile.gridy = 0;
        add(btnDeleteFile, gbc_btnDeleteFile);

        progress.setPreferredSize(new Dimension(100, 100));

        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 3;
        gbc_panel.gridy = 0;
        add(progress, gbc_panel);

        JLabel lblNewLabel = new JLabel("Done?:");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 1;
        add(lblNewLabel, gbc_lblNewLabel);

        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_1.gridx = 1;
        gbc_lblNewLabel_1.gridy = 1;
        add(lblNewLabel_1, gbc_lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel("targetFile");
        GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
        gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_2.gridx = 0;
        gbc_lblNewLabel_2.gridy = 2;
        add(lblNewLabel_2, gbc_lblNewLabel_2);

        GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
        gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_3.gridx = 1;
        gbc_lblNewLabel_3.gridy = 2;
        add(lblNewLabel_3, gbc_lblNewLabel_3);

        JLabel lblTitle = new JLabel("Title");
        GridBagConstraints gbc_lblTitle = new GridBagConstraints();
        gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
        gbc_lblTitle.gridx = 0;
        gbc_lblTitle.gridy = 3;
        add(lblTitle, gbc_lblTitle);

        GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
        gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_4.gridx = 1;
        gbc_lblNewLabel_4.gridy = 3;
        add(lblNewLabel_4, gbc_lblNewLabel_4);

        GridBagConstraints gbc_lblQuality = new GridBagConstraints();
        gbc_lblQuality.insets = new Insets(0, 0, 5, 5);
        gbc_lblQuality.gridx = 0;
        gbc_lblQuality.gridy = 4;
        add(lblQuality, gbc_lblQuality);

        gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_5.gridx = 1;
        gbc_lblNewLabel_5.gridy = 4;
        add(lblNewLabel_5, gbc_lblNewLabel_5);

        GridBagConstraints gbc_lblWeb = new GridBagConstraints();
        gbc_lblWeb.insets = new Insets(0, 0, 5, 5);
        gbc_lblWeb.gridx = 0;
        gbc_lblWeb.gridy = 5;
        add(lblWeb, gbc_lblWeb);

        GridBagConstraints gbc_lblWeb_1 = new GridBagConstraints();
        gbc_lblWeb_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblWeb_1.gridx = 1;
        gbc_lblWeb_1.gridy = 5;
        add(lblWeb_1, gbc_lblWeb_1);

        GridBagConstraints gbc_lblCurrent = new GridBagConstraints();
        gbc_lblCurrent.insets = new Insets(0, 0, 5, 5);
        gbc_lblCurrent.gridx = 0;
        gbc_lblCurrent.gridy = 6;
        add(lblCurrent, gbc_lblCurrent);

        GridBagConstraints gbc_lblCurrentValue = new GridBagConstraints();
        gbc_lblCurrentValue.insets = new Insets(0, 0, 5, 5);
        gbc_lblCurrentValue.gridx = 1;
        gbc_lblCurrentValue.gridy = 6;
        add(lblCurrentValue, gbc_lblCurrentValue);

        GridBagConstraints gbc_lblTotal = new GridBagConstraints();
        gbc_lblTotal.insets = new Insets(0, 0, 0, 5);
        gbc_lblTotal.gridx = 0;
        gbc_lblTotal.gridy = 7;
        add(lblTotal, gbc_lblTotal);

        GridBagConstraints gbc_lblTotalValue = new GridBagConstraints();
        gbc_lblTotalValue.insets = new Insets(0, 0, 0, 5);
        gbc_lblTotalValue.gridx = 1;
        gbc_lblTotalValue.gridy = 7;
        add(lblTotalValue, gbc_lblTotalValue);

    }

    public void setTransfer(Transfer t) {
        this.t = t;

        VGetDownload dd = (VGetDownload) t;

        VGetDownload.State state = dd.getState();

        if (dd.getException() != null)
            lblNewLabel_1.setText(dd.getException().toString());
        else
            lblNewLabel_1.setText("" + (dd.state.getDownloadState() == DownloadState.DOWNLOAD_COMPLETE));

        if (state.getTargetApp() != null)
            lblNewLabel_3.setText(state.getTargetApp().toString());
        else
            lblNewLabel_3.setText("");

        if (dd.getTarget() != null)
            btnDeleteFile.setEnabled(dd.getTarget().exists());
        else
            btnDeleteFile.setEnabled(false);

        lblWeb_1.setText(dd.getSource());

        lblNewLabel_4.setText(state.getInfo().getTitle());

        if (state.getInfo().getVq() != null)
            lblNewLabel_5.setText(state.getInfo().getVq().toString());
        else
            lblNewLabel_5.setText("");

        DownloadInfo di = state.getInfo().getInfo();

        if (di != null) {
            lblCurrentValue.setText(Long.toString(di.getCount()));
            lblTotalValue.setText(di.getLength() == null ? "" : Long.toString(di.getLength()));

            progress.update(di);
        }
    }
}
