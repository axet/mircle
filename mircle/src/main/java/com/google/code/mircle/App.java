package com.google.code.mircle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.io.FileUtils;

import com.github.axet.desktop.Desktop;
import com.github.axet.desktop.DesktopPower;
import com.github.axet.desktop.DesktopSysTray;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.StreamException;

public class App {
    public AppGUI appGUI;
    public AppSwitch appSwitch;

    Core core;

    public GlobalPlayer player;

    State state = new State();

    public DesktopSysTray sys = Desktop.getDesktopSysTray();
    JPopupMenu menu = new JPopupMenu();
    Icon favicon;

    public DesktopPower power = Desktop.getDesktopPower();

    @XStreamAlias("AppState")
    static class State {
        Long version = new Long(1);
        String model;
    }

    public App() {
        core = new Core(this);
        appGUI = new AppGUI(core);
        appSwitch = new AppSwitch(core, appGUI);

        registerUnhalted();

        appGUI.createWindow();
        appSwitch.loadPlugins();

        player = new GlobalPlayer(appGUI);

        core.load();

        createMenu();

        power.addListener(new DesktopPower.Listener() {
            @Override
            public void quit() {
                closeNoDispose();
            }
        });

        if (OSX.isMac())
            OSX.register(this);
    }

    void createMenu() {
        try {
            BufferedImage bmp = ImageIO.read(Core.class.getResourceAsStream("/favicon.png"));

            favicon = new ImageIcon(bmp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        JMenuItem itemShow = new JMenuItem("Show Main Window");
        itemShow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        appGUI.show();
                    }
                });
            }
        });
        menu.add(itemShow);

        JMenuItem itemQuit = new JMenuItem("Quit Mircle");
        itemQuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        close();
                    }
                });
            }
        });
        menu.add(itemQuit);

        sys.setIcon(favicon);
        sys.setTitle("Mircle");
        sys.setMenu(menu);

        sys.addListener(new DesktopSysTray.Listener() {
            @Override
            public void mouseLeftDoubleClick() {
            }

            @Override
            public void mouseLeftClick() {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        if (!appGUI.isVisible())
                            appGUI.show();
                        else
                            appGUI.hide();
                    }
                });
            }
        });

        sys.show();
    }

    void registerUnhalted() {
        UncaughtExceptionHandler u = new UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                AppError.fatalError(t, e);
                System.exit(1);
            }
        };

        Thread.setDefaultUncaughtExceptionHandler(u);
    }

    public void mircle(String url) {
        DownloadProgressThread progress = new DownloadProgressThread(core);
        progress.modal(url, appGUI.main);
    }

    public void closeNoDispose() {
        appGUI.hide();

        if (OSX.isMac())
            OSX.unregister();

        sys.close();

        power.close();

        appSwitch.close();

        core.close();

        appGUI.close();
    }

    public void close() {
        closeNoDispose();

        appGUI.dispose();
    }

    XStream xstream() {
        XStream stream = new XStream();
        stream.processAnnotations(new Class[] { State.class });
        return stream;
    }

    public void save(File f) {
        DefaultMutableTreeNode n = null;
        TreePath path = appGUI.left.getSelectionPath();
        if (path != null) {
            n = (DefaultMutableTreeNode) path.getLastPathComponent();

            View v = (View) n.getUserObject();
            Model m = v.getModel();

            state.model = m.getClass().getName();
        } else {
            state.model = null;
        }

        XStream stream = xstream();
        core.save(state, stream, FileUtils.getFile(f, "app.xml"));
    }

    @SuppressWarnings("unchecked")
    public void load(File f) {
        XStream stream = xstream();

        try {
            state = (State) stream.fromXML(FileUtils.getFile(f, "app.xml"));
        } catch (StreamException e) {
            return;
        }

        try {
            if (state.model != null) {
                appSwitch.modelSwitch((Class<? extends Model>) Class.forName(state.model));
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(final String[] arg) {
        try {
            App app = new App();

            app.appGUI.show();

            for (String s : arg) {
                app.mircle(s);
            }
        } catch (Exception e) {
            AppError.fatalError(Thread.currentThread(), e);
            System.exit(1);
        }
    }
}
