package com.google.code.mircle.core.transfers;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.google.code.mircle.core.Controller;

public class TransfersView extends JPanel implements Controller {

    public TransfersView(TransfersPlugin s) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 59, 55, 0 };
        gridBagLayout.rowHeights = new int[] { 28, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        GridBagConstraints gbc_lblDownloadFolder = new GridBagConstraints();
        gbc_lblDownloadFolder.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblDownloadFolder.insets = new Insets(0, 0, 5, 5);
        gbc_lblDownloadFolder.gridx = 0;
        gbc_lblDownloadFolder.gridy = 0;
        JLabel lblDownloadFolder = new JLabel("Download Folder or share");
        add(lblDownloadFolder, gbc_lblDownloadFolder);

        GridBagConstraints gbc_1 = new GridBagConstraints();
        gbc_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_1.anchor = GridBagConstraints.NORTH;
        gbc_1.insets = new Insets(0, 0, 5, 0);
        gbc_1.gridx = 1;
        gbc_1.gridy = 0;
        JTextField textField = new JTextField("value1");
        add(textField, gbc_1);

        {
            textField.setText(s.getDownloadFolder().toString());
        }
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }
}
