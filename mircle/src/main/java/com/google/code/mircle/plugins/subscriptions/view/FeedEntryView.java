package com.google.code.mircle.plugins.subscriptions.view;

import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.Worker.Status;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.core.transfers.direct.Transfer.State;
import com.google.code.mircle.plugins.subscriptions.model.FeedEntry;
import com.google.code.mircle.plugins.subscriptions.model.RSSItem;

public class FeedEntryView implements View {

    FeedEntry entry;
    RSSItem model;

    public FeedEntryView(Core core, RSSItem m, FeedEntry e) {
        entry = e;
        model = m;
    }

    public String toString() {
        String str = "";

        if (entry.direct) {
            Transfer transfer = model.getTransfer(entry);
            // has corresponding transfer?
            if (transfer != null) {
                switch (transfer.getState().getDownloadState()) {
                case DOWNLOAD_QUEUED:
                    str += "Q ";
                    break;
                case DOWNLOAD_STOP:
                    str += "S ";
                    break;
                case DOWNLOAD_START:
                    Status status = transfer.status();
                    if (status.total != null) {
                        str += String.format("%d%% / ", status.current * 100 / status.total);
                    } else {
                        str += status.current + " bytes / ";
                    }
                    break;
                case DOWNLOAD_FAILED:
                    str += "E ";
                    break;
                case DOWNLOAD_COMPLETE:
                    Transfer.State s = (State) transfer.getState();
                    if (!s.getDeleted()) {
                        str += "(downloaded) ";
                    } else {
                        // if it is delted, then feed should apear without any
                        // prefixes
                    }
                    break;
                default:
                    // here is no states left
                    break;
                }
            } else {
                // has direct and has no transfer attached?
                // probably it already deleted. so transfer sould apear without
                // any prefixes
            }
        } else {
            if (entry.newItem) {
                str += "(new) ";
            } else if (entry.unread) {
                str += "(unread) ";
            }
        }

        str += entry.title;

        str += " / " + entry.date;

        return str;
    }

    @Override
    public Model getModel() {
        return entry;
    }

    @Override
    public void close() {
    }

}