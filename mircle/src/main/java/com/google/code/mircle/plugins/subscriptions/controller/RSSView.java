package com.google.code.mircle.plugins.subscriptions.controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.transfers.direct.LinkLabel;
import com.google.code.mircle.plugins.subscriptions.model.FeedEntry;
import com.google.code.mircle.plugins.subscriptions.model.FeedListener;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;
import com.google.code.mircle.plugins.subscriptions.view.FeedEntryView;

public class RSSView extends JPanel implements Controller {
    private static final long serialVersionUID = -5843882598074012891L;

    class RSSFeedListView extends FeedListView {
        private static final long serialVersionUID = -9055061186316809042L;

        public RSSFeedListView(FeedListener listModel) {
            super(listModel);
        }

        @Override
        Object createView(Object m) {
            return new FeedEntryView(core, model, (FeedEntry) m);
        }

    }

    Core core;

    LinkLabel label = new LinkLabel();

    JPanel top;
    JButton loadMore;
    FeedListView feedListView;
    JScrollPane scroll;
    RSSModel model;

    JPanel splitTop;
    JSplitPane split;

    FeedDownView feedDownView;
    private JCheckBox chckbxAutoDownload;
    private JLabel lblunreadCount;
    private JButton btnMarkAllAs;

    public RSSView(Core c, RSSModel s) {
        super(new BorderLayout());

        core = c;

        model = s;

        feedDownView = new FeedDownView(core, model);

        feedDownView.setPreferredSize(new Dimension(0, 180));

        feedListView = new RSSFeedListView(model);

        feedListView.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++) {
                    if (feedListView.isSelectedIndex(i))
                        feedDownView.update((FeedEntryView) feedListView.getModel().getElementAt(i));
                }
            }
        });

        top = new JPanel(new GridBagLayout());

        lblunreadCount = new JLabel("(unread count)");
        GridBagConstraints gbc_lblunreadCount = new GridBagConstraints();
        gbc_lblunreadCount.insets = new Insets(0, 0, 0, 5);
        gbc_lblunreadCount.gridx = 0;
        gbc_lblunreadCount.gridy = 0;
        top.add(lblunreadCount, gbc_lblunreadCount);

        label.setText("Title");

        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.gridx = 1;
        gbc_label.gridy = 0;
        top.add(label, gbc_label);

        loadMore = new JButton();
        loadMore.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                feedListView.loadMore();
            }
        });
        loadMore.setText("Load More");
        GridBagConstraints gbc_loadMore = new GridBagConstraints();
        gbc_loadMore.insets = new Insets(0, 0, 0, 5);
        gbc_loadMore.gridx = 2;
        gbc_loadMore.gridy = 0;
        top.add(loadMore, gbc_loadMore);

        splitTop = new JPanel(new BorderLayout());
        splitTop.add(top, BorderLayout.NORTH);

        chckbxAutoDownload = new JCheckBox("Auto Download");
        chckbxAutoDownload.setSelected(model.getState().autoDownload);

        chckbxAutoDownload.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent arg0) {
                model.getState().autoDownload = arg0.getStateChange() == ItemEvent.SELECTED;

                if (model.getState().autoDownload) {
                    model.autoDownload();
                }
            }
        });

        GridBagConstraints gbc_chckbxAutoDownload = new GridBagConstraints();
        gbc_chckbxAutoDownload.insets = new Insets(0, 0, 0, 5);
        gbc_chckbxAutoDownload.gridx = 3;
        gbc_chckbxAutoDownload.gridy = 0;
        top.add(chckbxAutoDownload, gbc_chckbxAutoDownload);

        btnMarkAllAs = new JButton("Mark All As Read");
        btnMarkAllAs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                model.markAllAsRead();
            }
        });
        GridBagConstraints gbc_btnMarkAllAs = new GridBagConstraints();
        gbc_btnMarkAllAs.gridx = 4;
        gbc_btnMarkAllAs.gridy = 0;
        top.add(btnMarkAllAs, gbc_btnMarkAllAs);

        scroll = new JScrollPane(feedListView);

        splitTop.add(scroll, BorderLayout.CENTER);

        model.addListener(new LongRun.Listener() {
            @Override
            public void stop() {
            }

            @Override
            public void start() {
            }

            @Override
            public void changed() {
                update();
            }

            @Override
            public void close() {
            }
        });

        update();

        split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitTop, feedDownView);
        split.setResizeWeight(1);

        add(split);
    }

    void update() {
        lblunreadCount.setText("(" + model.getState().feed.getUnreadCount() + " / " + model.getState().entries.size()
                + ")");
        label.setURL(model.getState().feed.getTitle(), model.getState().source);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
        model.getState().feed.setNewCount(0);
        for (FeedEntry e : model.getState().entries) {
            e.newItem = false;
        }
    }

    @Override
    public void close() {
        deactivated();
    }
}
