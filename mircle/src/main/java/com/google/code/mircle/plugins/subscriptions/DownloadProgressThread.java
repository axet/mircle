package com.google.code.mircle.plugins.subscriptions;

import java.awt.Component;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.github.axet.wget.info.ex.DownloadInterruptedError;
import com.google.code.mircle.AppError;
import com.google.code.mircle.DownloadProgressDialog;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.transfers.direct.DirectDownload;
import com.google.code.mircle.core.transfers.direct.DirectView;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolder;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;

public class DownloadProgressThread {

    Core core;

    enum States {
        BEGIN, EXTRACTING, EXTRACTING_HTML, EXTRACTING_HTML_DONE
    };

    private DownloadInfo info;

    private States state;

    public DownloadProgressThread(Core core) {
        this.core = core;
    }

    public void modal(final String url, final RSSFolder parent, Component main) {
        final DownloadProgressDialog dialog = new DownloadProgressDialog();

        final AtomicBoolean stop = new AtomicBoolean(false);
        final Runnable notify = new Runnable() {
            @Override
            public void run() {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        String str = "";
                        switch (getState()) {
                        case BEGIN:
                            str += "Working ... ";
                            break;
                        case EXTRACTING:
                            if (getInfo().getDelay() != 0)
                                str += "Retyring ... " + getInfo().getDelay() + "\n" + getInfo().getException();
                            else
                                str += "Working ... " + getInfo().getState();
                            break;
                        case EXTRACTING_HTML:
                            str += "Extracting html";
                            break;
                        case EXTRACTING_HTML_DONE:
                            str += "Extracting html done";
                            break;
                        default:
                            break;
                        }
                        dialog.lblValue.setText(str);
                    }
                });
            }
        };

        setState(States.BEGIN);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mircle(url, parent, stop, notify);
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dispose();
                        }
                    });
                } catch (DownloadInterruptedError e) {
                    // ignore
                } catch (final DownloadError e) {
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dispose();
                            AppError.modalError(e);
                        }
                    });
                }
            }
        });
        t.start();

        dialog.lblUrl.setText(url);

        if (!dialog.modal(main))
            stop.set(true);

        try {
            t.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void mircle(String url, RSSFolder parent, AtomicBoolean stop, Runnable notify) {
        setState(States.BEGIN);
        notify.run();

        mircleContent(url, parent, stop, notify);
    }

    void mircleContent(String source, final RSSFolder parent, final AtomicBoolean stop, final Runnable notify) {
        try {
            URL url = new URL(source);

            {
                // 1) check if we already have this download in the download
                // list.
                // if so, we dont need to handle it twice. just highlight it
                // there.
                DirectDownload t = new DirectDownload(url, core.transfers.getDownloadFolder());
                Transfer tt = core.transfers.getDirect().check(t);
                if (tt != null) {
                    if (!tt.isAlive())
                        core.transfers.getDirect().start(tt);

                    core.save(core.transfers);
                    core.app.appSwitch.modelSwitch(core.transfers.getDirect().getClass());

                    DirectView view = (DirectView) core.app.appSwitch.getView(core.transfers.getDirect());
                    view.select(tt);
                    return;
                }
            }

            setInfo(new DownloadInfo(url));
            setState(States.EXTRACTING);
            notify.run();
            getInfo().extract(stop, notify);

            Model m = null;

            // 2) if it is rss+xml type === subscribe
            m = core.rss.extract(getInfo());
            if (m != null) {
                core.app.appSwitch.modelSwitch(core.rss.getClass(), m);
                core.save(core.rss);
                return;
            }

            String ct = getInfo().getContentType().toLowerCase();
            if (ct.contains("application/xml") || ct.contains("text/html") || ct.contains("text/xml")) {
                setState(States.EXTRACTING_HTML);
                String html = WGet.getHtml(new URL(source), new WGet.HtmlLoader() {
                    @Override
                    public void notifyRetry(int delay, Throwable e) {
                        notify.run();
                    }

                    @Override
                    public void notifyMoved() {
                        notify.run();
                    }

                    @Override
                    public void notifyDownloading() {
                        notify.run();
                    }
                }, stop);
                setState(States.EXTRACTING_HTML_DONE);
                // 3) if it is text / html + rss link inside === subscribe
                final RSSModel mm = (RSSModel) core.rss.extract(getInfo(), html);
                if (mm != null) {
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            RSSModel mmm = core.rss.exist(mm);
                            if (mmm == null) {
                                core.rss.add((RSSModel) mm, parent);
                                core.app.appSwitch.modelSwitch(core.rss.getClass(), mm);
                            } else {
                                core.app.appSwitch.modelSwitch(core.rss.getClass(), mmm);
                            }
                            core.save(core.rss);
                        }
                    });
                    return;
                }
                throw new DownloadError("it is not a rss feed");
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (MalformedURLException e) {
            throw new DownloadError(e);
        }
    }

    void downloadDirect(Transfer t) {
        t = core.transfers.getDirect().download(t);

        if (!t.isAlive())
            core.transfers.getDirect().start(t);

        core.app.appSwitch.modelSwitch(core.transfers.getDirect().getClass());

        DirectView view = (DirectView) core.app.appSwitch.getView(core.transfers.getDirect());
        view.select(t);
    }

    synchronized public DownloadInfo getInfo() {
        return info;
    }

    synchronized public void setInfo(DownloadInfo info) {
        this.info = info;
    }

    synchronized public States getState() {
        return state;
    }

    synchronized public void setState(States state) {
        this.state = state;
    }

}
