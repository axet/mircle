package com.google.code.mircle.plugins.subscriptions.model;

import javax.swing.event.ListDataListener;

public interface FeedListener {

    public Object get(int i);
    
    public int getSize();
    
    public void addListener(ListDataListener l);

    public void removeListener(ListDataListener l);

}
