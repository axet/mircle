package com.google.code.mircle.plugins.subscriptions.controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.QuickSearchList;
import com.google.code.mircle.plugins.subscriptions.model.FeedEntry;
import com.google.code.mircle.plugins.subscriptions.model.FeedListener;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolder;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolderExpand;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolderExpand.RSSModelExpand;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;
import com.google.code.mircle.plugins.subscriptions.view.FeedEntryView;

public class RSSFolderController extends JPanel implements Controller {
    private static final long serialVersionUID = -5843882598074012891L;

    class RSSFeedListView extends FeedListView {
        private static final long serialVersionUID = -9055061186316809042L;

        public RSSFeedListView(FeedListener listModel) {
            super(listModel);
        }

        @Override
        Object createView(Object m) {
            return new FeedEntryView(core, model, (FeedEntry) m);
        }

    }

    Core core;

    JLabel label = new JLabel();

    JPanel top;
    JButton loadMore;
    FeedListView feedListView;
    JScrollPane scroll;
    RSSFolder model;

    JPanel splitTop;
    JSplitPane split;

    DefaultListModel listQuick = new DefaultListModel();

    FeedFolderDownView feedDownView;
    private JLabel lblunreadCount;
    private JButton btnMarkAllAs;
    private JTextField textField;

    QuickSearchList qs;

    public RSSFolderController(Core c, RSSFolder s) {
        super(new BorderLayout());

        core = c;

        model = s;

        feedDownView = new FeedFolderDownView(core, model);

        feedDownView.setPreferredSize(new Dimension(0, 180));

        feedListView = new RSSFeedListView(model);

        feedListView.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++) {
                    if (feedListView.isSelectedIndex(i))
                        feedDownView.update((FeedEntryView) feedListView.getModel().getElementAt(i));
                }
            }
        });

        GridBagLayout gbl_top = new GridBagLayout();
        gbl_top.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
        top = new JPanel(gbl_top);

        lblunreadCount = new JLabel("(unread count)");
        GridBagConstraints gbc_lblunreadCount = new GridBagConstraints();
        gbc_lblunreadCount.insets = new Insets(0, 0, 0, 5);
        gbc_lblunreadCount.gridx = 0;
        gbc_lblunreadCount.gridy = 0;
        top.add(lblunreadCount, gbc_lblunreadCount);

        label.setText("Title");

        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.gridx = 1;
        gbc_label.gridy = 0;
        top.add(label, gbc_label);

        loadMore = new JButton();
        loadMore.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                feedListView.loadMore();
            }
        });
        loadMore.setText("Load More");
        GridBagConstraints gbc_loadMore = new GridBagConstraints();
        gbc_loadMore.insets = new Insets(0, 0, 0, 5);
        gbc_loadMore.gridx = 2;
        gbc_loadMore.gridy = 0;
        top.add(loadMore, gbc_loadMore);

        splitTop = new JPanel(new BorderLayout());
        splitTop.add(top, BorderLayout.NORTH);

        btnMarkAllAs = new JButton("Mark All As Read");
        btnMarkAllAs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                model.markAllAsRead();
            }
        });
        GridBagConstraints gbc_btnMarkAllAs = new GridBagConstraints();
        gbc_btnMarkAllAs.insets = new Insets(0, 0, 0, 5);
        gbc_btnMarkAllAs.gridx = 4;
        gbc_btnMarkAllAs.gridy = 0;
        top.add(btnMarkAllAs, gbc_btnMarkAllAs);

        textField = new JTextField();

        scroll = new JScrollPane(feedListView);

        splitTop.add(scroll, BorderLayout.CENTER);
        splitTop.add(textField, BorderLayout.SOUTH);

        qs = new QuickSearchList(feedListView, textField) {
            @Override
            public void notifySearch() {
            }

            @Override
            public String toString(Object o) {
                FeedEntryView v = (FeedEntryView) o;
                FeedEntry m = (FeedEntry) v.getModel();
                return m.title;
            }

            @Override
            public void doParentLayout() {
                splitTop.doLayout();
            }

            @Override
            public DefaultListModel getMasterModel() {
                DefaultListModel mm = new DefaultListModel();

                ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();
                RSSFolderExpand.expand(expand, model);

                for (RSSModelExpand m : expand) {
                    for (FeedEntry e : m.entries) {
                        FeedEntryView v = (FeedEntryView) feedListView.createView(e);
                        mm.addElement(v);
                    }
                }

                return mm;
            }
        };

        model.addListener(new LongRun.Listener() {
            @Override
            public void stop() {
            }

            @Override
            public void start() {
            }

            @Override
            public void changed() {
                update();
            }

            @Override
            public void close() {
            }
        });

        update();

        split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitTop, feedDownView);
        split.setResizeWeight(1);

        add(split);
    }

    void update() {
        lblunreadCount.setText("(" + model.getUnreadCount() + " / " + model.getSize() + ")");
        label.setText(model.getState().name);
    }

    @Override
    public void activated() {
        if (feedListView.modelSlave.getSize() == 0) {
            feedListView.loadMore();
        }

        update();
    }

    @Override
    public void deactivated() {
        if (model.getNewCount() > 0)
            model.markAllAsOld();
    }

    @Override
    public void close() {
        deactivated();
    }

    public void quickSearch(String str) {
        listQuick.clear();

        if (str.isEmpty()) {
            feedListView.setModel(feedListView.getModelSlave());
        } else {
            feedListView.setModel(listQuick);

            str = str.toLowerCase();

            ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();
            RSSFolderExpand.expand(expand, model);

            for (RSSModelExpand m : expand) {
                for (FeedEntry e : m.entries) {
                    FeedEntryView v = (FeedEntryView) feedListView.createView(e);
                    if (e.toString().toLowerCase().contains(str))
                        listQuick.addElement(v);
                }
            }
        }
    }
}
