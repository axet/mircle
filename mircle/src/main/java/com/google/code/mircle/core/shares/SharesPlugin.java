package com.google.code.mircle.core.shares;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;

public class SharesPlugin implements Plugin, PluginRoot {

    public SharesPlugin() {
    }

    public void close() {

    }

    public String getName() {
        return "Shares";
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
        // return Arrays.asList(new Model[] { new ShareModel("games",
        // "C:\\Documents\\Games"),
        // new ShareModel("www", "//mini.local/www"), new ShareModel("local",
        // "//mini.local/local"),
        // new ShareModel("Share", "/Users/axet/Share"), new ShareModel("xxx",
        // "smb://mini.local/xxx") });
    }

    public JPopupMenu contextMenu(Model m) {
        if (m == null) {
            JPopupMenu popup = new JPopupMenu();
            JMenuItem create = new JMenuItem("Create Share");
            create.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    createShareDialog((Component) e.getSource());
                }
            });
            popup.add(create);
            return popup;
        } else {
            return null;
        }
    }

    public void createShareDialog(Component c) {
        CreateShareDialog dialog = new CreateShareDialog();
        dialog.setLocationRelativeTo(SwingUtilities.getRoot(c));
        dialog.setVisible(true);
    }

    public Controller createController(Model m) {
        if (m instanceof ShareModel)
            return new ShareView((ShareModel) m);
        return null;
    }

    @Override
    public void save(File path) {
    }

    @Override
    public void load(File path) {
    }

    @Override
    public View createView(Model m) {
        return null;
    }

    @Override
    public int dndAction(Model m) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

}
