package com.google.code.mircle.plugins.googlereader;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class GoogleReaderView extends JPanel implements Controller {

    public GoogleReaderView(GoogleReaderModel s) {
        super(new BorderLayout());

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("Your personal Folder 1");
        listModel.addElement("Your personal  Feed 2");
        listModel.addElement(s.toString());

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
