package com.google.code.mircle.plugins.radio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class SHOUTstationView implements View {

    SHOUTstationModel t;

    int column;

    static class Favorite extends JButton {
        private static final long serialVersionUID = 4249871857592301582L;

        Icon starOn = new ImageIcon(SHOUTcastModelController.class.getResource ("/star-on.png"));
        Icon starOff = new ImageIcon(SHOUTcastModelController.class.getResource("/star-off.png"));

        SHOUTstationModel g;

        public Favorite(SHOUTstationModel gg) {
            this.g = gg;

            setBorder(BorderFactory.createEmptyBorder());

            update();

            setOpaque(true);

            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    g.setFavorite(!g.getFavorite());
                    update();
                    g.genre.notifyStationChanged(g);
                }
            });
        }

        void update() {
            if (g.getFavorite())
                setIcon(starOn);
            else
                setIcon(starOff);
        }
    }

    public SHOUTstationView(SHOUTstationModel t, int column) {
        this.t = t;
        this.column = column;
    }

    public static void initTree(DefaultTableModel model) {
        model.addColumn("Play");
        model.addColumn("Title");
        model.addColumn("Bitrate");
        model.addColumn("Codec");
        model.addColumn("Listeners");
        model.addColumn("Favorite");
    }

    public static DefaultTableModel createTreeModel() {
        DefaultTableModel m = new DefaultTableModel() {
            private static final long serialVersionUID = 208885644463396858L;

            public boolean isCellEditable(int row, int column) {
                if (column == 5)
                    return true;
                return false;
            }

            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                case 5:
                    return JButton.class;
                default:
                    return Object.class;
                }
            }
        };
        initTree(m);
        return m;
    }

    public static Object[] toTreeModel(SHOUTstationModel g) {
        return new Object[] { new SHOUTstationView(g, 0), new SHOUTstationView(g, 1), new SHOUTstationView(g, 2),
                new SHOUTstationView(g, 3), new SHOUTstationView(g, 4), new Favorite(g) };
    }

    public static SHOUTstationModel fromTreeModel(Object[] o) {
        return (SHOUTstationModel) ((SHOUTstationView) o[1]).getModel();
    }

    public String toString() {
        switch (column) {
        case 0: // play
            return t.play ? "P" : "";
        case 1: // title
            return t.getStation().getName();
        case 2: // bitrate
            return t.getStation().getBitrate().toString();
        case 3: // codec
            return t.getStation().getFormat();
        case 4: // listeners
            return t.getStation().getListeners().toString();
        default:
            throw new RuntimeException("bad column");
        }
    }

    @Override
    public Model getModel() {
        return t;
    }

    @Override
    public void close() {
    }

}
