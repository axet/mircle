package com.google.code.mircle.core.transfers;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.commons.io.FileUtils;

import com.github.axet.wget.info.DownloadInfo;
import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.transfers.direct.DirectDownload;
import com.google.code.mircle.core.transfers.direct.DirectModel;
import com.google.code.mircle.core.transfers.direct.DirectModelView;
import com.google.code.mircle.core.transfers.direct.DirectView;
import com.google.code.mircle.core.transfers.direct.EchoRuDownload;
import com.google.code.mircle.core.transfers.direct.VGetDownload;
import com.google.code.mircle.core.transfers.queue.QueueModel;
import com.google.code.mircle.core.transfers.queue.QueueView;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class TransfersPlugin implements Plugin, Model, PluginRoot {

    private File downloadFolder;

    Core core;

    DefaultListModel models = new DefaultListModel();

    DirectModel direct;

    @XStreamAlias("State")
    public static class State {
        public Long version = new Long(1);
        public Object state;
    }

    public TransfersPlugin(Core core) {
        this.core = core;

        direct = new DirectModel(core, this);

        // load models
        models.addElement(direct);

        setDownloadFolder(core.desktop.getDownloads());
    }

    public void close() {
        direct.close();
    }

    public String toString() {
        return "Transfers";
    }

    /**
     * update left tree pane name for Direct model.
     * 
     * when we need to update model name, call this method. usually when we have
     * start or done dowloading.
     */
    public void directChanged() {
        int i = models.indexOf(direct);
        models.set(i, direct);
    }

    @Override
    public DefaultListModel getTreeModels() {
        return models;
    }

    public DirectModel getDirect() {
        return direct;
    }

    public JPopupMenu contextMenu(Model m) {
        JPopupMenu popup = new JPopupMenu();
        popup.add(new JMenuItem("123"));
        return popup;
    }

    public Controller createController(Model m) {
        if (m instanceof DirectModel)
            return new DirectView(core, (DirectModel) m, this);
        if (m instanceof QueueModel)
            return new QueueView((QueueModel) m);
        if (m instanceof TransfersPlugin)
            return new TransfersView((TransfersPlugin) m);

        return null;
    }

    @Override
    public void save(File path) {
        try {
            XStream stream = xstream();
            State state = new State();
            state.state = direct.save();
            core.save(state, stream, FileUtils.getFile(path, "direct.xml"));
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    XStream xstream() {
        XStream stream = new XStream();
        stream.processAnnotations(new Class[] { State.class, DirectDownload.State.class, VGetDownload.State.class,
                EchoRuDownload.class, DownloadInfo.class });
        return stream;
    }

    @Override
    public void load(File path) {
        try {
            XStream s = xstream();
            State state = (State) s.fromXML(FileUtils.getFile(path, "direct.xml"));
            direct.load(state.state);

            // for (Model m : new Model[] { direct, new QueueModel("Queue") })
            // models.addElement(m);
        } catch (com.thoughtworks.xstream.io.StreamException stream) {
            try {
                throw stream.getCause();
            } catch (FileNotFoundException ignore) {
                // skip file not found
            } catch (EOFException ignore) {
                // empty file
            } catch (Throwable e) {
                throw new com.thoughtworks.xstream.io.StreamException(e);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public File getDownloadFolder() {
        return downloadFolder;
    }

    public void setDownloadFolder(File downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    @Override
    public View createView(Model m) {
        if (m instanceof TransfersPlugin)
            return new TransfersPluginView((TransfersPlugin) m);
        if (m instanceof DirectModel)
            return new DirectModelView((DirectModel) m);
        return null;
    }

    @Override
    public int dndAction(Model m) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }
}
