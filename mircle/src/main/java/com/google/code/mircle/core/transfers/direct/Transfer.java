package com.google.code.mircle.core.transfers.direct;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Worker;

public abstract class Transfer extends Worker implements Model {

    public static abstract class State extends Worker.State {
        // download file deleted localy
        private boolean deleted;
        // date when file was deleted
        private Date deletedDate;
        // file pointed by user
        private File targetUser;
        // file choisen by application (can be conflicted names, several folders
        // / file for torrent download)
        private File targetApp;
        // handable exception druning download (file not found, or video is
        // unavailable)
        private Throwable exception;
        // exception date (so we can purge bad downloads lately)
        private Date exceptionDate;

        synchronized public boolean getDeleted() {
            return deleted;
        }

        synchronized public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        /**
         * check target. if it is deleted or exist.
         * 
         * @return true if file exist
         */
        synchronized public boolean isDownloadExist() {
            File file = getTargetApp();
            if (file == null)
                return false;
            return file.exists();
        }

        synchronized public File getTargetApp() {
            return targetApp;
        }

        synchronized public void setTargetApp(File targetFile) {
            this.targetApp = targetFile;
        }

        synchronized public File getTargetUser() {
            return targetUser;
        }

        synchronized public void setTargetUser(File targetUser) {
            this.targetUser = targetUser;
        }

        synchronized public Date getDeletedDate() {
            return deletedDate;
        }

        synchronized public void setDeletedDate(Date deletedDate) {
            this.deletedDate = deletedDate;
        }

        synchronized public Throwable getException() {
            return exception;
        }

        synchronized public void setException(Throwable exception) {
            this.exception = exception;
            setExceptionDate(new Date());
        }

        synchronized public Date getExceptionDate() {
            return exceptionDate;
        }

        synchronized void setExceptionDate(Date exceptionDate) {
            this.exceptionDate = exceptionDate;
        }
    }

    public void downloadAgain() {
        State state = (State) getState();

        FileUtils.deleteQuietly(state.getTargetApp());
        state.setTargetApp(null);

        state.setDeleted(false);
        state.setDeletedDate(null);
    }
}
