package com.google.code.mircle.plugins.community;

import com.google.code.mircle.core.Model;

public class CommunityModel implements Model {

    String t;

    public CommunityModel(String t) {
        this.t = t;
    }

    public String toString() {
        return t;
    }

    @Override
    public void close() {
    }
}
