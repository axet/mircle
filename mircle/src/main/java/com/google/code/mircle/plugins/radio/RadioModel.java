package com.google.code.mircle.plugins.radio;

import com.google.code.mircle.core.Model;

public class RadioModel implements Model{

    String t;

    public RadioModel(String t) {
        this.t = t;
    }

    public String toString() {
        return t;
    }

    @Override
    public void close() {
    }
}
