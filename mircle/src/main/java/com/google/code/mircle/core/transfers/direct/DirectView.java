package com.google.code.mircle.core.transfers.direct;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.QuickSearchList;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.transfers.TransfersPlugin;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class DirectView extends JPanel implements Controller {

    private static final long serialVersionUID = -4836272012857354819L;

    final DirectModel model;
    final TransfersPlugin plugin;

    DefaultListModel listMaster;
    DefaultListModel listSlave;

    final Core core;

    // Parent: DirectView
    JSplitPane split;

    // Parent: split
    JPanel splitTop = new JPanel(new BorderLayout());

    // Parent: splitTop, Top panel holds toolbar
    JPanel splitTopTop;
    JButton btnStart = new JButton("Start");
    JButton btnStop = new JButton("Stop");
    JButton btnDelete = new JButton("Remove");

    // Parent: splitTop, center panel
    JPanel splitTopCenter;
    JScrollPane splitTopCenterScroll;
    JList splitTopCenterScrollList;

    // Parent: split
    JScrollPane splitDown;

    // Parent: splitDown, holds for ddp or tdp or ydp
    JPanel splitDownPanel = new JPanel();
    DirectDownloadPanel ddp = new DirectDownloadPanel();
    TorrentDownloadPanel tdp = new TorrentDownloadPanel();
    VGetDownloadPanel ydp;

    // each DirectDownload panel uses this variable
    //
    // spam of events from download (update each 100-1000 bytes downladed)
    // should be stopped by time filter. we shall skip all similar updates like
    // bytes chnaes
    // (status update force update)
    public static final long MAX_UPDATE_TIME = 20;

    Map<Transfer, LongRun.Listener> map = new HashMap<Transfer, LongRun.Listener>();
    private JButton btnPause;
    private JButton btnDownloadAgain;
    private JTextField textField;

    QuickSearchList qs;

    public DirectView(Core c, DirectModel s, final TransfersPlugin p) {
        super(new BorderLayout());

        ydp = new VGetDownloadPanel(c, p);

        this.core = c;
        this.plugin = p;
        this.model = s;

        listMaster = model.getTransfers();
        listSlave = new DefaultListModel();

        splitTopCenterScrollList = new JList(listSlave);

        {
            splitTopTop = new JPanel();

            GridBagLayout gbl_top = new GridBagLayout();
            gbl_top.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            gbl_top.rowHeights = new int[] { 0, 0 };
            gbl_top.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
            gbl_top.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
            splitTopTop.setLayout(gbl_top);

            GridBagConstraints gbc_btnStop = new GridBagConstraints();
            gbc_btnStop.insets = new Insets(0, 0, 0, 5);
            gbc_btnStop.gridx = 1;
            gbc_btnStop.gridy = 0;
            btnStop.setEnabled(false);
            btnStop.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
                    Transfer t = (Transfer) tt.getModel();
                    t.stop();
                    core.save(plugin);

                    updateBottom();
                }
            });

            GridBagConstraints gbc_btnStart = new GridBagConstraints();
            gbc_btnStart.insets = new Insets(0, 0, 0, 5);
            gbc_btnStart.gridx = 0;
            gbc_btnStart.gridy = 0;
            btnStart.setEnabled(false);
            btnStart.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
                    Transfer t = (Transfer) tt.getModel();
                    model.start(t);
                    core.save(plugin);
                }
            });
            splitTopTop.add(btnStart, gbc_btnStart);
            splitTopTop.add(btnStop, gbc_btnStop);

            GridBagConstraints gbc_btnDelete = new GridBagConstraints();
            gbc_btnDelete.insets = new Insets(0, 0, 0, 5);
            gbc_btnDelete.gridx = 2;
            gbc_btnDelete.gridy = 0;
            btnDelete.setEnabled(false);
            btnDelete.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    int i = splitTopCenterScrollList.getSelectedIndex();
                    TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
                    Transfer t = (Transfer) tt.getModel();
                    model.del(t);
                    if (i >= splitTopCenterScrollList.getModel().getSize())
                        i = splitTopCenterScrollList.getModel().getSize() - 1;
                    splitTopCenterScrollList.setSelectedIndex(i);
                }
            });
            splitTopTop.add(btnDelete, gbc_btnDelete);

            JButton btnNewButton = new JButton("Open");
            btnNewButton.setEnabled(false);
            btnNewButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    open();
                }
            });
            GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
            gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
            gbc_btnNewButton.gridx = 3;
            gbc_btnNewButton.gridy = 0;
            splitTopTop.add(btnNewButton, gbc_btnNewButton);
        }
        splitTop.add(splitTopTop, BorderLayout.NORTH);

        btnDownloadAgain = new JButton("Download Again");
        btnDownloadAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
                Transfer t = (Transfer) tt.getModel();
                t.stop();
                t.downloadAgain();
                core.transfers.getDirect().start(t);
                core.save(plugin);
            }
        });
        GridBagConstraints gbc_btnDownloadAgain = new GridBagConstraints();
        gbc_btnDownloadAgain.insets = new Insets(0, 0, 0, 5);
        gbc_btnDownloadAgain.gridx = 4;
        gbc_btnDownloadAgain.gridy = 0;
        splitTopTop.add(btnDownloadAgain, gbc_btnDownloadAgain);

        btnPause = new JButton("Pause All");
        btnPause.setEnabled(false);
        GridBagConstraints gbc_btnPause = new GridBagConstraints();
        gbc_btnPause.insets = new Insets(0, 0, 0, 5);
        gbc_btnPause.gridx = 5;
        gbc_btnPause.gridy = 0;
        splitTopTop.add(btnPause, gbc_btnPause);

        textField = new JTextField();

        qs = new QuickSearchList(splitTopCenterScrollList, textField) {
            @Override
            public void notifySearch() {
            }

            @Override
            public String toString(Object o) {
                TransferView v = (TransferView) o;
                Transfer m = (Transfer) v.getModel();
                return m.status().title;
            }

            @Override
            public void doParentLayout() {
                splitTopCenter.doLayout();
            }
        };

        {
            splitTopCenter = new JPanel(new BorderLayout());
            splitTopCenterScrollList.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent arg0) {
                    updateBottom();
                }
            });
            splitTopCenterScrollList.addMouseListener(new MouseListener() {

                @Override
                public void mouseReleased(MouseEvent arg0) {
                }

                @Override
                public void mousePressed(MouseEvent arg0) {
                }

                @Override
                public void mouseExited(MouseEvent arg0) {
                }

                @Override
                public void mouseEntered(MouseEvent arg0) {
                }

                @Override
                public void mouseClicked(MouseEvent arg0) {
                    if (arg0.getClickCount() == 2) {
                        open();
                    }
                }
            });
            splitTopCenterScroll = new JScrollPane(splitTopCenterScrollList);
            splitTopCenter.add(splitTopCenterScroll);
            splitTopCenter.add(textField, BorderLayout.SOUTH);

            splitTop.add(splitTopCenter, BorderLayout.CENTER);
        }

        splitDown = new JScrollPane(splitDownPanel);

        splitDown.setPreferredSize(new Dimension(0, 280));

        split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitTop, splitDown);
        split.setResizeWeight(1);

        add(split);

        load();
    }

    public void load() {
        ListDataListener l = new ListDataListener() {
            @Override
            public void intervalRemoved(ListDataEvent e) {
                for (int i = e.getIndex0(); i <= e.getIndex1(); i++) {
                    TransferView tt = (TransferView) listSlave.get(i);
                    Transfer t = (Transfer) tt.getModel();
                    LongRun.Listener l = map.remove(t);
                    t.removeListener(l);
                    listSlave.remove(i);
                }
            }

            @Override
            public void intervalAdded(ListDataEvent e) {
                for (int i = e.getIndex0(); i <= e.getIndex1(); i++) {
                    final Transfer t = (Transfer) listMaster.get(i);
                    final LongRun.Listener l = new LongRun.Listener() {
                        @Override
                        public void stop() {
                            update(t);
                        }

                        @Override
                        public void start() {
                            update(t);
                        }

                        @Override
                        public void changed() {
                            update(t);
                        }

                        @Override
                        public void close() {
                        }
                    };
                    t.addListener(l);
                    map.put(t, l);
                    listSlave.add(i, new TransferView(t));
                }
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                for (int i = e.getIndex0(); i <= e.getIndex1(); i++) {
                    // TODO i do not understan why it crashing here on a start.
                    // have to understand later.
                    if (listSlave.getSize() <= i)
                        continue;
                    Object l = listSlave.get(i);
                    listSlave.set(i, l);
                }
            }
        };

        listMaster.addListDataListener(l);

        if (listMaster.getSize() > 0)
            l.intervalAdded(new ListDataEvent(listMaster, ListDataEvent.INTERVAL_ADDED, 0, listMaster.getSize() - 1));
    }

    public void close() {
        for (Transfer t : map.keySet()) {
            t.removeListener(map.get(t));
        }
    }

    void updateBottom() {
        TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
        if (tt == null)
            return;

        Transfer t = (Transfer) tt.getModel();

        Worker w = t;

        btnDownloadAgain.setEnabled(!t.isAlive());
        btnStart.setEnabled(!t.isAlive() && !(t.getState().getDownloadState() == DownloadState.DOWNLOAD_COMPLETE)
                && !w.isQueued());
        btnStop.setEnabled(t.isAlive() || w.isQueued());
        btnDelete.setEnabled(!t.isAlive());

        splitDownPanel.removeAll();

        if (w instanceof DirectDownload) {
            ddp.setTransfer(t);

            if (splitDownPanel.getComponentCount() == 0 || !splitDownPanel.getComponent(0).equals(ddp)) {
                splitDownPanel.add(ddp);
            }
        } else if (w instanceof VGetDownload) {
            ydp.setTransfer(t);

            if (splitDownPanel.getComponentCount() == 0 || !splitDownPanel.getComponent(0).equals(ydp)) {
                splitDownPanel.add(ydp);
            }
        } else if (w instanceof TorrentDownload) {
            tdp.setTransfer(t);

            if (splitDownPanel.getComponentCount() == 0 || !splitDownPanel.getComponent(0).equals(tdp)) {
                splitDownPanel.add(tdp);
            }
        }

        splitDownPanel.revalidate();
        splitDownPanel.repaint();
    }

    public void select(Transfer t) {
        splitTopCenterScrollList.clearSelection();

        ListModel list = splitTopCenterScrollList.getModel();
        for (int i = 0; i < list.getSize(); i++) {
            TransferView v = (TransferView) list.getElementAt(i);
            if (v.getModel().equals(t))
                splitTopCenterScrollList.setSelectedValue(v, true);
        }
    }

    /**
     * Transfer State from the list, has been changed
     * 
     * @param t
     */
    void update(Transfer t) {
        // notify others, so they will update text representation
        // (DirectModelView)
        final DefaultListModel list = model.getTransfers();
        int index = list.indexOf(t);
        list.set(index, t);

        // update bottom
        TransferView tt = (TransferView) this.splitTopCenterScrollList.getSelectedValue();
        if (tt != null) {
            if (t.equals(tt.getModel()))
                updateBottom();
        }
    }

    void open() {
        TransferView tt = (TransferView) splitTopCenterScrollList.getSelectedValue();
        Transfer t = (Transfer) tt.getModel();
        Transfer.State s = (State) t.getState();
        core.playerPlugin.play(s.getTargetApp());
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

}
