package com.google.code.mircle.plugins.radio;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class RadioPluginView implements View {

    RadioPlugin model;

    public RadioPluginView(RadioPlugin m) {
        model = m;
    }

    public String toString() {
        return "Radio";
    }

    public void close() {
    }

    @Override
    public Model getModel() {
        return model;
    }

}
