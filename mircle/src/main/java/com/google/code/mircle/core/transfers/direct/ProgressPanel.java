package com.google.code.mircle.core.transfers.direct;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.DownloadInfo.Part;

public class ProgressPanel extends JPanel {

    private static final long serialVersionUID = 7273910828344230190L;

    /**
     * number of horisontal dots
     */
    int dotsXX = 18;
    /**
     * number of vertical dots
     */
    int dotsYY = 18;

    /**
     * supported states of matrix
     * 
     */
    enum State {
        EMPTY, PARTIAL, FULL
    }

    /**
     * progress matrix
     */
    State[] matrix = new State[dotsXX * dotsYY];

    long last = 0;

    public ProgressPanel() {
        clear();
    }

    public void update(DownloadInfo di) {
        clear();

        Long max = di.getLength();
        if (max == null)
            return;

        long size = max / matrix.length;

        // if size is too small to show progress
        if (size == 0)
            return;

        if (di.multipart()) {
            for (Part p : di.getParts()) {
                update(p.getStart(), p.getCount(), p.getEnd(), size);
            }
        } else {
            update(0, di.getCount(), di.getLength(), size);
        }

        repaint();
    }

    void update(long s, long c, long e, long size) {
        int ss = (int) (s / size);
        int ee = (int) ((s + c) / size);

        // vimeo may give you wrong size on extract step, and continue download
        // over 100%. so progress should not crash in this situations.
        if (ee >= matrix.length)
            ee = matrix.length;

        for (; ss < ee; ss++) {
            // do not overwrite partial downloads info.
            if (matrix[ss] == State.PARTIAL)
                continue;
            matrix[ss] = State.FULL;
        }
        if (c > 0 && s + c < e && ss < matrix.length) {
            matrix[ss] = State.PARTIAL;
        }
    }

    void clear() {
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = State.EMPTY;
        }

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D gg = (Graphics2D) g;

        // borders stroke size
        int stroke = 1;

        int CX = getWidth();
        int CY = getHeight();

        // width of drawing matrix
        int areaCX = CX;
        // Height of drawing matrix
        int areaCY = CY;

        // cutting whole area border
        int cx = areaCX - stroke * 2;
        int cy = areaCX - stroke * 2;

        // width of cell
        int cellXX = (cx - (dotsXX - 1) * stroke) / dotsXX;
        // height of cell
        int cellYY = (cy - (dotsYY - 1) * stroke) / dotsYY;

        // calculate back cx and cy
        cx = dotsXX * (stroke + cellXX);
        cy = dotsYY * (stroke + cellYY);

        areaCX = cx + stroke * 2;
        areaCY = cy + stroke * 2;

        int startx = 0;
        int starty = 0;

        // draw border
        {
            gg.setColor(Color.LIGHT_GRAY);
            gg.setStroke(new BasicStroke(stroke));
            gg.drawRect(startx, starty, areaCX - stroke, areaCY - stroke);
        }

        // draw matrix
        {
            gg.setColor(Color.LIGHT_GRAY);

            int x = startx + stroke + cellXX;
            while (x < cx) {
                gg.drawLine(x, starty, x, cy);
                x += cellXX + stroke;
            }

            int y = starty + stroke + cellYY;
            while (y < cy) {
                gg.drawLine(startx, y, cx, y);
                y += cellYY + stroke;
            }
        }

        // draw state
        {
            int startX = startx + stroke;
            int startY = starty + stroke;

            for (int i = 0; i < matrix.length; i++) {
                int y = i / dotsXX;
                int x = i - y * dotsXX;
                int dx = startX + x * (cellXX + stroke);
                int dy = startY + y * (cellYY + stroke);
                switch (matrix[i]) {
                case EMPTY:
                    gg.setColor(Color.WHITE);
                    break;
                case FULL:
                    gg.setColor(Color.BLUE);
                    break;
                case PARTIAL:
                    gg.setColor(Color.CYAN);
                    break;
                }
                gg.fillRect(dx, dy, cellXX, cellYY);
            }
        }
    }
}
