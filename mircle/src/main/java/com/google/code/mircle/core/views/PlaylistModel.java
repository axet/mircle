package com.google.code.mircle.core.views;

import com.google.code.mircle.core.Model;

public class PlaylistModel implements Model {

    String t;

    public PlaylistModel(String t) {
        this.t = t;
    }

    public String toString() {
        return t;
    }

    @Override
    public void close() {
    }
}
