package com.google.code.mircle.core.player;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class PlayerPluginView implements View {

    PlayerPlugin plugin;

    public PlayerPluginView(PlayerPlugin p) {
        plugin = p;
    }

    public String toString() {
        return "Player";
    }

    @Override
    public Model getModel() {
        return plugin;
    }

    @Override
    public void close() {
    }

}
