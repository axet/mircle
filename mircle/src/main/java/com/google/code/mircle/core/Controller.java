package com.google.code.mircle.core;

/**
 * Controller, java View / Panels, class displayble on right pane
 * 
 * @author axet
 * 
 */
public interface Controller {
    /**
     * app close view. (probably app closing it self)
     */
    public void close();

    /**
     * app switch makes this view active (user switched to see it)
     */
    public void activated();

    /**
     * app leaves the view
     */
    public void deactivated();
}
