package com.google.code.mircle.plugins.radio;

import javax.swing.table.DefaultTableModel;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class SHOUTgenreView implements View {

    SHOUTgenreModel t;

    public SHOUTgenreView(SHOUTgenreModel t) {
        this.t = t;
    }

    public static void initTree(DefaultTableModel t) {
        t.addColumn("Genres");
    }

    public static Object[] toTreeModel(SHOUTgenreModel g) {
        return new Object[] { new SHOUTgenreView(g) };
    }
    
    public static SHOUTgenreModel fromTreeModel(Object[] oo) {
        SHOUTgenreView v = (SHOUTgenreView) oo[0];
        return (SHOUTgenreModel) v.getModel();
    }

    public String toString() {
        String str = "";

        switch (t.getState().getDownloadState()) {
        case DOWNLOAD_QUEUED:
            str += "Q " + t.getGenre().getName() + " (" + t.getStations().size() + ")";
            break;
        case DOWNLOAD_FAILED:
            str += "E " + t.getGenre().getName() + " (" + t.getStations().size() + ")";
            break;
        case DOWNLOAD_NEW:
            str += t.getGenre().getName() + " (" + t.getStations().size() + ")";
            break;
        case DOWNLOAD_COMPLETE:
            str += t.getGenre().getName() + " (" + t.getStations().size() + ")";
            break;
        case DOWNLOAD_START:
            switch (t.s.getState()) {
            case EXTRACT:
                str += "R[EXTRACT:" + t.s.getStations().size() + "] " + t.getGenre().getName() + " ("
                        + t.getStations().size() + ")";
                break;
            case RETRY:
                str += "R[RETRY:" + t.s.getDelay() + "] " + t.getGenre().getName() + " (" + t.getStations().size()
                        + ")";
                break;
            case DONE:
                str += "R " + t.getGenre().getName() + " (" + t.getStations().size() + ")";
                break;
            }
            break;
        case DOWNLOAD_STOP:
            str += t.getGenre().getName() + " (" + t.getStations().size() + ")";
            break;
        }
        return str;
    }

    @Override
    public Model getModel() {
        return t;
    }

    @Override
    public void close() {
    }

}
