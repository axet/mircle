package com.google.code.mircle.core;

import java.awt.Component;
import java.awt.event.KeyListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

public abstract class QuickSearchList extends QuickSearch {

    class Model implements ListModel {
        DefaultListModel m;

        public Model(DefaultListModel m) {
            this.m = m;
        }

        DefaultListModel getModel() {
            return m;
        }

        @Override
        public void add(Object o) {
            addRow(m, o);
        }

        @Override
        public int getCount() {
            return m.getSize();
        }

        @Override
        public Object get(int i) {
            return getRow(m, i);
        }

        @Override
        public String toString(Object o) {
            return QuickSearchList.this.toString(o);
        }

    }

    ListContainer list = new ListContainer() {
        @Override
        public void setModel(ListModel l) {
            table.setModel(((Model) l).getModel());
        }

        @Override
        public void select(Object m) {
            int i = indexOf((DefaultListModel) table.getModel(), m);

            table.setSelectedIndex(i);
        }

        @Override
        public void scrollTo(Object m) {
            int i = indexOf((DefaultListModel) table.getModel(), m);

            table.ensureIndexIsVisible(i);
        }

        @Override
        public void requestFocus() {
            table.requestFocus();
        }

        @Override
        public Object getSelected() {
            int i = table.getSelectedIndex();

            if (i == -1)
                return null;

            return getRow((DefaultListModel) table.getModel(), i);
        }

        @Override
        public ListModel getModel() {
            return new Model((DefaultListModel) table.getModel());
        }

        @Override
        public void addKeyListener(KeyListener l) {
            table.addKeyListener(l);
        }
    };

    JList table;

    public QuickSearchList(JList table, JTextField field) {
        this.table = table;

        create(field, list);
    }

    public int indexOf(DefaultListModel m, Object o) {
        return m.indexOf(o);
    }

    public Object getRow(DefaultListModel m, int i) {
        return m.get(i);
    }

    public DefaultListModel createModel() {
        return new DefaultListModel();
    }

    public void addRow(DefaultListModel m, Object o) {
        m.addElement(o);
    }

    public ListModel getMasterListModel() {
        return new Model(getMasterModel());
    }

    public DefaultListModel getMasterModel() {
        return ((Model) modelMaster).getModel();
    }

    @Override
    public ListModel createListModel() {
        return new Model(createModel());
    }

    public abstract void doParentLayout();

    public abstract String toString(Object o);
}
