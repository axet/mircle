package com.google.code.mircle.plugins.subscriptions.model;

public class Range {
    public int i1;
    public int i2;

    public Range(int i1, int i2) {
        this.i1 = i1;
        this.i2 = i2;
    }

    public Range overlaps(Range r2) {
        Range r3 = null;

        Range r1 = this;

        int x1 = Math.max(r1.i1, r2.i1);
        int x2 = Math.min(r1.i2, r2.i2);

        if (x2 - x1 >= 0)
            r3 = new Range(x1, x2);

        return r3;
    }

    public int size() {
        return i2 - i1 + 1;
    }
}
