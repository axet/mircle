package com.google.code.mircle.core.transfers.direct;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.DefaultListModel;

import org.apache.commons.lang3.time.DateUtils;

import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.PluginDirect;
import com.google.code.mircle.core.TreeItemModel;
import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.transfers.TransfersPlugin;

public class DirectModel implements TreeItemModel, PluginDirect {

    DefaultListModel list = new DefaultListModel();

    Core core;
    TransfersPlugin p;

    // count current downloads
    int count = 0;

    // max amount of concurent downloads
    static final int MAX_CONCURRENTS_DOWNLOADS = 3;

    // every minute
    final static int REFRESH_RATE = 1 * 60 * 1000;

    // purge after days
    final static int PURGE_DELETED_AFTER = 30;

    public DirectModel(Core core, TransfersPlugin p) {
        this.core = core;
        this.p = p;
    }

    void checkFiles() {
        Core.invoke(REFRESH_RATE, new Runnable() {
            @Override
            public void run() {
                for (Enumeration<?> e = list.elements(); e.hasMoreElements();) {
                    Transfer t = (Transfer) e.nextElement();

                    Transfer.State s = (Transfer.State) t.getState();
                    Date deleteAfter = DateUtils.addDays(new Date(), -PURGE_DELETED_AFTER);

                    if (s.getDeleted()) {
                        if (deleteAfter.after(s.getDeletedDate())) {
                            del(t);
                            e = list.elements();
                        }
                    }

                    // never happens unles you edit xml files manually
                    if (s.getException() != null) {
                        if (s.getExceptionDate() == null)
                            s.setExceptionDate(new Date());
                    }

                    if (s.getException() != null) {
                        if (deleteAfter.after(s.getExceptionDate())) {
                            del(t);
                            e = list.elements();
                        }
                    }

                    if (t.getState().getDownloadState().equals(DownloadState.DOWNLOAD_COMPLETE) && !s.isDownloadExist()) {
                        s.setDeleted(true);
                        s.setDeletedDate(new Date());
                        t.notifyLongRunChanged();
                    }
                }

                checkFiles();
            }
        }, "Direct Model Refresh");
    }

    public Object save() {
        ArrayList<Object[]> list = new ArrayList<Object[]>();

        for (Enumeration<?> e = this.list.elements(); e.hasMoreElements();) {
            Transfer t = (Transfer) e.nextElement();
            list.add(new Object[] { t.getClass().getName(), t.save() });
        }

        return list;
    }

    public void load(Object o) {
        try {
            @SuppressWarnings("unchecked")
            ArrayList<Object[]> list = (ArrayList<Object[]>) o;
            for (Object[] oo : list) {
                Transfer w = null;

                String name = (String) oo[0];
                Transfer.State state = (Transfer.State) oo[1];

                Class<?> klass = Class.forName((String) name);
                w = (Transfer) klass.newInstance();
                w.load(state);

                this.list.addElement(w);

                DownloadState ds = w.getState().getDownloadState();
                if (ds == null)
                    ds = DownloadState.DOWNLOAD_QUEUED;

                switch (ds) {
                case DOWNLOAD_START:
                case DOWNLOAD_QUEUED:
                    start(w);
                default:
                    // ignore
                }
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        
        checkFiles();
    }

    /**
     * try to start download or place it to the download queue
     * 
     * @param t
     */
    public void start(final Transfer t) {
        if (count >= MAX_CONCURRENTS_DOWNLOADS) {
            t.putToQueue();
            return;
        }

        t.addListener(new LongRun.Listener() {
            @Override
            public void stop() {
                final Transfer.Listener that = this;

                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        t.removeListener(that);

                        count--;

                        p.directChanged();

                        startNext();
                    }
                });
            }

            @Override
            public void start() {
            }

            @Override
            public void changed() {
            }

            @Override
            public void close() {
            }
        });

        count++;
        p.directChanged();
        t.start();
    }

    public void startNext() {
        for (Enumeration<?> e = list.elements(); e.hasMoreElements();) {
            Transfer t = (Transfer) e.nextElement();

            if (t.getState().getDownloadState() == DownloadState.DOWNLOAD_QUEUED) {
                start(t);

                // stop tagging tasks only if here is a limit of downloads
                if (count >= MAX_CONCURRENTS_DOWNLOADS)
                    return;
            }
        }
    }

    public void add(Transfer t) {
        list.add(0, t);
    }

    public void del(Transfer t) {
        t.stop();
        list.removeElement(t);
        t.close();
    }

    public DefaultListModel getTransfers() {
        return list;
    }

    @Override
    public void close() {
        for (Enumeration<?> e = list.elements(); e.hasMoreElements();) {
            Transfer t = (Transfer) e.nextElement();
            t.close();
        }
    }

    public Transfer check(Transfer w) {
        for (Enumeration<?> e = list.elements(); e.hasMoreElements();) {
            Transfer t = (Transfer) e.nextElement();

            if (t.checkSource(w))
                return t;

            if (t.checkTarget(w))
                throw new RuntimeException("conflicting target download folder / target content");
        }

        return null;
    }

    public Transfer check(String source) {
        for (Enumeration<?> e = list.elements(); e.hasMoreElements();) {
            Transfer t = (Transfer) e.nextElement();

            if (t.checkSource(source))
                return t;
        }

        return null;
    }

    public Transfer download(Transfer w) {
        Transfer ww = check(w);
        if (ww != null)
            return ww;

        add(w);

        core.save(p);

        return w;
    }

    @Override
    public Transfer extract(String source) {
        try {
            Transfer t = null;

            if (VGetDownload.handle(source))
                t = new VGetDownload(new URL(source), p.getDownloadFolder());

            if (EchoRuDownload.handle(source))
                t = new EchoRuDownload(new URL(source), p.getDownloadFolder());

            if (TorrentDownload.handle(source))
                t = new TorrentDownload(source, p.getDownloadFolder());

            return t;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
    }

}
