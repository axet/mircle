package com.google.code.mircle.plugins.subscriptions;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class SubscriptionsPluginView implements View {

    SubscriptionsPlugin model;

    public SubscriptionsPluginView(SubscriptionsPlugin m) {
        model = m;
    }

    public String toString() {
        return "Subscriptions";
    }

    public void close() {
    }

    @Override
    public Model getModel() {
        return model;
    }

}
