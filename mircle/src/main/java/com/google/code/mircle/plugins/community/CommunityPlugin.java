package com.google.code.mircle.plugins.community;

import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;

public class CommunityPlugin implements Plugin, Model, PluginRoot {

    String t;

    public CommunityPlugin() {
    }

    public void close() {

    }

    public String getName() {
        return "Community Online";
    }

    public JPopupMenu contextMenu(Model m) {
        JPopupMenu popup = new JPopupMenu();
        popup.add(new JMenuItem("123"));
        return popup;
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
//        return Arrays.asList(new Model[] { new CommunityModel("Blog Favorites Audio feed"),
//                new CommunityModel("Blog Favorites Youtube feed"),
//                new CommunityModel("Your Shared Prodigy playlist") });
    }

    public Controller createController(Model m) {
        if (m instanceof CommunityModel)
            return new CommunityView((CommunityModel) m);
        if (m instanceof CommunityPlugin)
            return new CommunitySettingsView((CommunityPlugin) m);
        return null;
    }

    @Override
    public void save(File path) {
    }

    @Override
    public void load(File path) {
    }

    @Override
    public View createView(Model m) {
        return null;
    }

    @Override
    public int dndAction(Model m) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

}
