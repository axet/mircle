package com.google.code.mircle.core.transfers.direct;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.github.axet.wget.RetryWrap;
import com.github.axet.wget.SpeedInfo;
import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.DownloadInfo.Part;
import com.github.axet.wget.info.ex.DownloadError;
import com.github.axet.wget.info.ex.DownloadMultipartError;
import com.github.axet.wget.info.ex.DownloadRetry;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Worker;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * DirectDownload
 * 
 * Features:
 * 
 * 1) multi thread downloader
 * 
 * 2) taking name from steam (http respond)
 * 
 * 3) create missing folders
 * 
 * 4) rolling name
 * 
 * 5) retrying, continue download
 * 
 * 6) Marking contenet with a .mircle file describing source page, hash, source
 * url. Updatable to Magnet information, additional links, tags, images (xml
 * encoded and external)
 * 
 * @author axet
 * 
 */
public class DirectDownload extends Transfer {

    @XStreamAlias("DirectDownloadState")
    public static class State extends Transfer.State {
        private DownloadInfo info;
        private URL web;

        private int delay = 0;

        synchronized public DownloadInfo getInfo() {
            return info;
        }

        synchronized public void setInfo(DownloadInfo info) {
            this.info = info;
        }

        synchronized public URL getWeb() {
            return web;
        }

        synchronized public void setWeb(URL web) {
            this.web = web;
        }

        public int getDelay() {
            return delay;
        }

        public void setDelay(int delay) {
            this.delay = delay;
        }
    }

    class Sync implements Runnable {
        @Override
        public void run() {
            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    synchronized (state) {
                        speedInfo.step(state.getInfo().getCount());
                    }
                }
            });

            changed();
        }
    }

    State state = new State();

    SpeedInfo speedInfo = new SpeedInfo();

    public DirectDownload(URL source, File target) {
        this.state.setTargetUser(target);

        this.state.setInfo(new DownloadInfo(source));
    }

    public DirectDownload(DownloadInfo info, File target) {
        this.state.setTargetUser(target);

        try {
            this.state.setInfo(info);
            if (this.state.getInfo().getRange())
                this.state.getInfo().enableMultipart();
        } catch (DownloadRetry e) {
            state.setException(e);
        } catch (DownloadError e) {
            state.setException(e);
        }
    }

    public DirectDownload(URL source, File target, URL web) {
        this.state.setTargetUser(target);
        this.state.setWeb(web);

        this.state.setInfo(new DownloadInfo(source));
    }

    protected DirectDownload() {
    }

    @Override
    public Transfer.State getState() {
        return state;
    }

    @Override
    public void downloadAgain() {
        super.downloadAgain();

        state.getInfo().reset();
    }

    void target() {
        if (state.getTargetApp() != null) {
            if (state.getInfo().multipart())
                ;// multi part? overwrite.
            else if (state.getInfo().getRange()) {
                // range download? try to resume download from last
                // position
                if (state.getTargetApp().exists() && state.getTargetApp().length() != state.getInfo().getCount())
                    state.setTargetApp(null);
            } else {
                ; // single download? overwrite file
            }
        }

        if (state.getTargetApp() == null)
            state.setTargetApp(WGet.calcName(state.getInfo(), state.getTargetUser()));
    }

    @Override
    public void run() {
        synchronized (state) {
            if (state.getDownloadState().equals(DownloadState.DOWNLOAD_COMPLETE) && state.getTargetApp().exists()) {
                return;
            }
        }

        while (true) {
            synchronized (state) {
                state.setException(null);

                if (state.getInfo().empty()) {
                    state.getInfo().extract(stop, new Sync());
                    if (this.state.getInfo().getRange())
                        this.state.getInfo().enableMultipart();
                }

                target();

                // prevent complited, exist file, with correct size, with max
                // file size from, range or single download
                // from beign download again
                if (state.getInfo().multipart())
                    ;// continue download.
                else {
                    // range or single download?
                    if (state.getTargetApp().exists() && state.getTargetApp().length() == state.getInfo().getCount()) {
                        Long l = state.getInfo().getLength();
                        if (l != null) {
                            if (l == state.getInfo().getCount()) {
                                synchronized (state) {
                                    state.setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
                                }
                                return;
                            }
                        }
                    }
                }
            }

            speedInfo.start(state.getInfo().getCount());

            WGet w;
            w = new WGet(state.getInfo(), state.getTargetApp());

            try {
                w.download(stop, new Sync());

                if (stop.get())
                    return;

                state.setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
                return;
            } catch (DownloadMultipartError e) {
                // we have received multiple threads errors.

                // if here any runtimeexception we shall stop the app.
                //
                // if here only downloadretyr exceptions - we shall retry
                // download
                //
                // if here any downloaderror exceptoin we shall stop the
                // download
                List<Part> pp = e.getInfo().getParts();

                for (Part p : pp) {
                    Throwable eee = p.getException();
                    if (eee == null)
                        continue;
                    if (eee instanceof DownloadRetry) {
                        continue;
                    }
                    if (eee instanceof DownloadError) {
                        // so here is DownloadError. we shall stop the
                        // download. but first continue on the list
                        synchronized (state) {
                            state.setException(e);
                        }
                    }
                    // we come here then here a runtimeexcetpion. stop the
                    // app.
                    //
                    // if here any runtimeexceptions in the list we shall
                    // stop the app
                    throw e;
                }
                // this field only will be filled if here any
                // DownloadError exception. so we have to stop
                // downloading (by calling return)
                synchronized (state) {
                    if (state.getException() != null)
                        return;
                }
                // so. only DownloadRetrys here. retry()
                retry();
            } catch (DownloadError e) {
                synchronized (state) {
                    state.setException(e);
                }
                return;
            }
        }
    }

    void retry() {
        for (int i = RetryWrap.RETRY_DELAY; i >= 0; i--) {
            if (stop.get())
                return;

            changed();

            try {
                Thread.sleep(1000);
            } catch (Exception r) {
                throw new RuntimeException(r);
            }

            synchronized (state) {
                state.setDelay(i);
            }
        }
    }

    @Override
    public Status status() {
        synchronized (state) {
            return new Status(state.getInfo().getLength(), state.getInfo().getCount(), state.getInfo().getSource()
                    .toString(), state.getDelay(), speedInfo.getCurrentSpeed(), speedInfo.getAverageSpeed());
        }
    }

    @Override
    public Transfer.State save() {
        return state;
    }

    @Override
    public void load(Worker.State s) {
        state = (State) s;
    }

    @Override
    public String getSource() {
        synchronized (state) {
            return state.getInfo().getSource().toString();
        }
    }

    public void downloaded(String html) {
        synchronized (state) {
            state.getInfo().setCount(html.length());

            state.setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
            target();

            try {
                FileUtils.writeStringToFile(state.getTargetApp(), html);
            } catch (IOException e) {
                throw new DownloadError(e);
            }
        }
    }
}
