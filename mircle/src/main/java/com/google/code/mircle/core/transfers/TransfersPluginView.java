package com.google.code.mircle.core.transfers;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class TransfersPluginView implements View {

    TransfersPlugin plugin;

    public TransfersPluginView(TransfersPlugin p) {
        plugin = p;
    }

    public String toString() {
        return "Transfers";
    }

    @Override
    public Model getModel() {
        return plugin;
    }

    @Override
    public void close() {
    }

}
