package com.google.code.mircle;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.google.code.mircle.core.transfers.direct.LinkLabel;

public class AboutDialog extends JDialog {

    public String url;

    public AboutDialog() {
        setTitle("Mircle");
        final AboutDialog that = this;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);

        setBounds(100, 100, 467, 315);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0 };
        getContentPane().setLayout(gridBagLayout);

        JLabel lblDirectDownload = new JLabel();
        lblDirectDownload.setText("About");
        GridBagConstraints gbc_lblDirectDownload = new GridBagConstraints();
        gbc_lblDirectDownload.gridwidth = 1;
        gbc_lblDirectDownload.insets = new Insets(0, 0, 5, 0);
        gbc_lblDirectDownload.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDirectDownload.gridx = 0;
        gbc_lblDirectDownload.gridy = 0;
        getContentPane().add(lblDirectDownload, gbc_lblDirectDownload);

        JButton btnNewButton = new JButton("OK");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                that.dispose();
            }
        });

        JLabel lblMircle = new JLabel("Mircle");
        GridBagConstraints gbc_lblMircle = new GridBagConstraints();
        gbc_lblMircle.insets = new Insets(0, 0, 5, 0);
        gbc_lblMircle.gridx = 0;
        gbc_lblMircle.gridy = 1;
        getContentPane().add(lblMircle, gbc_lblMircle);
        
        LinkLabel lblWwwmirclenet = new LinkLabel("http://www.mircle.net");
        GridBagConstraints gbc_lblWwwmirclenet = new GridBagConstraints();
        gbc_lblWwwmirclenet.insets = new Insets(0, 0, 5, 0);
        gbc_lblWwwmirclenet.gridx = 0;
        gbc_lblWwwmirclenet.gridy = 2;
        getContentPane().add(lblWwwmirclenet, gbc_lblWwwmirclenet);

        JLabel lblNewLabel = new JLabel("New label");

        lblNewLabel.setText(App.class.getPackage().getImplementationVersion());

        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 3;
        getContentPane().add(lblNewLabel, gbc_lblNewLabel);

        GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
        gbc_btnNewButton.gridx = 0;
        gbc_btnNewButton.gridy = 4;
        getContentPane().add(btnNewButton, gbc_btnNewButton);
    }

    public boolean modal(Component root) {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(SwingUtilities.getRoot(root));
        setVisible(true);

        return url != null;
    }

    void download(String url) {
        this.url = url;
    }

}
