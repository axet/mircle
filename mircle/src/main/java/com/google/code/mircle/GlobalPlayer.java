package com.google.code.mircle;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.ini4j.Ini;
import org.ini4j.Profile.Section;

import com.github.axet.play.PlaySound;
import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;

public class GlobalPlayer extends JPanel {

    public interface PlayControl {
        public PlayObject getPlayObject();
    }

    public interface PlayObject {
        public URL getURL();

        public String getName();

        public void notifyStart();

        public void notifyStop();
    }

    PlaySound play;

    PlayControl pc;
    PlayObject pl;

    AppPlayerBottom bottom;

    public GlobalPlayer(final AppGUI app) {
        bottom = app.bottom;

        bottom.btnNewDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (play != null)
                    controlStop();
                else
                    controlPlay();
            }
        });

    }

    public void controlStop() {
        play.close();
        play = null;

        bottom.btnNewDownload.setText("Play");

        if (pl != null) {
            pl.notifyStop();
        }
    }

    public void control(PlayControl pc) {
        this.pc = pc;

        bottom.btnNewDownload.setEnabled(pc != null || pl != null);

        if (this.play == null) {
            if (pc != null) {
                pl = pc.getPlayObject();
                bottom.lblNewLabel.setText(pl.getName());
            }
        }
    }

    public void controlPlay() {
        if (pc != null) {
            pl = pc.getPlayObject();
        }
        if (pl != null)
            play(pl);
    }

    public void play(PlayObject pl) {
        if (this.pl != null) {
            this.pl.notifyStop();
        }
        this.pl = pl;
        bottom.btnNewDownload.setEnabled(true);

        DownloadInfo info = new DownloadInfo(pl.getURL());
        info.extract();

        if (info.getContentType().contains("audio/x-scpls")) {
            String html = WGet.getHtml(info.getSource());

            URL u = parsePlaylist(html);

            if (play != null) {
                controlStop();
            }
            play = new PlaySound();
            play.open(u);
            play.play();

            bottom.lblNewLabel.setText(pl.getName());
            pl.notifyStart();

            bottom.btnNewDownload.setText("Stop");
        }
    }

    URL parsePlaylist(String playlist) {
        try {
            InputStream is = new ByteArrayInputStream(playlist.getBytes("UTF-8"));
            Ini prefs = new Ini(is);

            Section s = prefs.get("playlist");

            int i = Integer.decode(s.get("numberofentries"));
            if (i <= 0)
                throw new RuntimeException("no entries");

            String file = s.get("File" + 1);
            return new URL(file);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
