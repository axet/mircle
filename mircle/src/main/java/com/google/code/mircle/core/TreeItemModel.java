package com.google.code.mircle.core;

import javax.swing.DefaultListModel;

/**
 * Root elements in the left pane
 * 
 * @author axet
 * 
 */
public interface TreeItemModel extends Model {

    public DefaultListModel getTreeModels();

}
