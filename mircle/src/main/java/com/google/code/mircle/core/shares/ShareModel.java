package com.google.code.mircle.core.shares;

import com.google.code.mircle.core.Model;

/**
 * ShareModel - Current Share on local machine.
 * 
 * Features:
 * 
 * 1) Scan folder content
 * 
 * 2) Subscribe for OS files changes events
 * 
 * 3) Keep cashed state
 * 
 * 4) Allow to store state into xml form
 * 
 * 5) Provide all information about files, stats, folders, size, etc..
 * 
 * 6) Support for offline mode (mark as unavailable. for remote shares, which is
 * currently not connected, like samba folders, or remote media like usb-flash)
 * 
 * 7) On View create torrent files / magnet files / magnet links
 * 
 * 8) Auto/Manual checks for file content and related torrent files
 * 
 * @author axet
 * 
 */
public class ShareModel implements Model {

    String name;
    String root;

    public ShareModel(String name, String root) {
        this.name = name;
        this.root = root;
    }

    public String toString() {
        return name + " --> " + root;
    }

    @Override
    public void close() {
    }

}
