package com.google.code.mircle;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.ModelExpand;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.TreeItemModel;
import com.google.code.mircle.core.View;

public class AppSwitch {
    HashMap<Model, DefaultMutableTreeNode> modelTreeNodeMap = new HashMap<Model, DefaultMutableTreeNode>();
    HashMap<Model, PluginRoot> modelPluginMap = new HashMap<Model, PluginRoot>();
    HashMap<Model, Controller> modelViewRuntimeMap = new HashMap<Model, Controller>();

    AppGUI appGUI;
    Core core;

    public AppSwitch(Core core, AppGUI g) {
        this.core = core;
        this.appGUI = g;
    }

    void load() {
        appGUI.left.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                TreePath p = e.getPath();
                DefaultMutableTreeNode o = (DefaultMutableTreeNode) p.getLastPathComponent();
                Object oo = (Object) o.getUserObject();

                if (appGUI.right.getComponentCount() > 0) {
                    Component view = appGUI.right.getComponent(0);
                    if (view instanceof Controller) {
                        Controller v = (Controller) view;
                        v.deactivated();
                    }
                }

                appGUI.right.removeAll();
                JComponent view;
                if (View.class.isAssignableFrom(oo.getClass())) {
                    Controller v = switchView((View) oo);
                    v.activated();
                    view = (JComponent) v;
                } else {
                    view = new JPanel();
                }
                appGUI.right.add(view, BorderLayout.CENTER);
                view.revalidate();
                view.repaint();
            }
        });
        appGUI.left.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                JTree tree = (JTree) e.getSource();
                if (e.getButton() == MouseEvent.BUTTON3) {
                    int selRow = tree.getRowForLocation(e.getX(), e.getY());
                    TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
                    if (selRow != -1) {
                        tree.setSelectionPath(selPath);

                        DefaultMutableTreeNode t = (DefaultMutableTreeNode) selPath.getLastPathComponent();
                        View oo = (View) t.getUserObject();
                        Object o = oo.getModel();
                        if (o instanceof Model) {
                            Model m = (Model) o;
                            PluginRoot p = modelPluginMap.get(m);
                            // TODO null pointer exption??? p == null. after you delete item and then click right click
                            JPopupMenu pop = p.contextMenu(m);
                            if (pop != null)
                                pop.show(tree, e.getX(), e.getY());
                        }
                        if (o instanceof PluginRoot) {
                            PluginRoot p = (PluginRoot) o;
                            JPopupMenu pop = p.contextMenu((Model) o);
                            if (pop != null)
                                pop.show(tree, e.getX(), e.getY());
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
            }

            @Override
            public void mouseClicked(MouseEvent arg0) {
            }
        });

        appGUI.left.setDragEnabled(true);
        appGUI.left.setDropMode(DropMode.ON);
        appGUI.left.setTransferHandler(new TransferHandler() {
            private static final long serialVersionUID = 4505807362939674906L;

            public int getSourceActions(JComponent c) {
                Model m = getSelectedModel();
                if (m == null)
                    return TransferHandler.NONE;
                PluginRoot p = modelPluginMap.get(m);
                return p.dndAction(m);
            }

            public boolean canImport(TransferHandler.TransferSupport support) {
                JTree.DropLocation l = (javax.swing.JTree.DropLocation) support.getDropLocation();
                TreePath p = l.getPath();

                DefaultMutableTreeNode n = (DefaultMutableTreeNode) p.getLastPathComponent();

                View mm = (View) n.getUserObject();
                Model m = mm.getModel();

                PluginRoot pp = modelPluginMap.get(m);

                Model mt = getSelectedModel();

                return pp.dndCheck(mt, m);
            }

            public boolean importData(TransferHandler.TransferSupport support) {
                JTree.DropLocation l = (javax.swing.JTree.DropLocation) support.getDropLocation();
                TreePath p = l.getPath();

                DefaultMutableTreeNode n = (DefaultMutableTreeNode) p.getLastPathComponent();

                View mm = (View) n.getUserObject();
                Model m = mm.getModel();

                PluginRoot pp = modelPluginMap.get(m);

                Model mt = getSelectedModel();

                return pp.dndDrop(mt, m);
            }

            protected Transferable createTransferable(JComponent c) {
                return new StringSelection(AppSwitch.class.getSimpleName());
            }

            protected void exportDone(JComponent c, Transferable t, int action) {
                if (action == MOVE) {
                    appGUI.left.setSelectionPath(null);
                }
            }
        });

        appGUI.left.addTreeWillExpandListener(new TreeWillExpandListener() {

            @Override
            public void treeWillExpand(TreeExpansionEvent evt) throws ExpandVetoException {
                TreePath path = evt.getPath();

                DefaultMutableTreeNode n = (DefaultMutableTreeNode) path.getLastPathComponent();
                View mm = (View) n.getUserObject();
                Model m = mm.getModel();

                if (m instanceof ModelExpand) {
                    ModelExpand me = (ModelExpand) m;
                    me.setCollapsed(false);
                }

                boolean veto = false;
                if (veto) {
                    throw new ExpandVetoException(evt);
                }
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent evt) throws ExpandVetoException {
                TreePath path = evt.getPath();

                DefaultMutableTreeNode n = (DefaultMutableTreeNode) path.getLastPathComponent();
                View mm = (View) n.getUserObject();
                Model m = mm.getModel();

                if (m instanceof ModelExpand) {
                    ModelExpand me = (ModelExpand) m;
                    me.setCollapsed(true);
                }

                boolean veto = false;
                if (veto) {
                    throw new ExpandVetoException(evt);
                }
            }
        });
    }

    public void close() {
        for (Model m : modelViewRuntimeMap.keySet()) {
            Controller v = modelViewRuntimeMap.get(m);
            v.close();
        }
        modelViewRuntimeMap.clear();
        modelPluginMap.clear();
        modelTreeNodeMap.clear();
    }

    public void modelSwitch(Class<? extends Model> m) {
        DefaultTreeModel model = (DefaultTreeModel) appGUI.left.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        modelSwitch(root, m);
    }

    boolean modelSwitch(DefaultMutableTreeNode root, Class<? extends Model> m) {
        for (Enumeration<?> e = root.children(); e.hasMoreElements();) {
            DefaultMutableTreeNode n = (DefaultMutableTreeNode) e.nextElement();
            View r = (View) n.getUserObject();
            Model rr = r.getModel();
            if (m.isAssignableFrom(rr.getClass())) {
                appGUI.left.setSelectionPath(new TreePath(n.getPath()));
                return true;
            }

            if (!n.isLeaf()) {
                if (modelSwitch(n, m))
                    return true;
            }
        }

        return false;
    }

    public void modelSwitch(Class<? extends Model> root, Model m) {
        DefaultTreeModel model = (DefaultTreeModel) appGUI.left.getModel();
        DefaultMutableTreeNode r = (DefaultMutableTreeNode) model.getRoot();
        modelSwitch(r, m);
    }

    boolean modelSwitch(DefaultMutableTreeNode root, Model m) {
        for (Enumeration<?> e = root.children(); e.hasMoreElements();) {
            DefaultMutableTreeNode n = (DefaultMutableTreeNode) e.nextElement();
            View rr = (View) n.getUserObject();
            Model r = rr.getModel();
            if (m.equals(r)) {
                appGUI.left.setSelectionPath(new TreePath(n.getPath()));
                return true;
            }

            if (!n.isLeaf()) {
                if (modelSwitch(n, m))
                    return true;
            }
        }

        return false;
    }

    public Controller getView(Model o) {
        return modelViewRuntimeMap.get(o);
    }

    Controller switchView(View v) {
        Model model = v.getModel();
        Controller view = getView(model);
        if (view == null) {
            PluginRoot p = modelPluginMap.get(model);

            view = p.createController(model);

            modelViewRuntimeMap.put(model, view);
        }

        return view;
    }

    public String getSelectedText() {
        DefaultMutableTreeNode n = null;
        TreePath path = appGUI.left.getSelectionPath();
        if (path != null) {
            n = (DefaultMutableTreeNode) path.getLastPathComponent();

            return n.toString();
        }

        return null;
    }

    public View getSelectedView() {
        DefaultMutableTreeNode n = null;
        TreePath path = appGUI.left.getSelectionPath();
        if (path != null) {
            n = (DefaultMutableTreeNode) path.getLastPathComponent();

            View mm = (View) n.getUserObject();
            return mm;
        }

        return null;
    }

    public Model getSelectedModel() {
        View mm = getSelectedView();

        if (mm != null) {
            Model m = mm.getModel();
            return m;
        }

        return null;
    }

    public void loadPlugins() {
        load();

        final DefaultTreeModel model = (DefaultTreeModel) appGUI.left.getModel();
        final DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        for (Plugin plugin : core.plugins) {
            if (plugin instanceof PluginRoot) {
                final PluginRoot pluginRoot = (PluginRoot) plugin;
                final DefaultMutableTreeNode pluginRootNode = new DefaultMutableTreeNode(
                        pluginRoot.createView(pluginRoot));
                loadTreeModels(pluginRootNode, model, pluginRoot, pluginRoot);
                root.add(pluginRootNode);
            }
            if (plugin instanceof Model)
                modelPluginMap.put((Model) plugin, (PluginRoot) plugin);
        }
        model.nodeStructureChanged((MutableTreeNode) model.getRoot());

        for (int i = 0; i < appGUI.left.getRowCount(); i++)
            appGUI.left.expandRow(i);
    }

    void loadTreeModels(final DefaultMutableTreeNode pluginRootNode, final DefaultTreeModel model,
            final PluginRoot pluginRoot, final TreeItemModel treeModel) {
        final DefaultListModel models = treeModel.getTreeModels();
        if (models != null) {
            ListDataListener l = new ListDataListener() {
                @Override
                public void intervalRemoved(ListDataEvent arg0) {
                    for (int i = arg0.getIndex0(); i <= arg0.getIndex1(); i++) {
                        DefaultMutableTreeNode dm = (DefaultMutableTreeNode) pluginRootNode.getChildAt(i);
                        View mm = (View) dm.getUserObject();
                        TreeItemModel m = (TreeItemModel) mm.getModel();
                        modelPluginMap.remove(m);
                        DefaultMutableTreeNode t = modelTreeNodeMap.remove(m);
                        model.removeNodeFromParent(t);
                    }
                }

                @Override
                public void intervalAdded(ListDataEvent arg0) {
                    for (int i = arg0.getIndex0(); i <= arg0.getIndex1(); i++) {
                        TreeItemModel m = (TreeItemModel) models.get(i);
                        DefaultMutableTreeNode t = new DefaultMutableTreeNode(pluginRoot.createView(m));
                        model.insertNodeInto(t, pluginRootNode, i);
                        loadTreeModels(t, model, pluginRoot, m);
                        modelPluginMap.put(m, pluginRoot);
                        modelTreeNodeMap.put(m, t);
                    }
                    if (treeModel instanceof ModelExpand) {
                        ModelExpand e = (ModelExpand) treeModel;
                        appGUI.left.setColapse(new TreePath(pluginRootNode.getPath()), e.getCollapsed());
                    } else {
                        appGUI.left.setColapse(new TreePath(pluginRootNode.getPath()), false);
                    }
                }

                @Override
                public void contentsChanged(ListDataEvent arg0) {
                    for (int i = arg0.getIndex0(); i <= arg0.getIndex1(); i++) {
                        TreeItemModel m = (TreeItemModel) models.get(i);
                        DefaultMutableTreeNode t = modelTreeNodeMap.get(m);
                        model.nodeChanged(t);
                    }
                }
            };
            models.addListDataListener(l);

            if (models.getSize() > 0)
                l.intervalAdded(new ListDataEvent(models, ListDataEvent.INTERVAL_ADDED, 0, models.getSize() - 1));
        }
    }

    DefaultMutableTreeNode getNode(Model m) {
        return modelTreeNodeMap.get(m);
    }
}
