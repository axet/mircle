package com.google.code.mircle.core;

/**
 * Model's who support nodes expand / colapse control (RSSFolder) should
 * implement this interface
 * 
 * @author axet
 * 
 */
public interface ModelExpand {

    public boolean getCollapsed();

    public void setCollapsed(boolean e);

}
