package com.google.code.mircle.plugins.subscriptions.controller;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.LinkLabel;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.core.transfers.direct.Transfer.State;
import com.google.code.mircle.plugins.subscriptions.model.FeedEntry;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolder;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;
import com.google.code.mircle.plugins.subscriptions.view.FeedEntryView;

public class FeedFolderDownView extends JPanel {
    private static final long serialVersionUID = -5843882598074012891L;

    DefaultListModel listModel = new DefaultListModel();

    LinkLabel lblUrl = new LinkLabel();
    JLabel lblNewLabel_1;

    FeedEntryView entryView;
    RSSFolder model;
    Core core;
    Transfer t;
    Worker.Status status;
    JLabel lblErrortext;
    JLabel lblFeedname;

    public FeedFolderDownView(Core c, RSSFolder m) {
        super(new GridBagLayout());

        this.core = c;
        this.model = m;

        JButton btnMarkAsRead = new JButton("Mark as Read");
        btnMarkAsRead.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FeedEntry entry = (FeedEntry) entryView.getModel();

                // if we click on unread button. and task already sheduled. stop
                // download it. remove from downloads. and mark entry as unread
                if (entry.direct) {
                    Transfer t = model.getTransfer(entry);
                    if (t != null)
                        core.transfers.getDirect().del(t);

                    entry.direct = false;
                    entry.unread = true;
                } else {
                    entry.unread = !entry.unread;
                }

                model.notifyChanged(entry);
            }
        });
        GridBagConstraints gbc_btnMarkAsRead = new GridBagConstraints();
        gbc_btnMarkAsRead.insets = new Insets(0, 0, 5, 5);
        gbc_btnMarkAsRead.gridx = 0;
        gbc_btnMarkAsRead.gridy = 0;
        add(btnMarkAsRead, gbc_btnMarkAsRead);

        JButton btnDownload = new JButton("Download");
        btnDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                FeedEntry entry = (FeedEntry) entryView.getModel();
                model.download(entry);
            }
        });
        GridBagConstraints gbc_btnDownload = new GridBagConstraints();
        gbc_btnDownload.insets = new Insets(0, 0, 5, 5);
        gbc_btnDownload.gridx = 1;
        gbc_btnDownload.gridy = 0;
        add(btnDownload, gbc_btnDownload);

        JButton btnOpenFile = new JButton("Open File");
        GridBagConstraints gbc_btnOpenFile = new GridBagConstraints();
        gbc_btnOpenFile.insets = new Insets(0, 0, 5, 5);
        gbc_btnOpenFile.gridx = 2;
        gbc_btnOpenFile.gridy = 0;
        add(btnOpenFile, gbc_btnOpenFile);

        JButton btnDeleteFile = new JButton("Delete File");
        GridBagConstraints gbc_btnDeleteFile = new GridBagConstraints();
        gbc_btnDeleteFile.insets = new Insets(0, 0, 5, 0);
        gbc_btnDeleteFile.gridx = 3;
        gbc_btnDeleteFile.gridy = 0;
        add(btnDeleteFile, gbc_btnDeleteFile);

        JLabel lblNewLabel = new JLabel("Source URL:");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 1;
        add(lblNewLabel, gbc_lblNewLabel);

        lblUrl = new LinkLabel();
        lblUrl.setText("URL");
        GridBagConstraints gbc_lblUrl = new GridBagConstraints();
        gbc_lblUrl.insets = new Insets(0, 0, 5, 0);
        gbc_lblUrl.gridx = 1;
        gbc_lblUrl.gridwidth = 3;
        gbc_lblUrl.gridy = 1;
        add(lblUrl, gbc_lblUrl);

        JLabel lblDate = new JLabel("Date");
        GridBagConstraints gbc_lblDate = new GridBagConstraints();
        gbc_lblDate.insets = new Insets(0, 0, 5, 5);
        gbc_lblDate.gridx = 0;
        gbc_lblDate.gridy = 2;
        add(lblDate, gbc_lblDate);

        lblNewLabel_1 = new JLabel("Date");
        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_1.gridx = 1;
        gbc_lblNewLabel_1.gridy = 2;
        add(lblNewLabel_1, gbc_lblNewLabel_1);

        JLabel lblError = new JLabel("Error");
        GridBagConstraints gbc_lblError = new GridBagConstraints();
        gbc_lblError.insets = new Insets(0, 0, 5, 5);
        gbc_lblError.gridx = 0;
        gbc_lblError.gridy = 3;
        add(lblError, gbc_lblError);

        lblErrortext = new JLabel("ErrorText");
        GridBagConstraints gbc_lblErrortext = new GridBagConstraints();
        gbc_lblErrortext.insets = new Insets(0, 0, 5, 5);
        gbc_lblErrortext.gridx = 1;
        gbc_lblErrortext.gridy = 3;
        add(lblErrortext, gbc_lblErrortext);

        JLabel lblFeed = new JLabel("Feed");
        GridBagConstraints gbc_lblFeed = new GridBagConstraints();
        gbc_lblFeed.insets = new Insets(0, 0, 0, 5);
        gbc_lblFeed.gridx = 0;
        gbc_lblFeed.gridy = 4;
        add(lblFeed, gbc_lblFeed);

        lblFeedname = new JLabel("FeedName");
        GridBagConstraints gbc_lblFeedname = new GridBagConstraints();
        gbc_lblFeedname.insets = new Insets(0, 0, 0, 5);
        gbc_lblFeedname.gridx = 1;
        gbc_lblFeedname.gridy = 4;
        add(lblFeedname, gbc_lblFeedname);

        JButton btnOpenfeed = new JButton("OpenFeed");
        btnOpenfeed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                RSSModel m = ((FeedEntry) entryView.getModel()).model;
                core.app.appSwitch.modelSwitch(core.rss.getClass(), m);
            }
        });
        GridBagConstraints gbc_btnOpenfeed = new GridBagConstraints();
        gbc_btnOpenfeed.insets = new Insets(0, 0, 0, 5);
        gbc_btnOpenfeed.gridx = 2;
        gbc_btnOpenfeed.gridy = 4;
        add(btnOpenfeed, gbc_btnOpenfeed);
    }

    public void update(FeedEntryView e) {
        entryView = e;

        FeedEntry entry = (FeedEntry) entryView.getModel();

        try {
            lblUrl.setURL(new URL(entry.link));
        } catch (MalformedURLException e1) {
            lblUrl.setText(entry.link);
        }

        lblNewLabel_1.setText(entry.date.toString());

        if (entry.exception != null)
            lblErrortext.setText(entry.exception.toString());
        else {
            Transfer t = model.getTransfer(entry);
            if (t != null) {
                Transfer.State s = (State) t.getState();
                if (s.getException() != null) {
                    lblErrortext.setText(s.getException().toString());
                }
            }
        }

        RSSModel m = ((FeedEntry) e.getModel()).model;
        lblFeedname.setText(m.getName());
    }

}
