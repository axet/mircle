package com.google.code.mircle.core.upnp;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class UPnPView extends JPanel implements Controller {

    UPnPPlugin s;

    public UPnPView(UPnPPlugin s) {
        super(new BorderLayout());

        this.s = s;
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }


}
