package com.google.code.mircle.core;

import java.util.HashSet;
import java.util.Set;

public abstract class LongRun {

    Thread thread;

    Set<Listener> list = new HashSet<LongRun.Listener>();

    public static interface Listener {
        /**
         * LongRun start task
         */
        public void start();

        /**
         * LongRun changed task
         */
        public void changed();

        /**
         * LongRun stop task
         */
        public void stop();

        /**
         * LongRun closed
         */
        public void close();
    }

    public static interface LongRunNotify {
        public void run(Runnable notify);
    }

    public void addListener(Listener l) {
        if (l == null)
            throw new NullPointerException();

        list.add(l);
    }

    public void removeListener(Listener l) {
        list.remove(l);
    }

    public void notifyLongRunChanged() {
        for (Listener l : new HashSet<LongRun.Listener>(list)) {
            l.changed();
        }
    }

    public void close() {
        if (isAlive()) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        for (Listener l : new HashSet<LongRun.Listener>(list)) {
            l.close();
        }
    }

    public void start() {
        final LongRun that = this;

        longRun(new LongRunNotify() {
            @Override
            public void run(Runnable notify) {
                that.run(notify);
            }
        });
    }

    public void longRun(final LongRunNotify r) {
        if (thread != null && thread.isAlive())
            throw new RuntimeException("already started");

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        for (Listener l : new HashSet<LongRun.Listener>(list)) {
                            l.changed();
                            l.start();
                        }
                    }
                });

                r.run(new Runnable() {
                    @Override
                    public void run() {
                        Core.invoke(new Runnable() {
                            @Override
                            public void run() {
                                for (Listener l : new HashSet<LongRun.Listener>(list))
                                    l.changed();
                            }
                        });
                    }
                });

                // send changed event after this thread done (download complete)
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            thread.join();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                        for (Listener l : new HashSet<LongRun.Listener>(list)) {
                            l.changed();
                            l.stop();
                        }
                    }
                });
            }
        }, "Long run thread " + getLongRunName());
        thread.start();
    }

    public boolean isAlive() {
        if (thread == null)
            return false;

        return thread.isAlive();
    }

    public abstract void run(Runnable notify);

    /**
     * the name of the download. uses to represents thread
     * 
     * @return getSource() as URL string representation
     */
    public abstract String getLongRunName();
}
