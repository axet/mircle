package com.google.code.mircle.plugins.subscriptions.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.Transfer;

public class RSSFolderExpand {
    // virtual feeds collected from this folder items. build on query for fast
    // indexed access. Updated each RSSModel event
    ArrayList<FeedEntry> rssModels = new ArrayList<FeedEntry>();

    RSSFolder folder;

    //
    // expand logic
    //

    public RSSFolderExpand(RSSFolder folder) {
        this.folder = folder;
    }

    public static class RSSModelExpand {
        // model
        public RSSModel m;
        // current feed extracted index
        public int index;
        // feed entries
        public List<FeedEntry> entries;
        // current entry
        public FeedEntry e;
        // current entry actual date
        public Date date;

        public RSSModelExpand(RSSModel m) {
            this.m = m;
            this.entries = m.getState().entries;

            update();
        }

        FeedEntry getCurrent() {
            if (entries.size() <= index)
                return null;
            return entries.get(index);
        }

        void next() {
            index++;
            update();
        }

        void update() {
            this.e = getCurrent();
            if (e != null) {
                this.date = e.dateReceived;
                if (this.date == null)
                    this.date = e.date;
            } else {
                this.date = null;
            }
        }
    }

    static class SortByDate implements Comparator<RSSModelExpand> {
        @Override
        public int compare(RSSModelExpand arg0, RSSModelExpand arg1) {
            // move to the end all entrys with no entry
            if (arg0.e == null && arg1.e == null)
                return 0;
            if (arg0.e == null)
                return 1;
            if (arg1.e == null)
                return -1;

            Date d1 = arg0.date;
            Date d2 = arg1.date;
            if (d1 == null && d2 == null)
                return 0;
            if (d1 == null)
                return -1;
            if (d2 == null)
                return 1;
            if (d1.after(d2))
                return -1;
            if (d1.before(d2))
                return 1;
            return 0;
        }

    }

    public static void expand(ArrayList<RSSModelExpand> expand, RSSFolder r) {
        for (Enumeration<?> e = r.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                expand.add(new RSSModelExpand(m));
            }
            if (t instanceof RSSFolder) {
                RSSFolder rr = (RSSFolder) t;
                expand(expand, rr);
            }
        }
    }

    public static RSSModelExpand getRecent(ArrayList<RSSModelExpand> expand) {
        Collections.sort(expand, new SortByDate());
        return expand.get(0);
    }

    void refreshItems() {
        int more = rssModels.size() - 1;
        if (more < 0)
            return;
        loadMoreItems(more);
    }

    void loadMoreItems(int upToIndex) {
        ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();

        expand(expand, folder);

        for (int i = 0; i <= upToIndex; i++) {
            RSSModelExpand e = getRecent(expand);
            if (e.e == null)
                throw new RuntimeException("no such upToIndex for specified folder " + upToIndex);

            if (rssModels.size() == i) {
                rssModels.add(e.e);
            } else if (rssModels.size() < i) {
                throw new RuntimeException("should never happens");
            } else {
                rssModels.set(i, e.e);
                folder.notifyChanged(e.e);
            }

            e.next();
        }
    }

    public int indexOf(FeedEntry e) {
        // search on virtual list (first 25 items, more relaible, fast search
        // then expand search)
        int i = rssModels.indexOf(e);
        if (i >= 0)
            return i;

        // not found on virtual list. trying to digg down and expand all folders
        ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();
        expand(expand, folder);
        int max = folder.getSize();
        for (int ii = 0; ii < max; ii++) {
            RSSModelExpand ee = getRecent(expand);
            if (ee.e.equals(e))
                return ii;
            ee.next();
        }

        throw new RuntimeException("folder dose not conatin item " + e);
    }
}
