package com.google.code.mircle.core.player;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.google.code.mircle.App;
import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;

public class PlayerPlugin implements Plugin, Model, PluginRoot {

    PlayerView view;
    App app;

    public PlayerPlugin(App app) {
        this.app = app;
        view = new PlayerView(app, this);
    }

    public void close() {

    }

    public String getName() {
        return "Player";
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
    }

    public JPopupMenu contextMenu(Model m) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem mm = new JMenuItem("123");
        mm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        popup.add(mm);
        return popup;
    }

    public Controller createController(Model m) {
        if (m instanceof PlayerPlugin)
            return view;
        return null;
    }

    public void play(File file) {
        app.appSwitch. modelSwitch(getClass());
        view.play(file);
    }

    @Override
    public void save(File path) {
    }

    @Override
    public void load(File path) {
    }

    @Override
    public View createView(Model m) {
        return new PlayerPluginView(this);
    }

    @Override
    public int dndAction(Model m) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

}
