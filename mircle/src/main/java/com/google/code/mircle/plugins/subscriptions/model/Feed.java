package com.google.code.mircle.plugins.subscriptions.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Feed")
public class Feed {
    private String title;
    // how many unread items
    private long unreadCount;
    // how many new added items
    private long newCount;

    synchronized public String getTitle() {
        return title;
    }

    synchronized public void setTitle(String title) {
        this.title = title;
    }

    synchronized public long getUnreadCount() {
        return unreadCount;
    }

    synchronized public void setUnreadCount(long unreadCount) {
        this.unreadCount = unreadCount;
    }

    synchronized public long getNewCount() {
        return newCount;
    }

    synchronized public void setNewCount(long newCount) {
        this.newCount = newCount;
    }
}
