package com.google.code.mircle.plugins.radio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.time.DateUtils;

import com.github.axet.shoutcast.SHOUTcast;
import com.github.axet.shoutcast.SHOUTgenre;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.TreeItemModel;
import com.google.code.mircle.core.Worker;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class SHOUTcastModel extends Worker implements TreeItemModel, Model {

    enum States {
        DONE, ERROR, LOAD_GENRES, LOAD_STATIONS
    }

    @XStreamAlias("SHOUTcastState")
    static class State extends Worker.State {
        // state version
        Long version = new Long(1);
        // last time genres been downloaded
        Date genresDate;
        // generes. store purpose. private variable only accesable from load /
        // save methods
        List<SHOUTgenreModel.State> genres = new ArrayList<SHOUTgenreModel.State>();
        // view data
        Integer selectedGenreIndex;
        Integer selectedStationIndex;
    }

    State state = new State();

    private States st = States.DONE;

    private Throwable e;

    SHOUTcast c = new SHOUTcast();

    DefaultTableModel genresViews = new DefaultTableModel() {
        private static final long serialVersionUID = 9204556227575758036L;

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };
    List<SHOUTgenreModel> genresModels = new ArrayList<SHOUTgenreModel>();

    // system function. run checks every minute
    static final int REFRESH_EVERY = 60 * 1000;
    // refresh genres every 1 hour
    static final int REFRESH_GENRES_EVERY = 60 * 60 * 1000;
    // refresh stations every 10 minutes (listeners? valuable info)
    static final int REFRESH_STATIONS_EVERY = 10 * 60 * 1000;

    // max amount of concurent downloads
    static final int MAX_CONCURRENT_DOWNLOADS = 3;

    // count current downloads
    int count = 0;

    RadioPlugin p;

    public SHOUTcastModel(RadioPlugin p) {
        this.p = p;

        SHOUTgenreView.initTree(genresViews);

        getState().setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
    }

    void timer() {
        Core.invoke(REFRESH_EVERY, new Runnable() {
            @Override
            public void run() {
                refresh();

                timer();
            }
        }, "SHOUTcast Refresh");
    }

    public void refresh() {
        // do not run twice
        if (isAlive())
            return;

        // not yet extracted. do so.
        if (empty()) {
            start();
            return;
        }

        // do not refresh it too often
        Date date = DateUtils.addMilliseconds(getState().genresDate, REFRESH_GENRES_EVERY);
        if (date.before(new Date())) {
            start();
            return;
        }
    }

    @Override
    public void close() {
    }

    @Override
    public State save() {
        state.genres = new ArrayList<SHOUTgenreModel.State>();

        for (SHOUTgenreModel m : genresModels) {
            state.genres.add(m.save());
        }

        return state;
    }

    @Override
    public void load(Worker.State state) {
        this.state = (State) state;

        for (SHOUTgenreModel.State g : this.state.genres) {
            SHOUTgenreModel m = new SHOUTgenreModel(this, g);
            genresModels.add(m);
            genresViews.addRow(new Object[] { new SHOUTgenreView(m) });
        }

        // clean it up. we shall not use until save();
        this.state.genres = null;

        timer();
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
    }

    public DefaultTableModel getGenres() {
        return genresViews;
    }

    public boolean empty() {
        if (genresModels == null)
            return true;
        return genresModels.isEmpty();
    }

    public class GenresSortByName implements Comparator<SHOUTgenreModel> {
        @Override
        public int compare(SHOUTgenreModel o1, SHOUTgenreModel o2) {
            return o1.getGenre().getName().compareTo(o2.getGenre().getName());
        }
    }

    boolean exist(SHOUTgenre g) {
        for (SHOUTgenreModel m : genresModels) {
            if (m.getGenre().equals(g))
                return true;
        }

        return false;
    }

    public void populateGenres(AtomicBoolean stop, Runnable notify) {
        c.extract(stop, notify);

        // delete old genres
        for (int i = 0; i < genresModels.size(); i++) {
            SHOUTgenreModel m = genresModels.get(i);
            if (!c.getGenres().contains(m.getGenre())) {
                genresModels.remove(i);
                // retry the same index again
                i--;
            }
        }

        // add new genres
        for (SHOUTgenre g : c.getGenres()) {
            if (!exist(g))
                genresModels.add(new SHOUTgenreModel(this, g));
        }

        Collections.sort(genresModels, new GenresSortByName());

        state.genresDate = new Date();
    }

    void updateGenres() {
        // delete genres no longer present in the list
        for (int i = 0; i < genresViews.getRowCount(); i++) {
            SHOUTgenreView t = (SHOUTgenreView) genresViews.getValueAt(i, 0);
            SHOUTgenreModel m = (SHOUTgenreModel) t.getModel();

            if (!genresModels.contains(m)) {
                if (m.removeNormal())
                    genresViews.removeRow(i);
                // restart current index check
                i--;
            }
        }

        // add new genres
        for (int i = 0; i < genresModels.size(); i++) {
            SHOUTgenreModel g = genresModels.get(i);
            if (!exist(g)) {
                genresViews.insertRow(i, new Object[] { new SHOUTgenreView(g) });
            }
        }
    }

    boolean exist(SHOUTgenreModel g) {
        int i = indexOf(genresViews, g);

        return i != -1;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    protected void run() {
        setSt(States.LOAD_GENRES);
        p.notifySHOUTcast();

        try {
            populateGenres(stop, new Runnable() {
                @Override
                public void run() {
                    p.notifySHOUTcast();
                }
            });
            setSt(States.DONE);
            p.notifySHOUTcast();

            Core.invoke(new Runnable() {
                @Override
                public void run() {
                    updateGenres();
                }
            });

        } catch (RuntimeException e) {
            this.setE(e);
            setSt(States.ERROR);
            p.notifySHOUTcast();
        }
    }

    @Override
    public String getSource() {
        return null;
    }

    @Override
    public Status status() {
        return null;
    }

    synchronized public States getSt() {
        return st;
    }

    synchronized public void setSt(States st) {
        this.st = st;
    }

    synchronized public Throwable getE() {
        return e;
    }

    synchronized public void setE(Throwable e) {
        this.e = e;
    }

    void genreChanged(SHOUTgenreModel m) {
        int i = indexOf(genresViews, m);

        if (i == -1)
            throw new RuntimeException("not found");

        genresViews.setValueAt(genresViews.getValueAt(i, 0), i, 0);
    }

    int indexOf(DefaultTableModel m, SHOUTgenreModel s) {
        for (int i = 0; i < m.getRowCount(); i++) {
            SHOUTgenreView ss = (SHOUTgenreView) m.getValueAt(i, 0);
            SHOUTgenreModel mm = (SHOUTgenreModel) ss.getModel();

            if (mm.equals(s))
                return i;
        }

        return -1;
    }

    /**
     * click on genre. we probably have to redownload the stations list
     * 
     * @param t
     */
    public void click(SHOUTgenreModel t) {
        // do not start twice
        if (t.isAlive())
            return;

        // if station has been update recently. do not update.
        Date d = t.getState().stationsDate;
        if (d == null) {
            start(t);
        } else {
            Date date = DateUtils.addMilliseconds(d, REFRESH_STATIONS_EVERY);
            if (date.before(new Date())) {
                start(t);
            }
        }
    }

    public void start(final SHOUTgenreModel t) {
        if (count >= MAX_CONCURRENT_DOWNLOADS) {
            t.putToQueue();
            t.setQueueDate(new Date());
            genreChanged(t);
            return;
        }

        t.addListener(new LongRun.Listener() {
            @Override
            public void stop() {
                final LongRun.Listener that = this;

                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        t.removeListener(that);
                        count--;
                        startNext();
                    }
                });
            }

            @Override
            public void start() {
            }

            @Override
            public void changed() {
            }

            @Override
            public void close() {
            }
        });

        count++;
        t.start();
        genreChanged((SHOUTgenreModel) t);
    }

    public class SortByQueueDate implements Comparator<SHOUTgenreModel> {
        @Override
        public int compare(SHOUTgenreModel o1, SHOUTgenreModel o2) {
            if (o2.getQueueDate() == null && o1.getQueueDate() == null)
                return 0;
            if (o1.getQueueDate() == null)
                return 1;
            if (o2.getQueueDate() == null)
                return -1;

            return o2.getQueueDate().compareTo(o1.getQueueDate());
        }
    }

    public void startNext() {
        ArrayList<SHOUTgenreModel> models = new ArrayList<SHOUTgenreModel>(genresModels);

        Collections.sort(models, new SortByQueueDate());

        for (SHOUTgenreModel t : models) {
            if (t.getState().getDownloadState() == DownloadState.DOWNLOAD_QUEUED) {
                start(t);

                // stop tagging tasks only if here is a limit of downloads
                if (count >= MAX_CONCURRENT_DOWNLOADS)
                    return;
            }
        }
    }
}
