package com.google.code.mircle.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.io.FileUtils;

import com.github.axet.desktop.Desktop;
import com.github.axet.desktop.DesktopFolders;
import com.google.code.mircle.App;
import com.google.code.mircle.AppError;
import com.google.code.mircle.GlobalPlayer;
import com.google.code.mircle.core.player.PlayerPlugin;
import com.google.code.mircle.core.transfers.TransfersPlugin;
import com.google.code.mircle.plugins.radio.RadioPlugin;
import com.google.code.mircle.plugins.subscriptions.SubscriptionsPlugin;
import com.google.common.collect.Iterables;
import com.thoughtworks.xstream.XStream;

public class Core {

    public final TransfersPlugin transfers;
    public final PlayerPlugin playerPlugin;
    public final SubscriptionsPlugin rss;
    public final RadioPlugin radio;

    public List<Plugin> plugins;

    File lock;
    FileOutputStream lockStream;
    FileLock lockStreamLock;

    public App app;

    public DesktopFolders desktop = Desktop.getDesktopFolders();

    public File shareDir = FileUtils.getFile(desktop.getAppData(), "Mircle");
    public File pluginsDir = FileUtils.getFile(shareDir, "plugins");

    public Core(App app) {
        this.app = app;
        
        AppError.savePath  = shareDir;

        transfers = new TransfersPlugin(this);
        playerPlugin = new PlayerPlugin(app);
        rss = new SubscriptionsPlugin(this);
        radio = new RadioPlugin(this);

        // put plugins in the right order. keep initialize depenecies.
        //
        // it will initialize plugin in the normal order, and close it in the
        // reverce order.
        //
        // keep it in mide: if your plugin need for transfers and transfers
        // closes first you will receive a lot of
        // Transfer.LongRun.Listerner.close events. But if your plugin closes
        // first, you will unsubscribe before.
        plugins = Arrays.asList(new Plugin[] { transfers, rss, radio });
    }

    public void close() {
        activesClose();

        for (Plugin p : Iterables.reverse(plugins)) {
            p.close();
        }

        save();
    }

    public void save() {
        for (Plugin p : plugins) {
            save(p);
        }

        app.save(shareDir);
    }

    public void save(Plugin p) {
        String klass = p.getClass().getName();
        File f = FileUtils.getFile(pluginsDir, klass);
        p.save(f);
    }

    void lock(File dir) {
        lock = FileUtils.getFile(dir, "lock");
        try {
            lockStream = new FileOutputStream(lock);
            lockStreamLock = lockStream.getChannel().tryLock();
            if (lockStreamLock == null) {
                throw new RuntimeException("already here");
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void load() {
        try {
            FileUtils.forceMkdir(shareDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        lock(shareDir);

        for (Plugin p : plugins) {
            String klass = p.getClass().getName();
            File f = FileUtils.getFile(pluginsDir, klass);
            if (f.exists())
                p.load(f);
        }

        app.load(shareDir);
    }

    public static void invoke(Runnable r) {
        SwingUtilities.invokeLater(r);
    }

    /**
     * big string wont return getBytes() that is why FileUtils.writeStringToFile
     * fails
     * 
     * https://github.com/axet/mircle/issues/14
     */
    void writeStringToFile(File f, String xml, String ecoding) {
        try {
            String[] lines = xml.split("\\r?\\n");
            FileOutputStream os = new FileOutputStream(f);
            for (String l : lines) {
                os.write(l.getBytes(ecoding));
                os.write("\n".getBytes(ecoding));
            }
            os.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void save(Object state, XStream stream, File f) {
        String xml = stream.toXML(state);
        save(xml, f);
    }

    // have problems with encoding (russian saves as ??????)
    public void saveStream(Object state, XStream stream, File f) {
        try {
            FileUtils.forceMkdir(f.getParentFile());
            File fsave = new File(f.getPath() + ".save");
            FileOutputStream fout = new FileOutputStream(fsave);
            stream.toXML(state, fout);
            fout.close();
            FileUtils.deleteQuietly(f);
            FileUtils.moveFile(fsave, f);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * do not use xml to store data. you will get produce OutOfMemory exception
     * on big data
     * 
     * https://github.com/axet/mircle/issues/14
     * 
     * @param xml
     * @param f
     */
    public void save(String xml, File f) {
        try {
            FileUtils.forceMkdir(f.getParentFile());
            File fold = new File(f.getPath() + ".old");
            File fsave = new File(f.getPath() + ".save");
            FileUtils.writeStringToFile(fsave, xml, "UTF-8");
            FileUtils.deleteQuietly(fold);
            if (f.exists())
                FileUtils.moveFile(f, fold);
            FileUtils.moveFile(fsave, f);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static Vector<Thread> actives = new Vector<Thread>();

    void activesClose() {
        Thread t;

        while ((t = activesPop()) != null) {
            t.interrupt();
            try {
                t.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    Thread activesPop() {
        synchronized (actives) {
            if (actives.isEmpty())
                return null;
            return actives.get(0);
        }
    }

    public static void invoke(final int delay, final Runnable r) {
        invoke(delay, r, null);
    }

    public static void invoke(final int delay, final Runnable r, String name) {
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(delay);

                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            r.run();
                        }
                    });
                } catch (Exception e) {
                    Thread.interrupted();
                    return;
                } finally {
                    synchronized (actives) {
                        actives.remove(Thread.currentThread());
                    }
                }
            }
        }, "Core Invoke Later: " + name);
        synchronized (actives) {
            actives.add(t);
        }
        t.start();
    }
}
