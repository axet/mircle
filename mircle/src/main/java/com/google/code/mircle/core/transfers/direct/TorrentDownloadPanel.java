package com.google.code.mircle.core.transfers.direct;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.io.FileUtils;

import com.google.code.mircle.core.LongRun.LongRunNotify;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class TorrentDownloadPanel extends JPanel {

    Transfer transfer;
    JLabel lblDoneValue = new JLabel("false");
    JLabel lblTargetValue = new JLabel("");
    JButton btnRedownload = new JButton("Download Again");
    JButton btnDeleteFile = new JButton("Delete File");
    JButton btnRecheck = new JButton("Recheck");
    private final JLabel lblWeb = new JLabel("Web");
    private final LinkLabel lblWebValue = new LinkLabel("Web URL");
    private final JLabel lblStatus = new JLabel("Status");
    private final JLabel lblStatusValue = new JLabel("New label");

    public TorrentDownloadPanel() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0 };
        setLayout(gridBagLayout);

        JLabel lblDirectDownload = new JLabel();
        lblDirectDownload.setText("Torrent Download");
        GridBagConstraints gbc_lblDirectDownload = new GridBagConstraints();
        gbc_lblDirectDownload.insets = new Insets(0, 0, 5, 5);
        gbc_lblDirectDownload.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDirectDownload.gridx = 0;
        gbc_lblDirectDownload.gridy = 0;
        add(lblDirectDownload, gbc_lblDirectDownload);

        btnRedownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                transfer.stop();
                Transfer.State s = (State) transfer.getState();
                FileUtils.deleteQuietly(s.getTargetApp());
                transfer.start();
            }
        });
        GridBagConstraints gbc_btnRedownload = new GridBagConstraints();
        gbc_btnRedownload.insets = new Insets(0, 0, 5, 5);
        gbc_btnRedownload.gridx = 1;
        gbc_btnRedownload.gridy = 0;
        add(btnRedownload, gbc_btnRedownload);

        btnDeleteFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Transfer.State s = (State) transfer.getState();
                FileUtils.deleteQuietly(s.getTargetApp());
            }
        });
        GridBagConstraints gbc_btnDeleteFile = new GridBagConstraints();
        gbc_btnDeleteFile.insets = new Insets(0, 0, 5, 5);
        gbc_btnDeleteFile.gridx = 2;
        gbc_btnDeleteFile.gridy = 0;
        add(btnDeleteFile, gbc_btnDeleteFile);

        btnRecheck.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final TorrentDownload dd = (TorrentDownload) transfer;

                if (transfer.isAlive()) {
                    dd.recheck();
                } else {
                    transfer.longRun(new LongRunNotify() {
                        @Override
                        public void run(Runnable notify) {
                            System.out.println("long run check begin");
                            dd.recheck();

                            while (transfer.isAlive() && dd.checking()) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                }
                                notify.run();
                            }
                            dd.sync();
                            dd.remove();
                            notify.run();
                            System.out.println("long run check done");
                        }
                    });
                }
            }
        });
        GridBagConstraints gbc_btnDeleteFile2 = new GridBagConstraints();
        gbc_btnDeleteFile2.insets = new Insets(0, 0, 5, 0);
        gbc_btnDeleteFile2.gridx = 3;
        gbc_btnDeleteFile2.gridy = 0;
        add(btnRecheck, gbc_btnDeleteFile2);

        JLabel lblDone = new JLabel("Done?:");
        GridBagConstraints gbc_lblDone = new GridBagConstraints();
        gbc_lblDone.insets = new Insets(0, 0, 5, 5);
        gbc_lblDone.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDone.gridx = 0;
        gbc_lblDone.gridy = 1;
        add(lblDone, gbc_lblDone);

        GridBagConstraints gbc_lblDoneValue = new GridBagConstraints();
        gbc_lblDoneValue.insets = new Insets(0, 0, 5, 5);
        gbc_lblDoneValue.gridx = 1;
        gbc_lblDoneValue.gridy = 1;
        add(lblDoneValue, gbc_lblDoneValue);

        JLabel lblTarget = new JLabel("targetFile");
        GridBagConstraints gbc_lblTarget = new GridBagConstraints();
        gbc_lblTarget.insets = new Insets(0, 0, 5, 5);
        gbc_lblTarget.gridx = 0;
        gbc_lblTarget.gridy = 2;
        add(lblTarget, gbc_lblTarget);

        GridBagConstraints gbc_lblTargetValue = new GridBagConstraints();
        gbc_lblTargetValue.insets = new Insets(0, 0, 5, 5);
        gbc_lblTargetValue.gridx = 1;
        gbc_lblTargetValue.gridy = 2;
        add(lblTargetValue, gbc_lblTargetValue);

        GridBagConstraints gbc_lblWeb = new GridBagConstraints();
        gbc_lblWeb.insets = new Insets(0, 0, 5, 5);
        gbc_lblWeb.gridx = 0;
        gbc_lblWeb.gridy = 3;
        add(lblWeb, gbc_lblWeb);

        GridBagConstraints gbc_lblWebValue = new GridBagConstraints();
        gbc_lblWebValue.insets = new Insets(0, 0, 5, 5);
        gbc_lblWebValue.gridx = 1;
        gbc_lblWebValue.gridy = 3;
        add(lblWebValue, gbc_lblWebValue);

        GridBagConstraints gbc_lblStatus = new GridBagConstraints();
        gbc_lblStatus.insets = new Insets(0, 0, 0, 5);
        gbc_lblStatus.gridx = 0;
        gbc_lblStatus.gridy = 4;
        add(lblStatus, gbc_lblStatus);

        GridBagConstraints gbc_lblStatusValue = new GridBagConstraints();
        gbc_lblStatusValue.insets = new Insets(0, 0, 0, 5);
        gbc_lblStatusValue.gridx = 1;
        gbc_lblStatusValue.gridy = 4;
        add(lblStatusValue, gbc_lblStatusValue);

    }

    public void setTransfer(Transfer t) {
        this.transfer = t;

        TorrentDownload dd = (TorrentDownload) t;

        if (dd.state.getException() != null)
            lblDoneValue.setText(dd.state.getException().getMessage());
        else
            lblDoneValue.setText("" + (dd.state.done));

        Transfer.State s = (State) dd.getState();

        if (s.getTargetApp() != null)
            lblTargetValue.setText(s.getTargetApp().toString());
        else
            lblTargetValue.setText("");

        btnRedownload.setEnabled(!t.isAlive());

        File f = s.getTargetApp();
        if (f != null)
            btnDeleteFile.setEnabled(f.exists());
        else
            btnDeleteFile.setEnabled(false);

        if (dd.state.web != null)
            lblWebValue.setText(dd.state.web);
        else
            lblWebValue.setText("");

        // if (dd.torrent != null) {
        // torrent_status st = dd.torrent.status();
        // lblStatusValue.setText(dd.lib.status(st));
        // }
    }
}
