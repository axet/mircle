package com.google.code.mircle.plugins.googlereader;

import com.google.code.mircle.core.Model;

public class GoogleReaderModel implements Model{

    String t;

    public GoogleReaderModel(String t) {
        this.t = t;
    }

    public String toString() {
        return t;
    }

    @Override
    public void close() {
    }
}
