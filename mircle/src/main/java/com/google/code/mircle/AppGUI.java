package com.google.code.mircle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.github.axet.wget.info.ex.DownloadError;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.ModelRename;

public class AppGUI {

    static class ExpandTree extends JTree {

        public ExpandTree(DefaultTreeModel m) {
            super(m);
        }

        public void setColapse(TreePath p, boolean b) {
            setExpandedState(p, !b);
        }
    }

    // top panel
    AppPanelTop top;
    // top frame
    public JFrame main;
    // our plugins / rss models list
    ExpandTree left;
    JScrollPane leftScrool;
    // main view
    JPanel right;
    // left and right panel slider
    JSplitPane split;
    public AppPlayerBottom bottom;

    int dividerSize;
    int dividerLocation;

    Core core;

    public AppGUI(Core core) {
        this.core = core;
    }

    public void close() {
        main.setVisible(false);

        top = null;
        right = null;
        left = null;
        leftScrool = null;
        split = null;
    }

    public void dispose() {
        main.dispose();
        main = null;
    }

    public void about() {
        AboutDialog d = new AboutDialog();
        d.modal(main);
    }

    public void createWindow() {
        main = new JFrame("Mircle - BETA");

        right = new JPanel(new BorderLayout());

        left = new ExpandTree(new DefaultTreeModel(new DefaultMutableTreeNode()));
        left.setRootVisible(false);
        left.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        leftScrool = new JScrollPane(left, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftScrool, right);
        split.setOneTouchExpandable(true);
        split.setDividerLocation(250);

        main.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent arg0) {
            }

            @Override
            public void windowIconified(WindowEvent arg0) {
            }

            @Override
            public void windowDeiconified(WindowEvent arg0) {
            }

            @Override
            public void windowDeactivated(WindowEvent arg0) {
            }

            @Override
            public void windowClosing(WindowEvent arg0) {
            }

            @Override
            public void windowClosed(WindowEvent arg0) {
            }

            @Override
            public void windowActivated(WindowEvent arg0) {
            }
        });

        top = new AppPanelTop(this);
        bottom = new AppPlayerBottom(this);

        main.getContentPane().add(top, BorderLayout.NORTH);
        main.getContentPane().add(split, BorderLayout.CENTER);
        main.getContentPane().add(bottom, BorderLayout.SOUTH);
        main.pack();

        main.setSize(new Dimension(1200, 800));
        main.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        main.setLocationRelativeTo(null);

        Icon model = new ImageIcon(AppGUI.class.getResource("/model.png"));
        Icon plugin = new ImageIcon(AppGUI.class.getResource("/plugin.png"));
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) left.getCellRenderer();
        renderer.setLeafIcon(plugin);
        renderer.setClosedIcon(plugin);
        renderer.setOpenIcon(plugin);
        renderer.setLeafIcon(model);
    }

    public boolean isVisible() {
        return main.isVisible();
    }

    public void show() {
        main.setVisible(true);
        main.toFront();
        main.requestFocus();
    }

    public void hide() {
        main.setVisible(false);
    }

    public void toggleFullScreen() {
        if (left.isVisible()) {
            dividerSize = split.getDividerSize();
            dividerLocation = split.getDividerLocation();
            split.setDividerSize(0);
            left.setVisible(false);
        } else {
            offFullScreen();
        }
    }

    public void offFullScreen() {
        if (!left.isVisible()) {
            left.setVisible(true);
            split.setDividerSize(dividerSize);
            split.setDividerLocation(dividerLocation);
        }
    }

    public void downloadDialog() {
        try {
            NewDownloadDialog dialog = new NewDownloadDialog();
            if (dialog.modal(main))
                core.app.mircle(dialog.url);
        } catch (DownloadError ee) {
            AppError.modalError(ee);
        }
    }

    /**
     * rename model in the left JTree
     * 
     * @param m
     */
    public void rename(ModelRename m) {
        RenameEditDialog re = new RenameEditDialog(core);
        re.m = m;
        re.modal();
    }
}
