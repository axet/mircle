package com.google.code.mircle.core;

/**
 * Class displayble as entry in the left tree panel.
 * 
 * View represens Model. View know how to parse model and produce preoper item
 * name.
 * 
 * @author axet
 * 
 */
public interface View {

    /**
     * get actual model correponed to the view
     * 
     * @return model
     */
    public Model getModel();

    /**
     * close view
     */
    public void close();

    /**
     * return proper model name
     * 
     * @return name
     */
    public String toString();

}
