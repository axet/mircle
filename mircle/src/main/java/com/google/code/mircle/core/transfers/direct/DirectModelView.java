package com.google.code.mircle.core.transfers.direct;

import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.View;

public class DirectModelView implements View {

    DirectModel model;

    public DirectModelView(DirectModel m) {
        model = m;
    }

    public String toString() {
        String str = "";

        if (model.count > 0)
            str += "D: " + model.count + " ";

        str += "Downloads";

        return str;
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public void close() {
    }

}
