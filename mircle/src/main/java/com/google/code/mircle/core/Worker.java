package com.google.code.mircle.core;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Worker extends LongRun {

    public static class Status {
        // delay in seconds before retry, 0 - downloading
        public Integer delay;
        // total bytes downloaded
        public Long current;
        // size of download, null unknown total
        public Long total;
        // download title
        public String title;
        // download rate
        public Long downloadRate;
        public Long downloadRateAverage;
        // upload rate (system trafic for direct downloads?)
        public Long uploadRate;

        public Status(String title) {
            this.title = title;
        }

        public Status(long total, long current, String title) {
            this.current = current;
            this.total = total;
            this.title = title;
        }

        public Status(long total, long current, String title, int delay) {
            this.current = current;
            this.total = total;
            this.title = title;
            this.delay = delay;
        }

        public Status(Long total, long current, String title, int delay, long download, long avg) {
            this.current = current;
            this.total = total;
            this.title = title;
            this.delay = delay;
            this.downloadRate = download;
            this.downloadRateAverage = avg;
        }

        public Status(long total, long current, String title, long download, long avg) {
            this.current = current;
            this.total = total;
            this.title = title;
            this.downloadRate = download;
            this.downloadRateAverage = avg;
        }
    }

    Runnable notify;

    protected AtomicBoolean stop = new AtomicBoolean(false);

    public static enum DownloadState {
        // just created
        DOWNLOAD_NEW,

        // downloading
        DOWNLOAD_START,

        // queued for downloading
        DOWNLOAD_QUEUED,

        // done - success
        DOWNLOAD_COMPLETE,

        // stop by user
        DOWNLOAD_STOP,

        // failed
        DOWNLOAD_FAILED
    }

    public static class State {
        // TODO drop download state. move it to the transfer.
        private DownloadState downloadState;

        public State() {
            setDownloadState(DownloadState.DOWNLOAD_QUEUED);
        }

        synchronized public DownloadState getDownloadState() {
            return downloadState;
        }

        synchronized public void setDownloadState(DownloadState downloadState) {
            this.downloadState = downloadState;
        }
    }

    /**
     * Give us your persistent state.
     * 
     * @return stat object
     */
    public abstract State getState();

    /**
     * call each time state / progress is changed
     */
    protected void changed() {
        notify.run();
    }

    abstract protected void run();

    @Override
    public void run(Runnable notify) {
        this.notify = notify;

        run();
    }

    /**
     * Check if source has a conflikt in source (the same source).
     * 
     * @param w
     * @return True if it is the same url
     */
    public boolean checkSource(Worker w) {
        return checkSource(w.getSource());
    }

    public boolean checkSource(String source) {
        return getSource().equals(source);
    }

    /**
     * Check if here a some conflickts with target files (cross files on the
     * same / different download target). App should show the dialog which shows
     * worker1 and worker2 with conflicts and files tree with highlight a
     * problem.
     * 
     * @param w
     * @return True - if here is some conflicts
     */
    public boolean checkTarget(Worker w) {
        return false;
    }

    /**
     * change state to start. override to control state change.
     */
    public void start() {
        stop.set(false);

        getState().setDownloadState(DownloadState.DOWNLOAD_START);

        super.start();
    }

    /**
     * change state to queued. override to control state change.
     */
    public void putToQueue() {
        getState().setDownloadState(DownloadState.DOWNLOAD_QUEUED);
    }

    public boolean isQueued() {
        return getState().getDownloadState().equals(DownloadState.DOWNLOAD_QUEUED);
    }

    public boolean isDownloading() {
        return getState().getDownloadState().equals(DownloadState.DOWNLOAD_START);
    }

    /**
     * change state to stop. override to control state change.
     */
    public void stop() {
        getState().setDownloadState(DownloadState.DOWNLOAD_STOP);

        stop.set(true);
    }

    /**
     * when application exits, it close the objects
     */
    public void close() {
        stop.set(true);

        super.close();
    }

    /**
     * save current state in xml form.
     * 
     * @return state
     */
    abstract public State save();

    /**
     * load state
     * 
     * @param s
     */
    abstract public void load(State s);

    /**
     * get source, can be magen link or url. so return String.
     * 
     * @return
     */
    abstract public String getSource();

    /**
     * Status text representation.
     * 
     * TODO drop status in favor of Model / View logic (which is here already)
     * 
     * @return status
     */
    abstract public Status status();

    @Override
    public String getLongRunName() {
        return getSource();
    }
}
