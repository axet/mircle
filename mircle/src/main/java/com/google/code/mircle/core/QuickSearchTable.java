package com.google.code.mircle.core;

import java.awt.Rectangle;
import java.awt.event.KeyListener;

import javax.swing.DefaultListModel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.google.code.mircle.core.QuickSearch.ListModel;
import com.google.code.mircle.core.QuickSearchList.Model;

public abstract class QuickSearchTable extends QuickSearch {

    class Model implements ListModel {
        DefaultTableModel m;

        public Model(DefaultTableModel m) {
            this.m = m;
        }

        DefaultTableModel getModel() {
            return m;
        }

        @Override
        public void add(Object o) {
            addRow(m, o);
        }

        @Override
        public int getCount() {
            return m.getRowCount();
        }

        @Override
        public Object get(int i) {
            return getRow(m, i);
        }

        @Override
        public String toString(Object o) {
            return QuickSearchTable.this.toString(o);
        }

    }

    ListContainer list = new ListContainer() {
        @Override
        public void setModel(ListModel l) {
            table.setModel(((Model) l).getModel());
        }

        @Override
        public void select(Object m) {
            int i = indexOf((DefaultTableModel) table.getModel(), m);

            table.setRowSelectionInterval(i, i);
        }

        @Override
        public void scrollTo(Object m) {
            int i = indexOf((DefaultTableModel) table.getModel(), m);

            Rectangle r = table.getCellRect(i, 0, true);
            table.scrollRectToVisible(r);
        }

        @Override
        public void requestFocus() {
            table.requestFocus();
        }

        @Override
        public Object getSelected() {
            int i = table.getSelectedRow();

            if (i == -1)
                return null;

            return getRow((DefaultTableModel) table.getModel(), i);
        }

        @Override
        public ListModel getModel() {
            return new Model((DefaultTableModel) table.getModel());
        }

        @Override
        public void addKeyListener(KeyListener l) {
            table.addKeyListener(l);
        }
    };

    JTable table;

    public QuickSearchTable(JTable table, JTextField field) {
        this.table = table;

        create(field, list);
    }

    public ListModel getMasterListModel() {
        return new Model(getMasterModel());
    }

    public DefaultTableModel getMasterModel() {
        return ((Model) modelMaster).getModel();
    }

    @Override
    public ListModel createListModel() {
        return new Model(createModel());
    }

    public abstract DefaultTableModel createModel();

    public abstract Object getRow(DefaultTableModel m, int i);

    public abstract void addRow(DefaultTableModel m, Object o);

    public abstract int indexOf(DefaultTableModel m, Object o);

    public abstract void doParentLayout();

    public abstract String toString(Object o);
}
