package com.google.code.mircle.core.views;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class PlaylistView extends JPanel implements Controller {

    public PlaylistView(PlaylistModel s) {
        super(new BorderLayout());

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("[manual playlist created from some_file_downloaded.zip by one click]");
        listModel.addElement("[can be some_file.torrent and listed here only valible directory content like *.avi]");
        listModel.addElement("[or directory from Shares with fiter by *.mp3]");
        listModel.addElement(s.toString() + " Season1 episode 1");
        listModel.addElement(s.toString() + " Season1 episode 2");
        listModel.addElement(s.toString() + " Season2 episode 1");

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }


}
