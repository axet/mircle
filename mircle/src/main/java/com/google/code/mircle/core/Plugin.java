package com.google.code.mircle.core;

import java.io.File;

public interface Plugin {

    public void close();

    public void save(File path);

    public void load(File path);

}
