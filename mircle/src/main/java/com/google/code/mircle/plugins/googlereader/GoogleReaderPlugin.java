package com.google.code.mircle.plugins.googlereader;

import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;

public class GoogleReaderPlugin implements Plugin, PluginRoot {

    public GoogleReaderPlugin() {
    }

    public String getName() {
        return "Google Reader API";
    }

    public void close() {

    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
//        return Arrays.asList(new Model[] { new GoogleReaderModel("Goole Read folder1"),
//                new GoogleReaderModel("Goole Read folder2") });
    }

    public JPopupMenu contextMenu(Model m) {
        JPopupMenu popup = new JPopupMenu();
        popup.add(new JMenuItem("123"));
        return popup;
    }

    public Controller createController(Model m) {
        if (m instanceof GoogleReaderModel)
            return new GoogleReaderView((GoogleReaderModel) m);
        return null;
    }

    @Override
    public void save(File path) {
    }

    @Override
    public void load(File path) {
    }

    @Override
    public View createView(Model m) {
        return null;
    }

    @Override
    public int dndAction(Model m) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        // TODO Auto-generated method stub
        return false;
    }
}
