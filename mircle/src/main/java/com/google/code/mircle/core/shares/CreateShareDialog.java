package com.google.code.mircle.core.shares;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class CreateShareDialog extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField textField;
    private JTextField textField_1;
    JDialog that;

    public CreateShareDialog() {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);
        that = this;

        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[] { 146, 213, 0 };
        gbl_contentPanel.rowHeights = new int[] { 28, 28, 0 };
        gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        contentPanel.setLayout(gbl_contentPanel);

        JLabel lblShareName = new JLabel("Share name:");
        GridBagConstraints gbc_lblShareName = new GridBagConstraints();
        gbc_lblShareName.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblShareName.insets = new Insets(0, 0, 5, 5);
        gbc_lblShareName.gridx = 0;
        gbc_lblShareName.gridy = 0;
        contentPanel.add(lblShareName, gbc_lblShareName);

        textField = new JTextField();
        textField.setColumns(10);
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.fill = GridBagConstraints.BOTH;
        gbc_textField.insets = new Insets(0, 0, 5, 0);
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 0;
        contentPanel.add(textField, gbc_textField);

        JLabel lblSharePath = new JLabel("Share Path:");
        GridBagConstraints gbc_lblSharePath = new GridBagConstraints();
        gbc_lblSharePath.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblSharePath.insets = new Insets(0, 0, 0, 5);
        gbc_lblSharePath.gridx = 0;
        gbc_lblSharePath.gridy = 1;
        contentPanel.add(lblSharePath, gbc_lblSharePath);

        textField_1 = new JTextField();
        textField_1.setColumns(10);
        GridBagConstraints gbc_textField_1 = new GridBagConstraints();
        gbc_textField_1.anchor = GridBagConstraints.NORTH;
        gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_1.gridx = 1;
        gbc_textField_1.gridy = 1;
        contentPanel.add(textField_1, gbc_textField_1);
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        that.setVisible(false);
                        that.dispose();
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        that.setVisible(false);
                        that.dispose();
                    }
                });
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }
}
