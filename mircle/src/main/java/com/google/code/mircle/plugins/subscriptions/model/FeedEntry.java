package com.google.code.mircle.plugins.subscriptions.model;

import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.code.mircle.core.Model;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FeedEntry")
public class FeedEntry implements Model {
    public String title;
    public String link;
    // feed date (usually published date)
    public Date date;
    // received date (when we have downloaded this item. normaly == published
    // date. but in many cases not)
    public Date dateReceived;
    public String description;
    public String hash;
    public String media;
    // never read by user
    public boolean unread = true;
    // just added. never seen by user
    public boolean newItem = true;
    // has been downloaded?
    public boolean direct = false;
    // if we have any problem with the feed put it here
    public Throwable exception;

    // parent
    transient public RSSModel model;

    String calcHash() {
        String s = "";
        s += link;
        s += title;
        s += date == null ? 0 : date.getTime();
        return DigestUtils.md5Hex(s);
    }

    public FeedEntry(SyndEntry e) {
        title = e.getTitle().trim();
        link = e.getLink().trim();
        date = e.getPublishedDate();
        SyndContent con = e.getDescription();
        if (con != null) {
            description = con.getValue();
            if (description != null)
                description = description.trim();
        }
        if (e.getEnclosures().size() > 0) {
            SyndEnclosure ee = (SyndEnclosure) e.getEnclosures().get(0);
            media = ee.getUrl();
        }
        hash = calcHash();
    }

    public String toString() {
        String str = "";

        if (newItem) {
            str += "(new) ";
        } else if (unread)
            str += "* ";

        str += title;

        str += " / " + date;

        return str;
    }

    @Override
    public void close() {
    }
}