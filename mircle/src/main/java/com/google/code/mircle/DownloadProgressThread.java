package com.google.code.mircle;

import java.awt.Component;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.StringUtils;

import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.github.axet.wget.info.ex.DownloadInterruptedError;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.transfers.direct.DirectDownload;
import com.google.code.mircle.core.transfers.direct.DirectView;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolder;
import com.google.code.mircle.plugins.subscriptions.model.RSSModel;

public class DownloadProgressThread {

    Core core;

    enum States {
        BEGIN, EXTRACTING, EXTRACTING_HTML, EXTRACTING_HTML_DONE
    };

    private DownloadInfo info;

    private States state;

    public DownloadProgressThread(Core core) {
        this.core = core;
    }

    public void modal(final String url, Component main) {
        final DownloadProgressDialog dialog = new DownloadProgressDialog();

        final AtomicBoolean stop = new AtomicBoolean(false);
        final Runnable notify = new Runnable() {
            @Override
            public void run() {
                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        String str = "";
                        switch (getState()) {
                        case BEGIN:
                            str += "Working ... ";
                            break;
                        case EXTRACTING:
                            if (getInfo().getDelay() != 0)
                                str += "Retyring ... " + getInfo().getDelay() + "\n" + getInfo().getException();
                            else
                                str += "Working ... " + getInfo().getState();
                            break;
                        case EXTRACTING_HTML:
                            str += "Extracting html";
                            break;
                        case EXTRACTING_HTML_DONE:
                            str += "Extracting html done";
                            break;
                        default:
                            break;
                        }
                        dialog.lblValue.setText(str);
                    }
                });
            }
        };

        setState(States.BEGIN);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mircle(url, stop, notify);
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dispose();
                        }
                    });
                } catch (DownloadInterruptedError e) {
                    // ignore
                } catch (final DownloadError e) {
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dispose();
                            AppError.modalError(e);
                        }
                    });
                }
            }
        });
        t.start();

        dialog.lblUrl.setText(url);

        if (!dialog.modal(main))
            stop.set(true);

        try {
            t.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void mircle(String url, AtomicBoolean stop, Runnable notify) {
        setState(States.BEGIN);
        notify.run();

        // url can come with mircle: prefix. usially from web browser. when
        // user
        // click on special link (registed to our app)
        url = StringUtils.removeStart(url, "mircle:");

        // if it is special url from a browser
        if (mircleBrowser(url))
            return;

        // Here we shall do two steps
        //
        // 1) check if we can determinate what to do by url host / params.
        // it can be obivious youtube.com, vimeo.com, torrent links
        //
        // 2) handle content. we shall check if it is a 1) RSS Feed, 2) HTML
        // with RSS feed, 3) direct download file, or 4) text file

        if (mircleDirect(url))
            return;

        if (mircleLocal(url))
            return;

        mircleContent(url, stop, notify);
    }

    boolean mircleLocal(String url) {
        try {
            new URL(url);
            return false;
        } catch (MalformedURLException e) {
            // check for .torrent file download
            throw new DownloadError("local files do not supported");
            // return true;
        }
    }

    boolean mircleBrowser(String url) {
        // parse url of scheme [url:some_url][origin:origin_url]

        String uuu = null;
        String origin = null;

        String q;
        try {
            q = URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        String[] uu = StringUtils.splitByWholeSeparator(q, "][");
        for (String u : uu) {
            u = StringUtils.removeStart(u, "[");
            u = StringUtils.removeEnd(u, "]");
            if (u.startsWith("url:")) {
                uuu = StringUtils.removeStart(u, "url:");
            }
            if (u.startsWith("origin:")) {
                origin = StringUtils.removeStart(u, "origin:");
            }
        }
        try {
            if (uuu != null && origin != null) {
                Transfer t = new DirectDownload(new URL(uuu), core.transfers.getDownloadFolder(), new URL(origin));
                downloadDirect(t);
                core.save(core.transfers);
                return true;
            }
        } catch (MalformedURLException e) {
            throw new DownloadError(e);
        }

        return false;
    }

    boolean mircleDirect(String source) {
        Transfer t = null;

        t = core.transfers.getDirect().extract(source);

        if (t != null) {
            downloadDirect(t);
            return true;
        }

        return false;

    }

    void mircleContent(String source, final AtomicBoolean stop, final Runnable notify) {
        try {
            URL url = new URL(source);

            {
                // 1) check if we already have this download in the download
                // list.
                // if so, we dont need to handle it twice. just highlight it
                // there.
                DirectDownload t = new DirectDownload(url, core.transfers.getDownloadFolder());
                Transfer tt = core.transfers.getDirect().check(t);
                if (tt != null) {
                    if (!tt.isAlive())
                        core.transfers.getDirect().start(tt);

                    core.save(core.transfers);
                    core.app.appSwitch.modelSwitch(core.transfers.getDirect().getClass());

                    DirectView view = (DirectView) core.app.appSwitch.getView(core.transfers.getDirect());
                    view.select(tt);
                    return;
                }
            }

            setInfo(new DownloadInfo(url));
            setState(States.EXTRACTING);
            notify.run();
            getInfo().extract(stop, notify);

            Model m = null;

            // 2) if it is rss+xml type === subscribe
            m = core.rss.extract(getInfo());
            if (m != null) {
                core.app.appSwitch.modelSwitch(core.rss.getClass(), m);
                core.save(core.rss);
                return;
            }

            String ct = getInfo().getContentType().toLowerCase();
            if (ct.contains("text/html") || ct.contains("text/xml")) {
                setState(States.EXTRACTING_HTML);
                String html = WGet.getHtml(new URL(source), new WGet.HtmlLoader() {
                    @Override
                    public void notifyRetry(int delay, Throwable e) {
                        notify.run();
                    }

                    @Override
                    public void notifyMoved() {
                        notify.run();
                    }

                    @Override
                    public void notifyDownloading() {
                        notify.run();
                    }
                }, stop);
                setState(States.EXTRACTING_HTML_DONE);
                // 3) if it is text / html + rss link inside === subscribe
                final Model mm = core.rss.extract(getInfo(), html);
                if (mm != null) {
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            RSSModel mmm = core.rss.exist((RSSModel) mm);
                            if (mmm == null) {
                                core.rss.add((RSSModel) mm);
                                core.app.appSwitch.modelSwitch(core.rss.getClass(), mm);
                            } else {
                                core.app.appSwitch.modelSwitch(core.rss.getClass(), mmm);
                            }
                            
                            core.save(core.rss);
                        }
                    });
                    return;
                } else {
                    // 4) if it is text / html with no rss link inside save as
                    // downloaded
                    DirectDownload t = new DirectDownload(getInfo(), core.transfers.getDownloadFolder());
                    t.downloaded(html);

                    // add it to the list
                    core.transfers.getDirect().add(t);

                    // save application state
                    core.save(core.transfers);

                    // and switch and hightlight the complited download
                    core.app.appSwitch.modelSwitch(core.transfers.getDirect().getClass());
                    DirectView view = (DirectView) core.app.appSwitch.getView(core.transfers.getDirect());
                    view.select(t);
                    core.save(core.transfers);
                    return;
                }
            } else {
                // 5) if it is not text / html === download (probaby direct link
                // to the movie or binary file
                Transfer t = new DirectDownload(getInfo(), core.transfers.getDownloadFolder());
                downloadDirect(t);
                core.save(core.transfers);
                return;
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (MalformedURLException e) {
            throw new DownloadError(e);
        }
    }

    void downloadDirect(Transfer t) {
        t = core.transfers.getDirect().download(t);

        if (!t.isAlive())
            core.transfers.getDirect().start(t);

        core.app.appSwitch.modelSwitch(core.transfers.getDirect().getClass());

        DirectView view = (DirectView) core.app.appSwitch.getView(core.transfers.getDirect());
        view.select(t);
    }

    synchronized public DownloadInfo getInfo() {
        return info;
    }

    synchronized public void setInfo(DownloadInfo info) {
        this.info = info;
    }

    synchronized public States getState() {
        return state;
    }

    synchronized public void setState(States state) {
        this.state = state;
    }

}
