package com.google.code.mircle;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class AppPanelTop extends JPanel {

    public AppPanelTop(final AppGUI app) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gridBagLayout.rowHeights = new int[] { 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        JButton btnNewDownload = new JButton("New Download");
        btnNewDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                app.downloadDialog();
            }
        });
        GridBagConstraints gbc_btnNewDownload = new GridBagConstraints();
        gbc_btnNewDownload.insets = new Insets(0, 0, 0, 5);
        gbc_btnNewDownload.gridx = 0;
        gbc_btnNewDownload.gridy = 0;
        add(btnNewDownload, gbc_btnNewDownload);

        final AppPanelTop that = this;

        JButton btnAbout = new JButton("About");
        btnAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                app.about();
            }
        });
        
        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 0, 5);
        gbc_panel.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel.gridx = 1;
        gbc_panel.gridy = 0;
        add(panel, gbc_panel);
        GridBagConstraints gbc_btnAbout = new GridBagConstraints();
        gbc_btnAbout.gridx = 10;
        gbc_btnAbout.gridy = 0;
        add(btnAbout, gbc_btnAbout);

    }

}
