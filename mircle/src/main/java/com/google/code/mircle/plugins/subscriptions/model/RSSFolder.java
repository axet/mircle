package com.google.code.mircle.plugins.subscriptions.model;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.github.axet.wget.info.DownloadInfo;
import com.google.code.mircle.core.LongRun;
import com.google.code.mircle.core.ModelExpand;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.plugins.subscriptions.model.RSSFolderExpand.RSSModelExpand;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class RSSFolder extends RSSItem implements ModelExpand {

    // QUEUED - need refresh
    //
    // DONE - refresh done, feed parsed and loaded
    //
    // EXTRACTING - loading data from the web, parsing the result, merging with
    // present feed
    //
    // STOP - work interrupted by user
    //
    // ERROR - exception catched (runttimeexception will be passed to the app,
    // and not holded here)

    @XStreamAlias("RSSFolderState")
    public static class State extends Worker.State {
        public String name;

        public boolean collapsed = false;

        // folder id
        public Long id;

        // parent folder id
        public Long folderId;
    }

    // items, rssfolders or rssmodels, for the TreeView
    DefaultListModel models = new DefaultListModel();

    // exapand all sub folders / models to the following object
    RSSFolderExpand expand;

    State state = new State();

    // listeners. who want to know when we add more rss entires, delete them or
    // update them.
    ArrayList<ListDataListener> listeners = new ArrayList<ListDataListener>();

    public RSSFolder() {
        expand = new RSSFolderExpand(this);

        getState().setDownloadState(null);
    }

    public void addListener(ListDataListener l) {
        listeners.add(l);
    }

    public void removeListener(ListDataListener l) {
        listeners.remove(l);
    }

    @Override
    public void close() {
        for (RSSItem t : modelListenerMap.keySet()) {
            t.removeListener(modelListenerMap.get(t));
        }
        modelListenerMap.clear();

        for (RSSItem t : modelLongListenerMap.keySet()) {
            t.removeListener(modelLongListenerMap.get(t));
        }
        modelLongListenerMap.clear();
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    protected void run() {
        // we dont run things
    }

    @Override
    public State save() {
        return state;
    }

    @Override
    public void load(Worker.State s) {
        state = (State) s;
    }

    @Override
    public String getSource() {
        // TODO drop getSource from worker and move it to Transfer
        return null;
    }

    @Override
    public Status status() {
        String title = state.name;

        return new Status(title);
    }

    public static boolean handle(DownloadInfo info) {
        if (info.getContentType().toLowerCase().contains("rss+xml"))
            return true;

        return false;
    }

    public Long getFolderId() {
        return getState().id;
    }

    @Override
    public FeedEntry get(int i) {
        if (expand.rssModels.size() <= i)
            expand.loadMoreItems(i);
        return expand.rssModels.get(i);
    }

    @Override
    public int getSize() {
        int count = 0;

        count += getSize(this);

        return count;
    }

    static int getSize(RSSFolder s) {
        int count = 0;

        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                count += m.getState().entries.size();
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                count += getSize(r);
            }
        }

        return count;
    }

    static int getDownloadedCount(RSSFolder s) {
        int count = 0;

        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                count += m.getDownloadedCount();
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                count += getDownloadedCount(r);
            }
        }

        return count;
    }

    public long getDownloadedCount() {
        return getDownloadedCount(this);
    }

    public long getDownloadCount() {
        return getDownloadedCount() + getDownloadingCount();
    }

    static int getDownloadingCount(RSSFolder s) {
        int count = 0;

        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                count += m.getDownloadingCount();
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                count += getDownloadingCount(r);
            }
        }

        return count;
    }

    public long getDownloadingCount() {
        return getDownloadingCount(this);
    }

    @Override
    public DefaultListModel getTreeModels() {
        return models;
    }

    // map helps to unsubscribe from element events which are delted. or on
    // RSSFolder.close() operation
    HashMap<RSSItem, ListDataListener> modelListenerMap = new HashMap<RSSItem, ListDataListener>();
    HashMap<RSSItem, LongRun.Listener> modelLongListenerMap = new HashMap<RSSItem, LongRun.Listener>();

    public void addModel(RSSItem t) {
        addModel(models.getSize(), t);
    }

    public void addModel(int pos, final RSSItem m) {
        ListDataListener l = new ListDataListener() {
            @Override
            public void intervalRemoved(ListDataEvent e) {
                expand.refreshItems();
                for (int i = e.getIndex0(); i < e.getIndex1(); i++) {
                    FeedEntry ee = (FeedEntry) m.get(i);
                    notifyChanged(ee);
                }
            }

            @Override
            public void intervalAdded(ListDataEvent e) {
                expand.refreshItems();
                for (int i = e.getIndex0(); i < e.getIndex1(); i++) {
                    FeedEntry ee = (FeedEntry) m.get(i);
                    notifyChanged(ee);
                }
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                expand.refreshItems();
                for (int i = e.getIndex0(); i < e.getIndex1(); i++) {
                    FeedEntry ee = (FeedEntry) m.get(i);
                    notifyChanged(ee);
                }
            }
        };
        m.addListener(l);
        modelListenerMap.put(m, l);

        LongRun.Listener ll = new LongRun.Listener() {
            @Override
            public void stop() {
                modelChanged(m);
                notifyLongRunChanged();
            }

            @Override
            public void start() {
                modelChanged(m);
                notifyLongRunChanged();
            }

            @Override
            public void close() {
                modelChanged(m);
                notifyLongRunChanged();
            }

            @Override
            public void changed() {
                modelChanged(m);
                notifyLongRunChanged();
            }
        };
        m.addListener(ll);
        modelLongListenerMap.put(m, ll);

        m.setParentFolderId(getState().id);

        models.add(pos, m);

        expand.refreshItems();
    }

    public void removeModel(RSSItem t) {
        ListDataListener l = modelListenerMap.get(t);
        t.removeListener(l);
        modelListenerMap.remove(t);

        LongRun.Listener ll = modelLongListenerMap.get(t);
        t.removeListener(ll);
        modelLongListenerMap.remove(t);

        int i = models.indexOf(t);
        models.remove(i);
    }

    public void merge(RSSFolder mm) {
        for (Enumeration<?> e = mm.models.elements(); e.hasMoreElements();) {
            RSSItem t = (RSSItem) e.nextElement();
            addModel(t);
        }
    }

    static int getNewCount(RSSFolder s) {
        int count = 0;

        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                count += m.getState().feed.getNewCount();
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                count += getNewCount(r);
            }
        }

        return count;
    }

    public int getNewCount() {
        return getNewCount(this);
    }

    static int getUnreadCount(RSSFolder s) {
        int count = 0;

        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                count += m.getState().feed.getUnreadCount();
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                count += getUnreadCount(r);
            }
        }

        return count;
    }

    public int getUnreadCount() {
        return getUnreadCount(this);
    }

    /**
     * mark all new items as old
     */
    public void markAllAsOld() {
        ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();
        RSSFolderExpand.expand(expand, this);

        for (RSSModelExpand m : expand) {
            RSSModel mm = m.m;
            mm.markAllAsOld();
        }

        notifyLongRunChanged();
    }

    @Override
    public Transfer getTransfer(FeedEntry e) {
        RSSModel m = e.model;
        return m.getTransfer(e);
    }

    public void markAllAsRead() {
        ArrayList<RSSModelExpand> expand = new ArrayList<RSSModelExpand>();
        RSSFolderExpand.expand(expand, this);

        for (RSSModelExpand m : expand) {
            RSSModel mm = m.m;
            mm.markAllAsRead();
        }

        notifyLongRunChanged();
    }

    @Override
    public void download(FeedEntry e) {
        e.model.download(e);
    }

    public void notifyChanged(FeedEntry e) {
        int i = expand.indexOf(e);
        for (ListDataListener l : listeners) {
            ListDataEvent d = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, i, i);
            l.contentsChanged(d);
        }
    }

    public void notifyAdded(FeedEntry e) {
        int i = expand.indexOf(e);
        for (ListDataListener l : listeners) {
            ListDataEvent d = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i);
            l.intervalAdded(d);
        }
    }

    @Override
    public Long getParentFolderId() {
        return getState().folderId;
    }

    @Override
    public void setParentFolderId(Long l) {
        getState().folderId = l;
    }

    @Override
    public String getName() {
        return getState().name;
    }

    @Override
    public void setName(String s) {
        getState().name = s;
    }

    static void getDownloadState(Set<DownloadState> states, RSSFolder s) {
        for (Enumeration<?> e = s.models.elements(); e.hasMoreElements();) {
            Worker t = (Worker) e.nextElement();
            if (t instanceof RSSModel) {
                RSSModel m = (RSSModel) t;
                states.add(m.getState().getDownloadState());
            }
            if (t instanceof RSSFolder) {
                RSSFolder r = (RSSFolder) t;
                getDownloadState(states, r);
            }
        }
    }

    @Override
    public DownloadState getDownloadState() {
        Set<DownloadState> states = new TreeSet<Worker.DownloadState>();

        getDownloadState(states, this);

        if (states.contains(DownloadState.DOWNLOAD_START))
            return DownloadState.DOWNLOAD_START;

        if (states.contains(DownloadState.DOWNLOAD_QUEUED))
            return DownloadState.DOWNLOAD_QUEUED;

        if (states.contains(DownloadState.DOWNLOAD_FAILED))
            return DownloadState.DOWNLOAD_FAILED;

        if (states.contains(DownloadState.DOWNLOAD_STOP))
            return DownloadState.DOWNLOAD_STOP;

        return DownloadState.DOWNLOAD_COMPLETE;
    }

    public void modelChanged(RSSItem m) {
        int index = models.indexOf(m);
        models.set(index, m);
    }

    @Override
    public boolean getCollapsed() {
        return getState().collapsed;
    }

    @Override
    public void setCollapsed(boolean e) {
        getState().collapsed = e;
    }

}
