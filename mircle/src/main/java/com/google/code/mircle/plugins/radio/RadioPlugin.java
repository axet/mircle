package com.google.code.mircle.plugins.radio;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.commons.io.FileUtils;

import com.github.axet.shoutcast.SHOUTgenre;
import com.github.axet.shoutcast.SHOUTparent;
import com.github.axet.wget.info.DownloadInfo;
import com.google.code.mircle.core.Controller;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Plugin;
import com.google.code.mircle.core.PluginRoot;
import com.google.code.mircle.core.View;
import com.google.code.mircle.core.Worker;
import com.thoughtworks.xstream.XStream;

public class RadioPlugin implements Plugin, PluginRoot {

    SHOUTcastModel shoutcast;
    DefaultListModel model = new DefaultListModel();
    Core core;

    public RadioPlugin(Core core) {
        this.core = core;

        shoutcast = new SHOUTcastModel(this);

        model.addElement(shoutcast);
    }

    public void close() {
    }

    @Override
    public DefaultListModel getTreeModels() {
        return model;
    }

    public JPopupMenu contextMenu(Model m) {
        JPopupMenu popup = new JPopupMenu();
        popup.add(new JMenuItem("123"));
        return popup;
    }

    public Controller createController(Model m) {
        if (m instanceof RadioPlugin)
            return new RadioController((RadioPlugin) m);
        if (m instanceof RadioModel)
            return new RadioView((RadioModel) m);
        if (m instanceof SHOUTcastModel)
            return new SHOUTcastModelController(core, (SHOUTcastModel) m);
        return null;
    }

    @Override
    public void save(File path) {
        core.save(shoutcast.save(), xstream(), FileUtils.getFile(path, "shoutcast.xml"));
    }

    XStream xstream() {
        XStream stream = new XStream();
        stream.processAnnotations(new Class[] { SHOUTcastModel.State.class, SHOUTgenre.class, SHOUTparent.class,
                DownloadInfo.class });
        return stream;
    }

    @Override
    public void load(File path) {
        try {
            XStream s = xstream();
            Worker.State state = (Worker.State) s.fromXML(FileUtils.getFile(path, "shoutcast.xml"));
            shoutcast.load(state);
        } catch (com.thoughtworks.xstream.io.StreamException stream) {
            try {
                throw stream.getCause();
            } catch (FileNotFoundException ignore) {
                // skip file not found
            } catch (EOFException ignore) {
                // empty file
            } catch (Throwable e) {
                throw new com.thoughtworks.xstream.io.StreamException(e);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void notifySHOUTcast() {
        int i = model.indexOf(shoutcast);
        model.set(i, model.get(i));
    }

    @Override
    public View createView(Model m) {
        if (m instanceof SHOUTcastModel)
            return new SHOUTcastModelView((SHOUTcastModel) m);
        return new RadioPluginView(this);
    }

    @Override
    public int dndAction(Model m) {
        return 0;
    }

    @Override
    public boolean dndCheck(Model source, Model target) {
        return false;
    }

    @Override
    public boolean dndDrop(Model source, Model target) {
        return false;
    }
}
