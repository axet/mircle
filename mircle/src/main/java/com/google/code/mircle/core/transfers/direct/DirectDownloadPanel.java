package com.google.code.mircle.core.transfers.direct;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.io.FileUtils;

import com.google.code.mircle.core.Worker.DownloadState;
import com.google.code.mircle.core.transfers.direct.Transfer.State;

public class DirectDownloadPanel extends JPanel {

    Transfer t;
    JLabel lblNewLabel_1 = new JLabel("false");
    JLabel lblNewLabel_3 = new JLabel("New label");
    JButton btnDeleteFile = new JButton("Delete File");
    private final JLabel lblWeb = new JLabel("Web");
    private final LinkLabel lblWeb_1 = new LinkLabel("Web URL");
    private final ProgressPanel progress = new ProgressPanel();
    private final JLabel lblState = new JLabel("State");
    private final JLabel label = new JLabel("New label");
    private final JLabel lblSize = new JLabel("Size");
    private final JLabel label_1 = new JLabel("New label");

    public DirectDownloadPanel() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0 };
        setLayout(gridBagLayout);

        JLabel lblDirectDownload = new JLabel();
        lblDirectDownload.setText("Direct Download");
        GridBagConstraints gbc_lblDirectDownload = new GridBagConstraints();
        gbc_lblDirectDownload.insets = new Insets(0, 0, 5, 5);
        gbc_lblDirectDownload.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblDirectDownload.gridx = 0;
        gbc_lblDirectDownload.gridy = 0;
        add(lblDirectDownload, gbc_lblDirectDownload);

        btnDeleteFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Transfer.State s = (State) t.getState();
                FileUtils.deleteQuietly(s.getTargetApp());
            }
        });
        GridBagConstraints gbc_btnDeleteFile = new GridBagConstraints();
        gbc_btnDeleteFile.insets = new Insets(0, 0, 5, 5);
        gbc_btnDeleteFile.gridx = 2;
        gbc_btnDeleteFile.gridy = 0;
        add(btnDeleteFile, gbc_btnDeleteFile);

        progress.setPreferredSize(new Dimension(100, 100));

        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.gridx = 3;
        gbc_panel.gridy = 0;
        add(progress, gbc_panel);

        JLabel lblNewLabel = new JLabel("Done?:");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 1;
        add(lblNewLabel, gbc_lblNewLabel);

        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_1.gridx = 1;
        gbc_lblNewLabel_1.gridy = 1;
        add(lblNewLabel_1, gbc_lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel("targetFile");
        GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
        gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_2.gridx = 0;
        gbc_lblNewLabel_2.gridy = 2;
        add(lblNewLabel_2, gbc_lblNewLabel_2);

        GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
        gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_3.gridx = 1;
        gbc_lblNewLabel_3.gridy = 2;
        add(lblNewLabel_3, gbc_lblNewLabel_3);

        GridBagConstraints gbc_lblWeb = new GridBagConstraints();
        gbc_lblWeb.insets = new Insets(0, 0, 5, 5);
        gbc_lblWeb.gridx = 0;
        gbc_lblWeb.gridy = 3;
        add(lblWeb, gbc_lblWeb);

        GridBagConstraints gbc_lblWeb_1 = new GridBagConstraints();
        gbc_lblWeb_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblWeb_1.gridx = 1;
        gbc_lblWeb_1.gridy = 3;
        add(lblWeb_1, gbc_lblWeb_1);

        GridBagConstraints gbc_lblState = new GridBagConstraints();
        gbc_lblState.insets = new Insets(0, 0, 5, 5);
        gbc_lblState.gridx = 0;
        gbc_lblState.gridy = 4;
        add(lblState, gbc_lblState);

        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 1;
        gbc_label.gridy = 4;
        add(label, gbc_label);

        GridBagConstraints gbc_lblSize = new GridBagConstraints();
        gbc_lblSize.insets = new Insets(0, 0, 0, 5);
        gbc_lblSize.gridx = 0;
        gbc_lblSize.gridy = 5;
        add(lblSize, gbc_lblSize);

        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 0, 5);
        gbc_label_1.gridx = 1;
        gbc_label_1.gridy = 5;
        add(label_1, gbc_label_1);

    }

    public void setTransfer(Transfer t) {
        this.t = t;

        DirectDownload dd = (DirectDownload) t;

        if (dd.state.getException() != null)
            lblNewLabel_1.setText(dd.state.getException().getMessage());
        else
            lblNewLabel_1.setText("" + (dd.state.getDownloadState() == DownloadState.DOWNLOAD_COMPLETE));

        Transfer.State s = (State) t.getState();

        if (s.getTargetApp() != null)
            lblNewLabel_3.setText(s.getTargetApp().toString());
        else
            lblNewLabel_3.setText("");

        File f = s.getTargetApp();
        if (f != null)
            btnDeleteFile.setEnabled(f.exists());
        else
            btnDeleteFile.setEnabled(false);

        if (dd.state.getWeb() != null)
            lblWeb_1.setText(dd.state.getWeb().toString());
        else
            lblWeb_1.setText("");

        if (dd.state.getInfo() != null && !dd.state.getInfo().empty()) {
            progress.update(dd.state.getInfo());
            label.setText(dd.state.getInfo().getState().toString());
            label_1.setText(String.format("%d / %d", dd.state.getInfo().getLength(), dd.state.getInfo().getCount()));
        }

    }
}
