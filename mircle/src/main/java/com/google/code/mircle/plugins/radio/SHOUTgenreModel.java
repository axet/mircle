package com.google.code.mircle.plugins.radio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.github.axet.shoutcast.SHOUTgenre;
import com.github.axet.shoutcast.SHOUTstation;
import com.github.axet.shoutcast.SHOUTstations;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Model;
import com.google.code.mircle.core.Worker;

public class SHOUTgenreModel extends Worker implements Model {

    public static class State extends Worker.State {
        SHOUTgenre genre;

        // stations refresh date
        Date stationsDate;

        // stations list
        List<SHOUTstationModel> stations = new ArrayList<SHOUTstationModel>();
    }

    State state = new State();
    SHOUTcastModel sc;

    SHOUTstations s = new SHOUTstations();

    DefaultTableModel model;

    // date when we put this task to the download queue.
    // determine the order in which we have to download it.
    Date queueDate;

    public SHOUTgenreModel(SHOUTcastModel sc, State s) {
        state = s;

        for (SHOUTstationModel m : state.stations) {
            m.genre = this;
        }

        create(sc);
    }

    public SHOUTgenreModel(SHOUTcastModel sc, SHOUTgenre s) {
        this.setGenre(s);

        create(sc);
    }

    void create(SHOUTcastModel sc) {
        this.sc = sc;

        getState().setDownloadState(DownloadState.DOWNLOAD_COMPLETE);

        model = SHOUTstationView.createTreeModel();

        updateStations();
    }

    public void setQueueDate(Date d) {
        queueDate = d;
    }

    public Date getQueueDate() {
        return queueDate;
    }

    @Override
    public void close() {
    }

    public SHOUTgenre getGenre() {
        return getState().genre;
    }

    public boolean empty() {
        return getState().stations.isEmpty();
    }

    public List<SHOUTstationModel> getStations() {
        return getState().stations;
    }

    @Override
    public boolean equals(Object g) {
        return this.getGenre().getURL().equals(((SHOUTgenreModel) g).getGenre().getURL());
    }

    @Override
    public State getState() {
        return state;
    }

    public class SortByListeners implements Comparator<SHOUTstationModel> {
        @Override
        public int compare(SHOUTstationModel o1, SHOUTstationModel o2) {
            Long l1 = o1.getStation().getListeners();
            Long l2 = o2.getStation().getListeners();

            return l2.compareTo(l1);
        }
    }

    @Override
    protected void run() {
        final SHOUTgenreModel that = this;

        s.extract(getState().genre, stop, new Runnable() {
            @Override
            public void run() {
                sc.genreChanged(that);
            }
        });

        // remove old
        for (int i = 0; i < getState().stations.size(); i++) {
            SHOUTstationModel m = getState().stations.get(i);
            if (m.getFavorite())
                continue;
            if (m.play)
                continue;

            if (!s.getStations().contains(m.getStation())) {
                getState().stations.remove(i);
                i--;
            }
        }

        // add new
        for (SHOUTstation ss : s.getStations()) {
            if (!contains(ss))
                getState().stations.add(new SHOUTstationModel(this, ss));
        }

        Collections.sort(getState().stations, new SortByListeners());

        state.stationsDate = new Date();

        getState().setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
        sc.genreChanged(this);

        Core.invoke(new Runnable() {
            @Override
            public void run() {
                updateStations();
            }
        });
    }

    boolean contains(SHOUTstation ss) {
        for (SHOUTstationModel s : getState().stations) {
            if (s.getStation().equals(ss))
                return true;
        }

        return false;
    }

    @Override
    public State save() {
        return state;
    }

    @Override
    public void load(Worker.State s) {
        state = (State) s;
    }

    @Override
    public String getSource() {
        return getState().genre.getURL().toString();
    }

    @Override
    public Status status() {
        return null;
    }

    public void setGenre(SHOUTgenre genre) {
        this.getState().genre = genre;
    }

    DefaultTableModel getModel() {
        return model;
    }

    void updateStations() {
        // delete sations no longer present in the list
        for (int i = 0; i < model.getRowCount(); i++) {
            SHOUTstationView t = (SHOUTstationView) model.getValueAt(i, 0);
            SHOUTstationModel m = (SHOUTstationModel) t.getModel();

            if (!state.stations.contains(m)) {
                if (m.getFavorite())
                    continue;
                if (m.play)
                    continue;

                model.removeRow(i);
                // restart current index check
                i--;
            }
        }

        // add new stations

        // boost on empty lists
        if (model.getRowCount() == 0) {
            for (int i = 0; i < state.stations.size(); i++) {
                SHOUTstationModel g = state.stations.get(i);
                model.insertRow(i, SHOUTstationView.toTreeModel(g));
            }
        } else {
            for (int i = 0; i < state.stations.size(); i++) {
                SHOUTstationModel g = state.stations.get(i);
                if (!exist(model, g)) {
                    if (model.getRowCount() < i)
                        throw new RuntimeException("station duplicates, broken storage");
                    model.insertRow(i, SHOUTstationView.toTreeModel(g));
                }
            }
        }
    }

    public void notifyStationChanged(SHOUTstationModel g) {
        for (int i = 0; i < model.getRowCount(); i++) {
            SHOUTstationView t = (SHOUTstationView) model.getValueAt(i, 0);
            SHOUTstationModel m = (SHOUTstationModel) t.getModel();

            if (m.equals(g)) {
                Object[] oo = SHOUTstationView.toTreeModel(g);
                for (int c = 0; c < model.getColumnCount(); c++) {
                    model.setValueAt(oo[c], i, c);
                }
                return;
            }
        }
    }

    boolean exist(DefaultTableModel model, SHOUTstationModel g) {
        for (int i = 0; i < model.getRowCount(); i++) {
            SHOUTstationView t = (SHOUTstationView) model.getValueAt(i, 0);
            SHOUTstationModel m = (SHOUTstationModel) t.getModel();

            if (m.equals(g))
                return true;
        }

        return false;
    }

    void addRow(SHOUTgenreModel g) {
        model.addRow(new Object[] { new SHOUTgenreView(g) });
    }

    /**
     * remove all normal (not marked as favorites)
     * 
     * @return true if all removed
     */
    public boolean removeNormal() {
        for (int i = 0; i < state.stations.size(); i++) {
            SHOUTstationModel s = state.stations.get(i);
            if (s.getFavorite())
                continue;
            if (s.play)
                continue;

            state.stations.remove(i);
            i--;
        }

        return state.stations.size() == 0;
    }
}
