package com.google.code.mircle.core;

import com.google.code.mircle.core.transfers.direct.Transfer;

/**
 * Plugin which handles urls and downloads. (http://, magnet://)
 * 
 */
public interface PluginDirect {

    public void close();

    /**
     * 
     * @param url
     *            file to download
     * @return
     */
    public Transfer extract(String url);

}
