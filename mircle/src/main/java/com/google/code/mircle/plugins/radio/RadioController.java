package com.google.code.mircle.plugins.radio;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

import com.google.code.mircle.core.Controller;

public class RadioController extends JPanel implements Controller {

    public RadioController(RadioPlugin s) {
        super(new BorderLayout());

        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("Radio Provider 1");
        listModel.addElement("Radio Provider 2");
        listModel.addElement(s.toString());

        JList l = new JList(listModel);
        add(l, BorderLayout.CENTER);
    }

    @Override
    public void activated() {
    }

    @Override
    public void deactivated() {
    }

    @Override
    public void close() {
    }

}
