package com.google.code.mircle.plugins.subscriptions.model;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.google.code.mircle.core.Core;
import com.google.code.mircle.core.Worker;
import com.google.code.mircle.core.transfers.direct.DirectDownload;
import com.google.code.mircle.core.transfers.direct.Transfer;
import com.google.code.mircle.plugins.subscriptions.SubscriptionsPlugin;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class RSSModel extends RSSItem {

    // QUEUED - need refresh
    //
    // DONE - refresh done, feed parsed and loaded
    //
    // EXTRACTING - loading data from the web, parsing the result, merging with
    // present feed
    //
    // STOP - work interrupted by user
    //
    // ERROR - exception catched (runttimeexception will be passed to the app,
    // and not holded here)

    @XStreamAlias("RSSModelState")
    public static class State extends Worker.State {
        // web source url
        public URL source;
        // actual rss feed url
        public URL rssSource;

        public Throwable e;

        public Date refresh;

        public boolean autoDownload = false;

        public String name;

        // parent folder id
        public Long folderId;

        // feed
        public Feed feed = new Feed();

        // entries
        public List<FeedEntry> entries = new ArrayList<FeedEntry>();
    }

    // amout of downloading objects
    private long downloadingCount = 0;

    // amount of downloaded objects
    private long downloadedCount = 0;

    static class SortByDate implements Comparator<SyndEntry> {
        @Override
        public int compare(SyndEntry arg0, SyndEntry arg1) {
            Date d1 = arg0.getPublishedDate();
            Date d2 = arg1.getPublishedDate();
            if (d1 == null && d2 == null)
                return 0;
            if (d1 == null)
                return -1;
            if (d2 == null)
                return 1;
            return d1.compareTo(d2);
        }

    }

    State state = new State();

    Core core;

    // map rss entry hash to the actual entry.
    // helps to find entries quickly (and prevent duplicates)
    Map<String, FeedEntry> hashEntries = new HashMap<String, FeedEntry>();

    Map<FeedEntry, RSSModelTransferLink> transfers = new HashMap<FeedEntry, RSSModelTransferLink>();

    // listeners. who want to know when we add more rss entires, delete them or
    // update them.
    ArrayList<ListDataListener> listeners = new ArrayList<ListDataListener>();

    public RSSModel(Core core) {
        this.core = core;
    }

    public RSSModel(Core core, URL web, String xml) throws FeedException {
        this.core = core;
        this.state.source = web;
        this.state.rssSource = web;

        loadFeed(xml);
    }

    public RSSModel(Core core, URL web, URL url) {
        this.core = core;
        this.state.source = web;
        this.state.rssSource = url;
    }

    public RSSModel(Core core, URL url) {
        this.core = core;
        this.state.source = url;
        this.state.rssSource = url;
    }

    public void addListener(ListDataListener l) {
        listeners.add(l);
    }

    public void removeListener(ListDataListener l) {
        listeners.remove(l);
    }

    public void notifyChanged(FeedEntry e) {
        int first = state.entries.indexOf(e);
        final ListDataEvent eventChanged = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, first, first);
        for (ListDataListener l : listeners) {
            l.contentsChanged(eventChanged);
        }
    }

    public boolean extracted() {
        return state.refresh != null;
    }

    public void extract(final AtomicBoolean stop, final Runnable notify) {
        String html = WGet.getHtml(state.rssSource, new WGet.HtmlLoader() {
            @Override
            public void notifyRetry(int delay, Throwable e) {
                notify.run();
            }

            @Override
            public void notifyDownloading() {
                notify.run();
            }

            @Override
            public void notifyMoved() {
                notify.run();
            }
        }, stop);

        try {
            loadFeed(html);

            if (state.name == null) {
                state.name = state.feed.getTitle();
            }

            state.setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
        } catch (RuntimeException e) {
            throw e;
        } catch (FeedException e) {
            throw new DownloadError(e);
        }
    }

    void loadFeed(String html) throws FeedException {
        try {
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new StringReader(html));

            state.feed.setTitle(feed.getTitle().trim());

            @SuppressWarnings("unchecked")
            List<SyndEntry> entries = feed.getEntries();
            Collections.sort(entries, new SortByDate());

            synchronized (state) {
                int count = 0;
                for (SyndEntry e : entries) {
                    FeedEntry f = new FeedEntry((SyndEntry) e);
                    f.model = this;
                    if (hashEntries.containsKey(f.hash))
                        continue;

                    f.dateReceived = new Date();

                    state.feed.setUnreadCount(state.feed.getUnreadCount() + 1);
                    state.feed.setNewCount(state.feed.getNewCount() + 1);
                    transfers.put(f, new RSSModelTransferLink(core, this, f));
                    hashEntries.put(f.hash, f);
                    state.entries.add(0, f);
                    count++;
                }

                if (count > 0) {
                    final ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, count - 1);
                    Core.invoke(new Runnable() {
                        @Override
                        public void run() {
                            for (ListDataListener l : listeners) {
                                l.intervalAdded(event);
                            }
                        }
                    });
                    int last = state.entries.size() - 1;
                    int first = count;
                    if (first <= last) {
                        final ListDataEvent eventChanged = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED,
                                first, last);
                        Core.invoke(new Runnable() {
                            @Override
                            public void run() {
                                for (ListDataListener l : listeners) {
                                    l.contentsChanged(eventChanged);
                                }
                            }
                        });
                    }
                }

                Core.invoke(new Runnable() {
                    @Override
                    public void run() {
                        autoDownloadIf();
                    }
                });
            }
            state.refresh = new Date();
        } catch (IllegalArgumentException e) {
            throw new FeedException("loadFeed", e);
        }
    }

    @Override
    public void close() {
        for (FeedEntry e : transfers.keySet()) {
            RSSModelTransferLink l = transfers.get(e);
            l.close();
        }
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    protected void run() {
        try {
            extract(stop, new Runnable() {
                @Override
                public void run() {
                    changed();
                }
            });
        } catch (DownloadError e) {
            state.setDownloadState(DownloadState.DOWNLOAD_FAILED);
            state.e = e;
        }
    }

    @Override
    public State save() {
        return state;
    }

    @Override
    public void load(Worker.State s) {
        state = (State) s;

        for (FeedEntry e : state.entries) {
            e.model = this;
            hashEntries.put(e.hash, e);
            transfers.put(e, new RSSModelTransferLink(core, this, e));
        }
    }

    @Override
    public String getSource() {
        return state.source.toString();
    }

    @Override
    public Status status() {
        String title = state.name;
        if (title == null)
            title = state.source.toString();

        return new Status(title);
    }

    public static boolean handle(DownloadInfo info) {
        if (info.getContentType().toLowerCase().contains("rss+xml"))
            return true;

        return false;
    }

    public static List<URL> extractRSS(URL url, String html) {
        try {
            List<URL> urls = new ArrayList<URL>();

            // 1) application/rss+xml
            {
                Document doc = Jsoup.parse(html, url.toString());
                Elements paragraphs = doc.select("link[type=application/rss+xml]");
                if (paragraphs.size() > 0) {
                    for (int i = 0; i < paragraphs.size(); i++) {
                        Element e = paragraphs.get(i);
                        String href = e.attr("abs:href");
                        if (href != null && !href.isEmpty())
                            urls.add(new URL(href));
                        else
                            throw new DownloadError("bad href rss " + href);
                    }
                }
            }

            // 2) application/atom+xml
            {
                Document doc = Jsoup.parse(html, url.toString());
                Elements paragraphs = doc.select("link[type=application/atom+xml]");
                if (paragraphs.size() > 0) {
                    for (int i = 0; i < paragraphs.size(); i++) {
                        Element e = paragraphs.get(i);
                        String href = e.attr("abs:href");
                        if (href != null && !href.isEmpty())
                            urls.add(new URL(href));
                        else
                            throw new DownloadError("bad href rss " + href);
                    }
                }
            }

            return urls;
        } catch (MalformedURLException e) {
            throw new DownloadError(e);
        }
    }

    @Override
    public FeedEntry get(int i) {
        return getState().entries.get(i);
    }

    @Override
    public int getSize() {
        return getState().entries.size();
    }

    public long getDownloadedCount() {
        return downloadedCount;
    }

    public void setDownloadedCount(long downloadedCount) {
        this.downloadedCount = downloadedCount;

        notifyLongRunChanged();
    }

    public Transfer getTransfer(FeedEntry e) {
        RSSModelTransferLink l = transfers.get(e);
        return l.transfer;
    }

    public void setTransfer(FeedEntry e, Transfer ee) {
        RSSModelTransferLink l = transfers.get(e);
        e.direct = true;
        l.setTransfer(ee);

        notifyChanged(e);
    }

    public void autoDownloadIf() {
        if (state.autoDownload)
            autoDownload();
    }

    public void autoDownload() {
        for (FeedEntry e : getState().entries) {
            if (getDownloadCount() >= SubscriptionsPlugin.AUTO_DOWNLOAD_MAX)
                return;

            if (!e.unread)
                continue;

            if (e.direct)
                continue;

            download(e);
        }
    }

    public long getDownloadCount() {
        return getDownloadedCount() + getDownloadingCount();
    }

    public void downloadNext() {
        ;
    }

    @Override
    public void download(FeedEntry entry) {
        try {
            String file = entry.media;
            String web = entry.link;

            Transfer t = null;

            if (file == null)
                t = core.transfers.getDirect().extract(web);
            else
                t = core.transfers.getDirect().extract(file);

            if (t == null) {
                if (file == null)
                    t = new DirectDownload(new URL(web), core.transfers.getDownloadFolder());
                else
                    t = new DirectDownload(new URL(file), core.transfers.getDownloadFolder(), new URL(web));
            }

            t = core.transfers.getDirect().download(t);

            setTransfer(entry, t);

            if (!t.isAlive())
                core.transfers.getDirect().start(t);

            core.save(core.transfers);
            notifyLongRunChanged();
        } catch (MalformedURLException e) {
            entry.exception = e;
            notifyChanged(entry);
        }
    }

    public long getDownloadingCount() {
        return downloadingCount;
    }

    public void setDownloadingCount(long downloadingCount) {
        this.downloadingCount = downloadingCount;
    }

    @Override
    public DefaultListModel getTreeModels() {
        return null;
    }

    @Override
    public Long getParentFolderId() {
        return getState().folderId;
    }

    @Override
    public void setParentFolderId(Long l) {
        getState().folderId = l;
    }

    @Override
    public String getName() {
        return getState().name;
    }

    @Override
    public void setName(String s) {
        getState().name = s;
    }

    public void markAllAsRead() {
        for (FeedEntry e : getState().entries) {
            e.unread = false;
            e.newItem = false;
            notifyChanged(e);
        }
        getState().feed.setNewCount(0);
        getState().feed.setUnreadCount(0);
        notifyLongRunChanged();
    }

    public void markAllAsOld() {
        for (FeedEntry e : getState().entries) {
            e.newItem = false;
            notifyChanged(e);
        }
        getState().feed.setNewCount(0);
        notifyLongRunChanged();
    }
}
