package com.google.code.mircle.core.transfers.direct;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import com.github.axet.vget.VGet;
import com.github.axet.vget.info.VideoInfo;
import com.github.axet.vget.info.VimeoParser;
import com.github.axet.vget.info.YouTubeParser;
import com.github.axet.wget.SpeedInfo;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.ex.DownloadError;
import com.github.axet.wget.info.ex.DownloadInterruptedError;
import com.google.code.mircle.core.Worker;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class VGetDownload extends Transfer {

    State state = new State();

    Object joinLock = new Object();

    VGet vget;

    SpeedInfo speedInfo = new SpeedInfo();

    @XStreamAlias("VGetDownloadState")
    public static class State extends Transfer.State {
        private VideoInfo info;

        synchronized public VideoInfo getInfo() {
            return info;
        }

        synchronized public void setInfo(VideoInfo info) {
            this.info = info;
        }

        synchronized public void setDownloadState(DownloadState downloadState, Throwable e) {
            super.setDownloadState(downloadState);

            this.setException(e);
        }
    }

    public VGetDownload(URL source, File targetDir) {
        this.state.setTargetUser(targetDir);

        this.state.setInfo(new VideoInfo(source));
    }

    public VGetDownload() {

    }

    public static boolean handle(String url) {
        try {
            if (YouTubeParser.extractId(new URL(url)) != null)
                return true;

            if (VimeoParser.extractId(new URL(url)) != null)
                return true;

            return false;
        } catch (RuntimeException e) {
            throw e;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public void close() {
        super.close();

        // after close thread. sync bytes information. so we hold actual data in
        // save state
        sync();
    }

    class Sync implements Runnable {
        @Override
        public void run() {
            sync();
            changed();
        }

    }

    class SyncError implements Runnable {
        long last = 0;

        @Override
        public void run() {
            long now = System.currentTimeMillis();
            if (last + DirectView.MAX_UPDATE_TIME < now) {
                last = now;

                sync();
                getState().setException(getState().getInfo().getException());
                changed();
            }
        }

    }

    @Override
    public void run() {
        if (state.getDownloadState() == DownloadState.DOWNLOAD_COMPLETE && getTarget().exists()) {
            return;
        }

        while (!stop.get()) {
            try {
                VideoInfo vinfo = state.getInfo();

                vget = new VGet(vinfo, state.getTargetUser());

                if (vget.empty()) {
                    vget.extract(stop, new SyncError());
                    if (vinfo.getInfo().getRange())
                        vinfo.getInfo().enableMultipart();
                }

                state.setException(null);

                // keep target between restarts only if file present and the
                // same size
                if (state.getTargetApp() != null) {
                    vget.setTarget(state.getTargetApp());
                }

                DownloadInfo info = vinfo.getInfo();
                if (info != null)
                    speedInfo.start(info.getCount());

                vget.download(stop, new SyncError());

                state.setDownloadState(DownloadState.DOWNLOAD_COMPLETE);
                sync();
                changed();

                return;
            } catch (DownloadInterruptedError e) {
                return;
            } catch (DownloadError e) {
                state.setDownloadState(DownloadState.DOWNLOAD_FAILED, e);
                sync();
                changed();
                return;
            }
        }
    }

    @Override
    public void downloadAgain() {
        super.downloadAgain();

        VideoInfo vinfo = state.getInfo();
        vinfo.reset();

        DownloadInfo info = vinfo.getInfo();
        if (info != null)
            info.reset();
    }

    void sync() {
        if (state.getInfo().getInfo() != null) {
            speedInfo.step(state.getInfo().getInfo().getCount());
        }
        if (vget != null)
            state.setTargetApp(vget.getTarget());
    }

    @Override
    public Status status() {
        String title = state.getInfo().getTitle();

        if (title == null)
            title = getSource();

        if (state.getInfo().getInfo() != null)
            return new Status(state.getInfo().getInfo().getLength(), state.getInfo().getInfo().getCount(), title, state
                    .getInfo().getDelay(), speedInfo.getCurrentSpeed(), speedInfo.getAverageSpeed());
        else
            return new Status(-1l, -1l, title, state.getInfo().getDelay(), speedInfo.getCurrentSpeed(),
                    speedInfo.getAverageSpeed());
    }

    public Throwable getException() {
        return state.getException();
    }

    public File getTarget() {
        return state.getTargetApp();
    }

    @Override
    public Transfer.State save() {
        return state;
    }

    @Override
    public void load(Worker.State s) {
        state = (State) s;
    }

    @Override
    public String getSource() {
        return state.getInfo().getWeb().toString();
    }

}
