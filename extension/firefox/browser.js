Mircle.Browser = {

  init : function() {
    var that = this;
    gBrowser.addEventListener("DOMContentLoaded", function(event) {
      that.handlePageLoaded(event);
    }, true);
  },

  uninit : function() {
  },

  /**
   * Handles the DOMContentLoaded event.
   * 
   * @param aEvent
   *            the event object.
   */
  handlePageLoaded : function(event) {
    var doc = event.originalTarget;

    if (doc instanceof HTMLDocument) {
      Mircle.Services.probe(doc);
    }
  }

};
