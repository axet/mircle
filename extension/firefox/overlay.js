window.addEventListener("load", function() {
  Mircle.Console.init();
  Mircle.Browser.init();
}, false);

window.addEventListener("unload", function() {
  Mircle.Browser.uninit();
}, false);
