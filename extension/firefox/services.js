Mircle.Services = {

  googlereader : {
    probe : function(doc) {
      var chrome = doc.getElementById('chrome');
      if (chrome == null)
        return false;

      return true;
    },
    url : 'resource://mircle/injection/googlereader.js'
  },

  probe : function(doc) {
    var services = [ this.googlereader ];

    for ( var i = 0; i < services.length; i++) {
      var service = services[i];
      if (service.probe(doc)) {
        var invokeIPC = doc.getElementById('invokeIPC');
        if (invokeIPC == null) {
          invokeIPC = this.inject(doc, 'invokeIPC', 'resource://mircle/injection/ipc.js');
        }
        var invokeService = doc.getElementById('invokeService');
        if (invokeService == null) {
          invokeService = this.inject(doc, 'invokeService', service.url);
        }
        return true;
      }
    }

    return false;
  },

  inject : function(doc, name, url) {
    invokeService = doc.createElement('script');
    invokeService.id = name;
    invokeService.setAttribute('type', 'text/javascript');
    invokeService.setAttribute('src', url);
    doc.body.appendChild(invokeService);
    
    return invokeService;
  },

};
