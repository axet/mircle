var Mircle = {};

Mircle.IPC = {

  url : function(path) {
    var id = chrome.i18n.getMessage("@@extension_id");
    return "chrome-extension://" + id + "/" + path;
  },

};
