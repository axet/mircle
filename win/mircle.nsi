; The name of the installer
Name "Mircle"

; The file to write
OutFile "mircle.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Mircle\

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page directory
Page instfiles

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File Mircle\mircle.jar
  File Mircle\mircle.ico
  File /r Mircle\natives\
  
  CreateDirectory "$SMPROGRAMS\Mircle\"
  CreateShortCut "$SMPROGRAMS\Mircle\Mircle.lnk" "$SYSDIR\javaw.exe" "-jar mircle.jar" "$INSTDIR\mircle.ico"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Mircle" \
                 "DisplayName" "Mircle - Your Personal File Portal"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Mircle" \
				 "UninstallString" "$\"$INSTDIR\uninstall.exe$\""

  WriteUninstaller "$INSTDIR\uninstall.exe"
				 
SectionEnd ; end the section


Section "Uninstall"
  RMDir /r "$INSTDIR"

  RMDIR /r "$SMPROGRAMS\Mircle"
  
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Mircle"
SectionEnd
