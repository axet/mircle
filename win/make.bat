cd ..

cmd.exe /C mvn clean package -DskipTests

cd win

rmdir /S /Q Mircle
mkdir Mircle
copy /B ..\mircle\target\mircle-*.jar Mircle\mircle.jar
xcopy /E ..\mircle\target\natives Mircle\natives\
copy /B mircle.ico Mircle\mircle.ico

cmd.exe /C "C:\Program Files\NSIS\makensis.exe" mircle.nsi

rmdir /S /Q Mircle

pause
